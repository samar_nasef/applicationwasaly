import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import Pusher from 'pusher-js';
declare const Pusher: any;
@Injectable()
export class PusherServiceProvider {

  channel
  constructor(public http: HttpClient) {
    console.log('Hello PusherServiceProvider Provider');
    // 4bfc71f760c7d3f919db -> api key
    // 6d505d3ed5dd3857b199 -> my app key
    // samarnasef80@gmail.com ,, qw123456

//     app_id = "721611"
// key = "d0405c6a3d83a10edb55"
// secret = "397a50ac0bdb7e6cac10"
// cluster = "eu"

    var pusher = new Pusher("d0405c6a3d83a10edb55", { 
      cluster: 'eu',
      encrypted: true,
      });
      this.channel = pusher.subscribe('my-channel');
  }
  public init(){
    return this.channel;
   }
}
