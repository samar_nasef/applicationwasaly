import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController ,AlertController,Events,Platform} from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { Storage } from '@ionic/storage';

import * as firebase from 'firebase';

//buttons border color #8c8986

// #!/usr/bin/env node

@Injectable()
export class HelperProvider {

public registration;
  // public lang_direction = "rtl";
  public lang_direction ;
  public lat = 30.044281;
  public lng = 31.340002;
  // public serviceUrl = "http://www.wasly.eg-target.com";
  //test fire base
  public serviceUrl = "http://www.wasly2.eg-target.com";
  
//  public googlekey = "AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY"

// public googlekey="AIzaSyCcmnjBytUy5oWEy5iC3EGcr5-A3dlmF7c"  //key for new account for wasaly 
// public googlekey="AIzaSyCY9jLYq3Z62ZFQqRYbGpyraX7aStJUW0s" 
public googlekey = "AIzaSyC2QnyjAeavsSLmTnsNPXeAVj5nlcWOOiU"

 public fromLocation=""
 public toLocation=""
public toAddressDesc = "" 

public mapStyle=[
  {
      "featureType": "landscape.natural",
      "elementType": "geometry.fill",
      "stylers": [
          {
              "visibility": "on"
          },
          {
              "color": "#e0efef"
          }
      ]
  },
  {
      "featureType": "poi",
      "elementType": "geometry.fill",
      "stylers": [
          {
              "visibility": "on"
          },
          {
              "hue": "#1900ff"
          },
          {
              "color": "#c0e8e8"
          }
      ]
  },
  {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [
          {
              "lightness": 100
          },
          {
              "visibility": "simplified"
          }
      ]
  },
  {
      "featureType": "road",
      "elementType": "labels",
      "stylers": [
          {
              "visibility": "off"
          }
      ]
  },
  {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [
          {
              "visibility": "on"
          },
          {
              "lightness": 700
          }
      ]
  },
  {
      "featureType": "water",
      "elementType": "all",
      "stylers": [
          {
              "color": "#7dcdcd"
          }
      ]
  }
]

  constructor(public alertCtrl: AlertController,private platform: Platform,
    private geolocation: Geolocation,
    public diagnostic: Diagnostic,
    public locationAccuracy: LocationAccuracy,
    private storage: Storage,public toastCtrl: ToastController,public http: HttpClient) {
    console.log('Hello HelperProvider Provider');

  }
  public presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'bottom',
      cssClass: this.lang_direction
    });
    toast.present();
  }
  storeLanguage(userlang){
   
    console.log("storeLanguage",userlang);
    this.storage.set('wasaly_language', userlang).then(resp=>{
      console.log("resp set('language',: ",resp)
      // window.location.reload();
    });
  }
  storeInfo(info){
    console.log("store this data from helper: ",info)
    this.storage.set('info', info);
  }

  // geoLoc(success) {

  //   let LocationAuthorizedsuccessCallback = (isAvailable) => {
  //     //console.log('Is available? ' + isAvailable);
  //     if (isAvailable) {
        
  //       this.GPSOpened(success);
  //     }
  //     else {
  //       this.requestOpenGPS(success);
  //     }
  //   };
  //   let LocationAuthorizederrorCallback = (e) => console.error(e);
  //   this.diagnostic.isLocationAvailable().then(LocationAuthorizedsuccessCallback).catch(LocationAuthorizederrorCallback);
  
  // }
  // GPSOpened(success) {
  //   let optionsLoc = {}
  //   optionsLoc = { timeout: 20000, enableHighAccuracy: true, maximumAge: 3600 };
  
  
  
  //   this.geolocation.getCurrentPosition(optionsLoc).then((resp) => {
  //     this.lat = resp.coords.latitude;
  //     this.lng = resp.coords.longitude;
  //    // this.events.publish('detectUserLocation');
  //     let data = {
  //       lat: resp.coords.latitude,
  //       long: resp.coords.longitude
  //     }
  //     console.log("user loction : ",data)
  //     success(data);
  //   },
  //   err => {
  //     console.log("getCurrentPosition error "+err)
  //     success("-1");
  //   }
  //   ).catch((error) => {
  //     success("-1");
  //   });
  
  
  // }
  // requestOpenGPS(success) {
  //   var detetLoc 
  //   var giveGPSPermission
  //   var cancel
  //   var openSettings
  //   if(this.lang_direction == "rtl"){
  //     detetLoc  = "تحديد الموقع"
  //     giveGPSPermission = "يجب اعطاء صلاحيات استخدام الموقع الجغرافى للتطبيق"
  //     cancel = "إلغاء"
  //     openSettings = "فتح الإعدادت"

  //   }else if(this.lang_direction == "ltr"){
  //     detetLoc  = "Detect Location"
  //     giveGPSPermission = "You must give permission to use the geographic location of the application"
  //     cancel = "Cancel"
  //     openSettings = "Open Settings"
  //   }
  //   if (this.platform.is('ios')) {
  //     this.diagnostic.getLocationAuthorizationStatus().then(status => {
  //       if (status == this.diagnostic.permissionStatus.NOT_REQUESTED) {
  //         //console.log('Permission not requested')
  //        // console.log('6')
  //         this.diagnostic.requestLocationAuthorization().then(status => {
  //           if (status == this.diagnostic.permissionStatus.NOT_REQUESTED) {
            
  //               let alert = this.alertCtrl.create({
  //                 title: detetLoc,
  //                 message: giveGPSPermission,
  //                 buttons: [
  //                   {
  //                     text: cancel,
  //                     role: 'cancel',
  //                     handler: () => {
  //                       console.log('Cancel clicked');
  //                       success("-1")
  //                     }
  //                   },
  //                   {
  //                     text: openSettings,
  //                     handler: () => {
  //                       console.log("open sett");
  //                       this.diagnostic.switchToSettings();
  
  //                     }
  //                   }
  //                 ]
  //               });
  //               alert.present();
              
  //           }
  //           else if (status == this.diagnostic.permissionStatus.DENIED) {
              
                
  //               let alert = this.alertCtrl.create({
  //                 title: detetLoc,
  //                 message: giveGPSPermission,
  //                 buttons: [
  //                   {
  //                     text: cancel,
  //                     role: 'cancel',
  //                     handler: () => {
  //                       console.log('Cancel clicked');
  //                       success("-1")
  //                     }
  //                   },
  //                   {
  //                     text: openSettings,
  //                     handler: () => {
  //                       console.log("open sett");
  //                       this.diagnostic.switchToSettings();
  
  //                     }
  //                   }
  //                 ]
  //               });
  //               alert.present();
              
  //           }
  //           else if (status == this.diagnostic.permissionStatus.GRANTED) {
  //             this.GPSOpened(success);
  //           }
  //           else if (status == this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE) {
  //             this.GPSOpened(success);
  //           }
  //         })
  //       }
  //       else if (status == this.diagnostic.permissionStatus.DENIED) {
          
  //           let alert = this.alertCtrl.create({
  //             title: detetLoc,
  //             message: giveGPSPermission,
  //             buttons: [
  //               {
  //                 text: cancel,
  //                 role: 'cancel',
  //                 handler: () => {
  //                   console.log('Cancel clicked');
  //                   success("-1")
  //                 }
  //               },
  //               {
  //                 text: openSettings,
  //                 handler: () => {
  //                   console.log("open sett");
  //                   this.diagnostic.switchToSettings();
  
  //                 }
  //               }
  //             ]
  //           });
  //           alert.present();
          
  //       }
  //       else if (status == this.diagnostic.permissionStatus.GRANTED) {
  //         this.GPSOpened(success);
  //       }
  //       else if (status == this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE) {
  //         this.GPSOpened(success);
  //       }
  //     });
  //   }
  //   else {
  //     this.locationAccuracy.canRequest().then((canRequest: boolean) => {
  
  //       if (canRequest) {
  //         this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
  //           () => {
  //             console.log('Request successful')
  
  //             this.geoLoc(success);
  //           },
  //           error => {
  //             console.log('Error requesting location permissions', error);
  
  //             // let toast = this.toastCtrl.create({
  //             //   message: this.translate.instant("gpsWithHigh"),
  //             //   duration: 3000,
  //             //   position: 'bottom',
  //             //   cssClass: 'errorTeast'
  //             // });
              
  //               let alert = this.alertCtrl.create({
  //                 title: detetLoc,
  //                 message: giveGPSPermission,
  //                 buttons: [
  //                   {
  //                     text: cancel,
  //                     role: 'cancel',
  //                     handler: () => {
  //                       console.log('Cancel clicked');
  //                       success("-1")
  //                     }
  //                   },
  //                   {
  //                     text: openSettings,
  //                     handler: () => {
  //                       console.log("open sett");
  //                       this.diagnostic.switchToLocationSettings();
  
  //                     }
  //                   }
  //                 ]
  //               });
  //               alert.present();
              
  //           }
  //         );
  //       }
  
  //       else {
  //           let alert = this.alertCtrl.create({
  //             title: detetLoc,
  //             message: giveGPSPermission,
  //             buttons: [
  //               {
  //                 text: cancel,
  //                 role: 'cancel',
  //                 handler: () => {
  //                   console.log('Cancel clicked');
  //                   success("-1")
  //                 }
  //               },
  //               {
  //                 text: openSettings,
  //                 handler: () => {
  //                   console.log("open sett");
  //                   this.diagnostic.switchToSettings();
  
  //                 }
  //               }
  //             ]
  //           });
  //           alert.present();
         
  //       }
  //     });
  //   }
  
  // }
 
  geoLoc(success) {

    let LocationAuthorizedsuccessCallback = (isAvailable) => {
      //console.log('Is available? ' + isAvailable);
      if (isAvailable) {
        // this.locAlert = 0
        if (this.platform.is('android')) {
          this.diagnostic.getLocationMode().then((status) => {
            if (!(status == "high_accuracy")) {
              this.requestOpenGPS(success)
            }
            else {
              this.GPSOpened(success);
            }
          })
        }
        else {
          this.requestOpenGPS(success);
        }

      }
      else {
        this.requestOpenGPS(success);
      }
    };
    let LocationAuthorizederrorCallback = (e) => {
      console.error(e)
      this.requestOpenGPS(success);
    }
      ;
    this.diagnostic.isLocationAvailable().then(LocationAuthorizedsuccessCallback).catch(LocationAuthorizederrorCallback);

  }
  GPSOpened(success) {
    let optionsLoc = {}
    optionsLoc = { timeout: 30000, enableHighAccuracy: true, maximumAge: 3600 };
    this.geolocation.getCurrentPosition(optionsLoc).then((resp) => {
      //this.events.publish("locationEnabled")
      this.lat = resp.coords.latitude;
      this.lng = resp.coords.longitude;
      // this.userLocAccuracy = resp.coords.accuracy;
      let data = {
        inspectorLat: resp.coords.latitude,
        inspectorLong: resp.coords.longitude,
        inspectorLocAccuracy: resp.coords.accuracy
      }
      console.log("user loc : ",this.lat, " , ",this.lng)
      success(data);
    }
    ).catch((error) => {
      success("-1");
    });


  }
  requestOpenGPS(success) {
    if (this.platform.is('ios')) {
      this.diagnostic.isLocationEnabled().then(enabled => {
        if(enabled){
          this.diagnostic.getLocationAuthorizationStatus().then(status => {
            if (status == this.diagnostic.permissionStatus.NOT_REQUESTED) {
              this.diagnostic.requestLocationAuthorization().then(status => {
                if (status == this.diagnostic.permissionStatus.NOT_REQUESTED) {
                  this.locationAccuracy.canRequest().then(requested => {
                    if (requested) {
                      this.requestOpenGPS(success)
                    }
                    else {
                      success("-1")
                    }
  
                  }).catch(err => success("-1"))
                }
                else if (status == this.diagnostic.permissionStatus.GRANTED || status == this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE) {
                  this.GPSOpened(success);
                }
                else{
                  success("-1")
                }
              })
            }
            else if (status == this.diagnostic.permissionStatus.DENIED) {
              success("-1")
            }
            else if (status == this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE) {
              this.GPSOpened(success);
            }
          });
        }
        else{
          this.diagnostic.requestLocationAuthorization().then(val => {
            if (val == "GRANTED") {
              this.requestOpenGPS(success)
            }
            else {
              success("-1")
            }
          })
        }
      })
    }
    else {
      this.diagnostic.isLocationAuthorized().then(authorized => {
        if (authorized) {
          this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            if (canRequest) {
              this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                () => {
                  console.log('Request successful')

                  this.geoLoc(success);
                },
                error => {
                  console.log('Error requesting location permissions', error);

                  success("-1")
                }
              );
            }

            else {
              console.log('Error requesting location permissions');

              success("-1")
          
            }
          });
        }
        else {
          this.diagnostic.requestLocationAuthorization().then(val => {
            if (val == "GRANTED") {
              this.requestOpenGPS(success)
            }
            else {
              success("-1")
            }
          })
        }
      })
    }

  }

  getStatus(userId){
    firebase.database().ref(`users/${userId}/location`).on('value',(snap)=>{
      //alert("newOrder "+ snap.val())
      console.log("users status "+snap.val());
     
  
    });
  }
  createuser(userId,driverlocation,userLocation,thingLocation){
   
    console.log("createuser id :",userId," , location :",driverlocation,",userLocation : ",userLocation," ,thingLocation : ",thingLocation);
    var orderData = firebase.database().ref('users/');
    orderData.child(userId).set({location:driverlocation,userLoc:userLocation,thingLoc:thingLocation,next:0});
  
  }

  onlocationChanged(userId,successCallback){
    console.log("onlocationChanged user id : ",userId)
    // child_changed
    firebase.database().ref(`users/${userId}/location`).on('value',(snap)=>{
   
      console.log("onlocationChanged : ",snap.val())
      successCallback(snap.val())

    });
  }
  getUserLoc(userId,successCallback){
    console.log("userLoc user id : ",userId)
    // child_changed
    firebase.database().ref(`users/${userId}/userLoc`).on('value',(snap)=>{
   
      console.log("userLoc : ",snap.val())
      successCallback(snap.val())

    });
  }
  getThingLoc(userId,successCallback){
    console.log("thingLoc user id : ",userId)
    // child_changed
    firebase.database().ref(`users/${userId}/thingLoc`).on('value',(snap)=>{
   
      console.log("thingLoc : ",snap.val())
      successCallback(snap.val())

    });
  }

  updateLocation(userId,userLoc){
   

      firebase.database().ref().child(`users/${userId}`)
       .update({location:userLoc})
    
    
  }

  updatenext(userId){
   
    firebase.database().ref().child(`users/${userId}`)
     .update({next:1})
}

onnextChanged(userId,successCallback){
  console.log("onnextChanged user id : ",userId)
  // child_changed
  firebase.database().ref(`users/${userId}/next`).on('value',(snap)=>{
 
    console.log("onnextChanged : ",snap.val())
    successCallback(snap.val())

  });
}

  timer
  startTimerTogetLocation(driverId){
   this.timer =   setInterval(()=>{
      this.geoLoc((data)=>{
        console.log("location data :" ,data)
//         inspectorLat: 31.0657428
// inspectorLocAccuracy: 10.914999961853027
// inspectorLong: 31.6421449
var resp = JSON.parse(JSON.stringify(data))
this.updateLocation(driverId,resp.inspectorLat + " , "+resp.inspectorLong)
      })
    },2000)
    
  }

  removeUserObject(ID){
    console.log("remove user",ID);
    firebase.database().ref().child(`users/${ID}`)
     .remove();
  }
}
