import { HttpClient ,HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {HelperProvider } from '../helper/helper';
import { LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';


@Injectable()
export class ServiceProvider {
  
  
  constructor(public storage: Storage,public loading:LoadingController,public http: HttpClient, public helper:HelperProvider) {
    console.log('Hello ServiceProvider Provider');
   

  }
 

  register2(userData,successCallback, failureCallback){
    let loading = this.loading.create({
     
    }); 
  
    let params={
      'name' : userData.name,
      'phone': userData.phone,
      'password':userData.password,
      'img':userData.img,
      'roles':userData.roles,
      'email':userData.email,
      'id_img':userData.id_img,
      'id_car':userData.id_car,
      'car_type':userData.car_type,
      "car_num":userData.car_num,
      "criminal_img":userData.criminal_img
    }
    loading.present()
    let headers = new HttpHeaders();
   console.log(JSON.stringify(params))
  headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
  let serviceUrl = this.helper.serviceUrl + '/api/registration?name='+userData.name+"&phone=2"+ userData.phone+ "&password="+userData.password+
  "&img="+userData.img+"&roles="+userData.roles+"&email="+userData.email

      this.http.post(serviceUrl, { headers: headers})
      
        .subscribe(
          data => {
            loading.dismiss()
            successCallback(JSON.stringify(data))
          },
          err => {
            loading.dismiss()
            failureCallback(err)
          }
        )
    
  }


  register(userData,successCallback, failureCallback){
    let loading = this.loading.create({
     
    }); 
  
    let params={
      'name' : userData.name,
      'phone': userData.phone,
      'password':userData.password,
      'img':userData.img,
      'roles':userData.roles,
      'email':userData.email,
      'id_img':userData.id_img,
      'id_car':userData.id_car,
      'car_type':userData.car_type,
      "car_num":userData.car_num,
      "criminal_img":userData.criminal_img,
      "type":userData.type
    }
    loading.present()
    let headers = new HttpHeaders();
   console.log(JSON.stringify(params))
  headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
  let serviceUrl = this.helper.serviceUrl + '/api/registration'

      this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
      
        .subscribe(
          data => {
            loading.dismiss()
            successCallback(JSON.stringify(data))
          },
          err => {
            loading.dismiss()
            failureCallback(err)
          }
        )
    
  }

  login(userData,successCallback, failureCallback){
    // www.wasly.eg-target.com/api/login
    let loading = this.loading.create({
     
    }); 
  console.log("data from login:",userData)
    let params={
      'phone': userData.mobile,
      'password':userData.password
    }
    loading.present()
    let headers = new HttpHeaders();
   console.log(JSON.stringify(params))
  headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
  let serviceUrl = this.helper.serviceUrl + '/api/login'

      this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
      
        .subscribe(
          data => {
            loading.dismiss()
            successCallback(JSON.stringify(data))
          },
          err => {
            loading.dismiss()
            failureCallback(err)
          }
        )
    
  }

  
  activate(userData,successCallback, failureCallback){
  // verify_verificationcode
    // www.wasly.eg-target.com/api/login
    let loading = this.loading.create({
     
    }); 
  
    let params={
      'user_id':userData.id,
      'token':userData.code,
      "isverified":userData.isverified
    }
    loading.present()
    let headers = new HttpHeaders();
   console.log(JSON.stringify(params))
  headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
  let serviceUrl = this.helper.serviceUrl + '/api/verify_verificationcode'

      this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
      
        .subscribe(
          data => {
            loading.dismiss()
            successCallback(JSON.stringify(data))
          },
          err => {
            loading.dismiss()
            failureCallback(err)
          }
        )
    
  }

  resend(userData,successCallback, failureCallback){
    // verify_verificationcode
      // www.wasly.eg-target.com/api/login
      let loading = this.loading.create({
       
      }); 
    
      let params={
        'user_id':userData.id,
        'token':userData.code,
        "isverified":userData.isverified
      }
      loading.present()
      let headers = new HttpHeaders();
     console.log(JSON.stringify(params))
    headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
    let serviceUrl = this.helper.serviceUrl + '/api/verify_verificationcode_forget'
  
        this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
        
          .subscribe(
            data => {
              loading.dismiss()
              successCallback(JSON.stringify(data))
            },
            err => {
              loading.dismiss()
              failureCallback(err)
            }
          )
      
    }

    
  updateLocation(userData,successCallback, failureCallback){
    // www.wasly.eg-target.com/api/UpdateProfiles
    let loading = this.loading.create({
     
    }); 
  
    let params={
      'lat':userData.lat,
      'lng':userData.lng
    }
    loading.present()
    let headers = new HttpHeaders();
   console.log(JSON.stringify(params))
  headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
  let serviceUrl = this.helper.serviceUrl + '/api/UpdateProfiles'

      this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
      
        .subscribe(
          data => {
            loading.dismiss()
            successCallback(JSON.stringify(data))
          },
          err => {
            loading.dismiss()
            failureCallback(err)
          }
        )
    
  }

  searchForDrivers(userData,successCallback, failureCallback){
    // www.wasly.eg-target.com/api/driverSearch
    this.storage.get("token").then(val=>{
      console.log("val from token",val)
    })
    let loading = this.loading.create({
     
    }); 
  
    let params={
      'lat':userData.lat,
      'lng':userData.lng
    }
    loading.present()
    let headers = new HttpHeaders();
   console.log(JSON.stringify(params))
  headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
  let serviceUrl = this.helper.serviceUrl + '/api/Search'

      this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
      
        .subscribe(
          data => {
            loading.dismiss()
            successCallback(JSON.stringify(data))
          },
          err => {
            loading.dismiss()
            failureCallback(err)
          }
        )
    
  }

  driverOnlineOffline(userid,status,successCallback,failureCallback){
//0 off
//1 online
// online
//id
// changeOnline
let loading = this.loading.create({
     
}); 

let params={
  id:userid,
  online:status
}
loading.present()
let headers = new HttpHeaders();
console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
let serviceUrl = this.helper.serviceUrl + '/api/changeOnline'

  this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
  
    .subscribe(
      data => {
        loading.dismiss()
        successCallback(JSON.stringify(data))
      },
      err => {
        loading.dismiss()
        failureCallback(err)
      }
    )

  }
  getUserOrders(userid,successCallback, failureCallback){
    //www.wasly.eg-target.com/api/getOrdersUser
    let loading = this.loading.create({
     
    }); 
  
    let params={
      'user_id':userid
    }
    loading.present()
    let headers = new HttpHeaders();
   console.log(JSON.stringify(params))
  headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
  let serviceUrl = this.helper.serviceUrl + '/api/getOrdersUser'

      this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
      
        .subscribe(
          data => {
            loading.dismiss()
            successCallback(JSON.stringify(data))
          },
          err => {
            loading.dismiss()
            failureCallback(err)
          }
        )
    
  }
  
  getDriverOrders(userid,successCallback, failureCallback){
    //www.wasly.eg-target.com/api/getOrdersDriver 
    let loading = this.loading.create({
     
    }); 
  
    let params={
      'driver_id':userid
    }
    loading.present()
    let headers = new HttpHeaders();
   console.log(JSON.stringify(params))
  headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
  let serviceUrl = this.helper.serviceUrl + '/api/getOrdersDriver'

      this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
      
        .subscribe(
          data => {
            loading.dismiss()
            successCallback(JSON.stringify(data))
          },
          err => {
            loading.dismiss()
            failureCallback(err)
          }
        )
    
  }

  createOrder(userdata,successCallback, failureCallback){
    //www.wasly.eg-target.com/api/getOrdersUser
    let loading = this.loading.create({
     
    }); 
  
    let params={
      'driver_id':userdata.driver_id,
      "user_id":userdata.user_id,
      "description":userdata.description,
      "address":userdata.address,
      "lat":userdata.lat,
      "lng":userdata.lng,
      // "lat_user":
      // "lng_user":,
      // "driver_lat":",
      // "driver_lng":""
    }
    loading.present()
    let headers = new HttpHeaders();
   console.log(JSON.stringify(params))
  headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
  let serviceUrl = this.helper.serviceUrl + '/api/createOrder'

      this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
      
        .subscribe(
          data => {
            loading.dismiss()
            successCallback(JSON.stringify(data))
          },
          err => {
            loading.dismiss()
            failureCallback(err)
          }
        )
    
  }
 
  createnewOrder(userdata,successCallback, failureCallback){
    //www.wasly.eg-target.com/api/getOrdersUser
    let loading = this.loading.create({
     
    }); 
  
    let params={
      // 'driver_id':userdata.driver_id,
      "user_id":userdata.user_id,
      "description":userdata.description,
      "address":userdata.address,
      "lat":userdata.lat,
      "lng":userdata.lng,
      "lat_user":userdata.userlat,
      "lng_user":userdata.userlng,
      "type":userdata.type,
      "price":userdata.price
      // "driver_lat":",
      // "driver_lng":""
    }
    loading.present()
    let headers = new HttpHeaders();
   console.log(JSON.stringify(params))
  headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
  let serviceUrl = this.helper.serviceUrl + '/api/createOrder'

      this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
      
        .subscribe(
          data => {
            loading.dismiss()
            successCallback(JSON.stringify(data))
          },
          err => {
            loading.dismiss()
            failureCallback(err)
          }
        )
    
  }
  updateProfile(userdata,successCallback, failureCallback){
    //www.wasly.eg-target.com/api/getOrdersUser
    let loading = this.loading.create({
     
    }); 
  
    let params={
      'name':userdata.name,
      // "phone":userdata.user_id,
      "password":userdata.password,
      "email":userdata.email,
      "img":userdata.img,
      "roles":userdata.roles
      
    }
    loading.present()
    let headers = new HttpHeaders();
   console.log(JSON.stringify(params))
  headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
  let serviceUrl = this.helper.serviceUrl + '/api/updateProfile'

      this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
      
        .subscribe(
          data => {
            loading.dismiss()
            successCallback(JSON.stringify(data))
          },
          err => {
            loading.dismiss()
            failureCallback(err)
          }
        )
    
  }
jsActivationCode(JsSID,Code,successCallback,failureCallback){
  // 'activate?JsSID=1005&Code=123123'
  //var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
let serviceUrl = this.helper.serviceUrl + 'JS/activate?JsSID='+JsSID+'&Code='+Code;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
      },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
  
}

listDriverNotification(id,successCallback, failureCallback){
  // www.wasly.eg-target.com/api/UpdateProfiles
  let loading = this.loading.create({
   
  }); 

  let params={
    'id':id
  }
  loading.present()
  let headers = new HttpHeaders();
 console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
let serviceUrl = this.helper.serviceUrl + '/api/Driver_Notification'

    this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
  
}

acceptOrder(userData,successCallback,failureCallback){
  let loading = this.loading.create({
     
  }); 

  let params={
    'user_id' : userData.user_id,
    'driver_id': userData.Drvier_id
    // 'order_id':userData.order_id
  }
  loading.present()
  let headers = new HttpHeaders();
 console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
let serviceUrl = this.helper.serviceUrl +"/api/AcceptOrder"

    this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}
rejectOrder(userData,successCallback,failureCallback){
  let loading = this.loading.create({
     
  }); 

  let params={
    // 'Drvier_id' : userData.Drvier_id,
    // 'user_id':userData.user_id,
    'id':userData.order_id
  }
  loading.present()
  let headers = new HttpHeaders();
 console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
let serviceUrl = this.helper.serviceUrl +"/api/RejectOrder"

    this.http.post(serviceUrl, JSON.stringify(params),{ headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}
getAddress(user_id,successCallback,failureCallback){
  // getAddress

  let loading = this.loading.create({
     
  }); 
  // user_id
  let params={
    'user_id' : user_id
  }

  loading.present()
  let headers = new HttpHeaders();
headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
let serviceUrl = this.helper.serviceUrl +"/api/getAddress"

    this.http.post(serviceUrl, JSON.stringify(params),{ headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}


addAddress(userData,successCallback,failureCallback){
  let loading = this.loading.create({
     
  }); 

  let params={
    'name' : userData.name,
    'lat':userData.lat,
    'lng':userData.lng,
    'address':userData.address,
    'user_id':userData.user_id
  }
  loading.present()
  let headers = new HttpHeaders();
 console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
let serviceUrl = this.helper.serviceUrl +"/api/storeAddress"

    this.http.post(serviceUrl, JSON.stringify(params),{ headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
  
}

deleteAddress(userData,successCallback,failureCallback){
  let loading = this.loading.create({
     
  }); 

  let params={
    'addressId' : userData.addressId,
    'user_id':userData.user_id
  }
  loading.present()
  let headers = new HttpHeaders();
 console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
let serviceUrl = this.helper.serviceUrl +"/api/deleteAddress"

    this.http.post(serviceUrl, JSON.stringify(params),{ headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
  
}

getDriverNotification(driver_id,successCallback,failureCallback){
  // Driver_Notification
  let loading = this.loading.create({
     
  }); 

  let params={
    'driver_id' : driver_id
  }
  loading.present()
  let headers = new HttpHeaders();
headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
let serviceUrl = this.helper.serviceUrl +"/api/Driver_Notification"

    this.http.post(serviceUrl, JSON.stringify(params),{ headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}

// getUserNotification(successCallback,failureCallback){
//   // Driver_Notification
//   let loading = this.loading.create({
     
//   }); 


//   loading.present()
//   let headers = new HttpHeaders();
// headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
// let serviceUrl = this.helper.serviceUrl +"/Driver_Notification"

//     this.http.get(serviceUrl, { headers: headers})
    
//       .subscribe(
//         data => {
//           loading.dismiss()
//           successCallback(JSON.stringify(data))
//         },
//         err => {
//           loading.dismiss()
//           failureCallback(err)
//         }
//       )

// }
getDriverDetails(id,successCallback,failureCallback){
  // api\DriverDetails
  // id
  let loading = this.loading.create({
     
  }); 

  let params={
    
    'id':id
  }
  loading.present()
  let headers = new HttpHeaders();
 console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
let serviceUrl = this.helper.serviceUrl + '/api/DriverDetails'

    this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
 

}
getDurationAndDistance(sLat,sLon,dLat,dLon){
  //https://maps.googleapis.com/maps/api/directions/json?origin=31.0657632,31.6421222&destination=31.037933,31.381523
  var url = 'https://cors.io/?https://maps.googleapis.com/maps/api/directions/json?origin='+sLat+','+sLon+'&destination='+dLat+','+dLon+'&key='+this.helper.googlekey;
  console.log("googlw api url ",url);
  return this.http.get(url);
}
getaddress(lat,lng){
    
  var url = "https:maps.googleapis.com/maps/api/geocode/json?address="+lat+","+lng+"&key="+this.helper.googlekey;
  console.log("google api url ",url);
  return this.http.get(url);
}
endOrder(orderid,successCallback,failureCallback){
  // wasly.eg-target.com/api/EndOrder
  // id = order


  // 'value' => 'required',
  //                'user_id' => 'required',
  //                'driver_id' => 'required',

  //                wasly.eg-target.com/api/Store_rate

  //                post

  let loading = this.loading.create({
     
  }); 

  let params={
    
    'id':orderid
  }
  loading.present()
  let headers = new HttpHeaders();
 console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
let serviceUrl = this.helper.serviceUrl + '/api/EndOrder'

    this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
 

}
rateOrder(desc,rate,userId,driverId,successCallback,failureCallback)
{
let loading = this.loading.create({
     
  }); 

  //'value' => 'required',
  //                'user_id' => 'required',
  //                'driver_id' => 'required',

  let params={
    
    'value':rate,
    'user_id':userId,
    'driver_id':driverId,
    'description':desc
  }
  loading.present()
  let headers = new HttpHeaders();
 console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
let serviceUrl = this.helper.serviceUrl + '/api/Store_rate'

    this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
 
}

firebaseregistration(id,token,successCallback,failureCallback)
{
// let loading = this.loading.create({
     
//   }); 


  let params={
    
    'id':id,
    'fcm_token':token
  }
  // loading.present()
  let headers = new HttpHeaders();
 console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
let serviceUrl = this.helper.serviceUrl + '/api/updatedFCm_Token'

    this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
    
      .subscribe(
        data => {
          // loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          // loading.dismiss()
          failureCallback(err)
        }
      )
 
}


updateDriverLocation(id,lat,lng,successCallback,failureCallback)
{
  // http://wasly2.eg-target.com/api/updateLocation

  let params={
    
    'id':id,
    'lat':lat,
    'lng':lng
  }
  
  let headers = new HttpHeaders();
 console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json')//.set('token',newtoken);
let serviceUrl = this.helper.serviceUrl + '/api/updateLocation'

    this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
    
      .subscribe(
        data => {
          
          successCallback(JSON.stringify(data))
        },
        err => {
          
          failureCallback(err)
        }
      )
 
}

}
