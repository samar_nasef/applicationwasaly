import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
// import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage} from '../pages/login/login';
import { DriverHomePage } from '../pages/driver-home/driver-home'
// import { DashboardPage } from '../pages/dashboard/dashboard';
// import { SetupPage } from '../pages/setup/setup';
// import { SitePage } from '../pages/site/site';
// import { ScanTrapPage } from '../pages/scan-trap/scan-trap';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { NgxQRCodeModule } from 'ngx-qrcode2';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Camera } from '@ionic-native/camera';
import { HelperProvider } from '../providers/helper/helper';
import { HttpClient ,HttpClientModule} from '@angular/common/http';
import { ServiceProvider } from '../providers/service/service';
// import { Geolocation } from '@ionic-native/geolocation';
// import { AES256 } from '@ionic-native/aes-256';
import { IonicStorageModule } from '@ionic/storage';
import { Serial } from '@ionic-native/serial';

import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';


import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { DatePipe } from '@angular/common';

import { LoginOrRegisterPage } from '../pages/login-or-register/login-or-register';
import {
  GoogleMaps
} from '@ionic-native/google-maps';
import { PusherServiceProvider } from '../providers/pusher-service/pusher-service';
import { UserHomePage } from '../pages/user-home/user-home';
import { LocationPusherProvider } from '../providers/location-pusher/location-pusher';
import { Broadcaster } from '@ionic-native/broadcaster/ngx';
// import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Ionic2RatingModule } from "ionic2-rating";

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    // ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    LoginOrRegisterPage,
    DriverHomePage,
    UserHomePage
    // DashboardPage,
    // SetupPage,
    // SitePage,
    // ScanTrapPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    NgxQRCodeModule,
    HttpClientModule,
    IonicStorageModule.forRoot({ name: '__mydb2', driverOrder: ['sqlite', 'websql', 'indexeddb'] } ),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    Ionic2RatingModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    // ContactPage,
    HomePage,
    TabsPage,
    LoginOrRegisterPage,
    LoginPage,
    DriverHomePage,
    UserHomePage
    // DashboardPage,
    // SetupPage,
    // SitePage,
    // ScanTrapPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BarcodeScanner,
    Camera,
    HelperProvider,
    HttpClient,
    ServiceProvider,
    Serial,
    Diagnostic, 
    LocationAccuracy,
    Geolocation,
    [DatePipe],
    GoogleMaps,
    PusherServiceProvider,
    LocationPusherProvider,
    Broadcaster,
    // BackgroundGeolocation
    // AES256,
    // Geolocation
  ]
})
export class AppModule {}
