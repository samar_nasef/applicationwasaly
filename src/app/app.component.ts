import { Component,ViewChild } from '@angular/core';
import { Platform,NavController,App,Events ,Nav} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { Storage } from '@ionic/storage';
import { HelperProvider } from '../providers/helper/helper';
import { DriverHomePage } from '../pages/driver-home/driver-home';
import { TranslateService } from '@ngx-translate/core';

import * as firebase from 'firebase/app';
import { LoginOrRegisterPage } from '../pages/login-or-register/login-or-register';

import { HomePage } from '../pages/home/home';
import { UserHomePage } from '../pages/user-home/user-home';
import { Push, PushObject, PushOptions } from '@ionic-native/push';


// var firebaseConfig = {
//   apiKey: "AIzaSyCO000hx8tHemiNN0gA99GZoIUL3LqN-_4",
//   authDomain: "wasaly-cd486.firebaseapp.com",
//   databaseURL: "https://wasaly-cd486.firebaseio.com",
//   projectId: "wasaly-cd486",
//   storageBucket: "wasaly-cd486.appspot.com",
//   // messagingSenderId: "381921023811"
//   messagingSenderId:  "546879395480"
// };

var firebaseConfig = {
  apiKey: "AIzaSyCDDGercVM0NsNPLXU7nN0QCqWYNwYyGyI",
  authDomain: "wasaly-40c3c.firebaseapp.com",
  databaseURL: "https://wasaly-40c3c.firebaseio.com",
  projectId: "wasaly-40c3c",
  storageBucket: "wasaly-40c3c.appspot.com",
  // messagingSenderId: "381921023811"
  messagingSenderId:  "898723461501"
};


@Component({
  templateUrl: 'app.html',
  providers:[Push]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any ;
  menu2 
  userLoged = false
  userImageUrl = "assets/imgs/default-avatar.png"
  userName = ""
  langDirection

  // #2e434e kalam 

  //#2e434e 
// دي الدرجة ال تجت في siderbar

// #324852
// و دي الدرجة ال فوق

//user 12345625412,123456 ,.....,phone: "+212345678912" , 123456
//user 4545 ..0 , 1234
//user 123456789
//driver 01085478965,123456
  
constructor( private push: Push,public events: Events,  public helper:HelperProvider,public storage:Storage,public translate: TranslateService,public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
     
      statusBar.styleDefault();
      statusBar.backgroundColorByHexString("#30414c");
      splashScreen.hide();
      this.initializeLang();
      this.initPage();
          firebase.initializeApp(firebaseConfig);
          this.pushnotification();
    });
 

 

    events.subscribe('user:userLoginSucceeded', (userData) => {
      console.log("suscribe userLoginSucceeded",userData)
      
      this.userLoged=true;
      if(userData.type=="user")
        this.menu2=true;
      else
        this.menu2=false;


      if(userData.type=="user")
      {
        console.log("user",userData)
        this.userName = userData.name;
        this.userImageUrl ="assets/imgs/default-avatar.png";
      } 
      else if(userData.type=="driver")
      {
        console.log("driver",userData)
        this.userName = userData.name;
        this.userImageUrl ="assets/imgs/default-avatar.png";

      }

    });

    // events.subscribe('user:userLogedout', () => {
    //   this.userLoged = false;
    //   this.logout();
    //   console.log("user loged out")
    // });

  }

  initPage(){
    
    this.storage.get('info').then((val) => {
      console.log("val from get info",val);
      if(val)
      {   
        if(val.type=="user")
        {
          this.menu2=true;
          this.userLoged=true;
          // this.rootPage = HomePage;
          this.rootPage = UserHomePage;
          this.userName = val.alldata.name;
          this.userImageUrl = "assets/imgs/default-avatar.png";
        } 
        else if(val.type=="driver")
        {
          this.menu2=false;
          this.userLoged=true;
          // this.rootPage = HomePage;
          this.rootPage = DriverHomePage;
          this.userName =val.alldata.name;
          this.userImageUrl = "assets/imgs/default-avatar.png";

        }

        
      }  
      else
        this.rootPage = LoginOrRegisterPage; 
    }).catch(err=>{
      console.log("catch from get info",err);
    });
}

initializeLang() {
 
  this.storage.get('wasaly_language').then((val) => {
    if(val){
      console.log("lang value detected",val);
      
      // val = 'ar'
      
      if(val == 'ar'){
        this.translate.setDefaultLang('ar');  
        this.translate.use('ar');    
        this.platform.setDir('rtl', true);
        this.helper.lang_direction = "rtl";   
      

      }else{
        console.log("form initializeLang: ",val);
        this.translate.setDefaultLang('en');
        this.translate.use('en');
        this.platform.setDir('ltr', true);
        this.helper.lang_direction = "ltr";
      
      }
    }else{
      console.log("lang value not detected",val);
      var userLang = navigator.language.split('-')[0];
      console.log("navigator.language",navigator.language);
      console.log("userlang",userLang);
      
    // userLang = 'ar';

      if (userLang == 'ar') {
        this.translate.setDefaultLang('ar');  
        this.translate.use('ar');    
        this.platform.setDir('rtl', true);
        this.helper.lang_direction = "rtl";  
        this.helper.storeLanguage('ar') 
        
      }else {
            this.translate.setDefaultLang('en');
            this.translate.use('en');
            this.platform.setDir('ltr', true);
            this.helper.lang_direction = "ltr";
            this.helper.storeLanguage('en')
      }

    }
  });



}
openHome(){
  // this.nav.setRoot(HomePage)
  this.nav.setRoot(UserHomePage)
}
opendriverHome(){
  this.nav.setRoot(DriverHomePage)
}
openProfile(){
  this.nav.setRoot('ProfilePage',{from:"user"})
}
opendriverProfile(){
  this.nav.setRoot('ProfilePage',{from:"driver"})
}
openOrders(){
  this.nav.setRoot('OrdersPage',{from:"user"})
}
opendriverOrders(){
  this.nav.setRoot('OrdersPage',{from:"driver"})
}
opendriveNotifications(){
  this.nav.setRoot('NotificationsPage',{from:"driver"})
}
openNotifications(){
  this.nav.setRoot('NotificationsPage',{from:"user"})
}
opensearch(){

}
logout(){
  console.log("logout")
  this.storage.remove('wasaly_language')
  this.storage.remove('info').then(()=>{
    console.log("info removed");
    this.userLoged = false;
    this.nav.setRoot(LoginOrRegisterPage); 
  });
}
opensettings(){
  console.log("openSetting")
  this.nav.setRoot('SettingsPage',{from:"user"})
}
opendriversetting(){
  console.log("open driver Setting")
  this.nav.setRoot('SettingsPage',{from:"driver"})
}

openaddresshistory(){
  this.nav.setRoot('AddressHistoryPage')
}
pushnotification() {
  let options: PushOptions
    options = {
      android: {
        // forceShow:true
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      }
    }
  const pushObject: PushObject = this.push.init(options);
  pushObject.on('notification').subscribe((notification: any) => {
    console.log("notification " + JSON.stringify(notification))
    
    if (this.platform.is('ios')) {
      console.log("ios notification",notification);
      alert("ios notification "+JSON.stringify(notification))
    //           if(notification.additionalData["gcm.notification.redirectTo"] == "3")
    //   {
    //     //js hire goto active jobs
    //     this.nav.setRoot(TabsPage);
    //     this.nav.push('ActiveJobsPage');
    //   }else if(notification.additionalData["gcm.notification.redirectTo"] == "1")
    //   {
    //     //comp  js checkin apear view to accept or reject
    //     this.nav.setRoot(TabsPage);
    //     this.nav.push('CompanyCheckInNotificationPage',{notificationType:"checkIn",jsid:notification.additionalData["gcm.notification.seeker"],jobId:notification.additionalData["gcm.notification.jobID"]});
  
    //   }else if(notification.additionalData["gcm.notification.redirectTo"] == "2")
    //   {
    //     //comp  js checkout apear view to accept or reject
    //     this.nav.setRoot(TabsPage);
    //     this.nav.push('CompanyCheckInNotificationPage',{notificationType:"checkout",jsid:notification.additionalData.seeker,jobId:notification.additionalData.jobID});
  
    //   }else if(notification.additionalData["gcm.notification.redirectTo"] == "5")
    //   {
    //     //js  comp accept checkin 
    //     this.nav.setRoot(TabsPage);
    //     this.nav.setRoot(TabsPage);
    //     this.nav.push('ActiveJobsPage');
  
    //   }else if(notification.additionalData["gcm.notification.redirectTo"] == "6")
    //   {
    //     //js  comp reject checkin 
    //     this.nav.setRoot(TabsPage);
    //     this.nav.setRoot(TabsPage);
    //         this.nav.push('ActiveJobsPage');
  
    //   }else if(notification.additionalData["gcm.notification.redirectTo"] == "4")
    //   {
    //     //comp  js cancelJOb 
    //     this.nav.setRoot(TabsPage);
    //     this.nav.push('CompanyCheckInNotificationPage',{notificationType:"cancel",jsid:notification.additionalData["gcm.notification.seeker"],jobId:notification.additionalData["gcm.notification.jobID"]});
  
    //   }else if(notification.additionalData["gcm.notification.redirectTo"] == "7" || notification.additionalData["gcm.notification.redirectTo"] == "8")
    //   {
    //     //confirm company for js late or ontime
    //     this.nav.setRoot(TabsPage);
    // //     var newjobname = "";
    // // if(this.langDirection == "rtl")
    // // newjobname = item.JobTitleAr;
    // // else if(this.langDirection == "ltr")
    // // newjobname = item.JobTitleEn;
    // this.nav.push("CompanyselectedusersPage",{jobId:notification.additionalData["gcm.notification.jobID"],jobname:""})

    //   }else if(notification.additionalData["gcm.notification.redirectTo"] == "9" )
    //   {
    //    //comp  js apply open postedjobs
    //    this.nav.setRoot(TabsPage);
    //    // this.nav.push('CompanyCheckInNotificationPage',{notificationType:"checkIn",jsid:notification.additionalData.seeker,jobId:notification.additionalData.jobID});
    //    this.nav.push(CompanypostedjobPage,{from:"notification"});
    //   }

      
    }else {
      console.log("notification from android",notification);
      // alert("android notification "+JSON.stringify(notification))
      // if(notification.additionalData.driverOrUser == "user"){
        if(notification.additionalData.type == "3"){
          // this.storage.get('info').then(val=>{
          //   console.log("user ",val)
            // if(notification.additionalData.user_id == val.alldata.id){
              console.log(" if ==")
              // notification.additionalData.Notifcation ===  order id
              // if(this.langDirection == "rtl")
              //   this.scheduleNotification(this.translate.instant("orderAccepted2"),notification.additionalData.driver_id,notification.additionalData.Notifcation)
              // else if(this.langDirection == "ltr")
              //   this.scheduleNotification(this.translate.instant("orderAccepted2"),newData.driver_id,newData.Notifcation)

              this.events.publish('user:Cleartimer');
                this.nav.setRoot("MapPage",{driver_id:notification.additionalData.driver_id,order_id:notification.additionalData.Order})

            //   }else{
            //   console.log(" else ==")
            // }
          // });
          
        }
        else if(notification.additionalData.type == "4"){
          // this.storage.get('info').then(val=>{
          //   console.log("user ",val)
          //   if(notification.additionalData.user_id == val.alldata.id){
          //     console.log(" if ==")
          //     // notification.additionalData.Notifcation ===  order id
          //     // if(this.langDirection == "rtl")
          //     //   this.scheduleNotification(this.translate.instant("orderAccepted2"),notification.additionalData.driver_id,notification.additionalData.Notifcation)
          //     // else if(this.langDirection == "ltr")
          //     //   this.scheduleNotification(this.translate.instant("orderAccepted2"),newData.driver_id,newData.Notifcation)

          //       this.nav.setRoot("MapPage",{driver_id:notification.additionalData.driver_id,order_id:notification.additionalData.Notifcation})

          //     }else{
          //     console.log(" else ==")
          //   }
          // });
          this.nav.setRoot(DriverHomePage)
        }
        else if(notification.additionalData.type == "1"){
          this.storage.get('info').then(val=>{
            console.log("user ",val)
            if(notification.additionalData.driver_id == val.alldata.id){
              console.log(" if ==")
            //   if(this.langDirection == "rtl")
            //   this.scheduleNotification(newData.Notifcation.ar_title,newData.Notifcation.order_id,newData.Notifcation.description,newData.Notifcation.user_id,newData.driver_id,newData.Order.lat,newData.Order.lng,newData.Notifcation.price,newData.Notifcation.type,newData.Order.lat_user,newData.Order.lng_user)
            // else if(this.langDirection == "ltr")
            //   this.scheduleNotification(newData.Notifcation.en_title,newData.Notifcation.order_id,newData.Notifcation.description,newData.Notifcation.user_id,newData.user_id,newData.Order.lat,newData.Order.lng,newData.Notifcation.price,newData.Notifcation.type,newData.Order.lat_user,newData.Order.lng_user)
  
            // scheduleNotification(txt,orderId,desc,userid,driver_id,lat1,lng1,price,type,userLat,userLng) {

              var xdata= {orderId : notification.additionalData.order_id,desc: notification.additionalData.description,userId:notification.additionalData.user_id,driverId:notification.additionalData.driver_id,lat:notification.additionalData.lat,lng:notification.additionalData.lng,price:notification.additionalData.price,type:notification.additionalData.type,userLat:notification.additionalData.lat_user,userLng:notification.additionalData.lng_user}
    console.log("xdata: ",xdata)
    this.nav.setRoot("ShowOrderDetailsPage",xdata)


              }else{
              console.log(" else ==")
            }
          });
          
        }
      // }else if(notification.additionalData.driverOrUser == "driver"){


      // }

      //   if(notification.additionalData.redirectTo == "3")
    //   {
    //     //js hire goto active jobs
    //     console.log("from redirectTo 3")
    //     this.nav.setRoot(TabsPage);
    //     this.nav.push('ActiveJobsPage');
    //   }else if(notification.additionalData.redirectTo == "1")
    //   {
    //     //comp  js checkin apear view to accept or reject
    //     this.nav.setRoot(TabsPage);
    //     this.nav.push('CompanyCheckInNotificationPage',{notificationType:"checkIn",jsid:notification.additionalData.seeker,jobId:notification.additionalData.jobID});
  
    //   }else if(notification.additionalData.redirectTo == "2")
    //   {
    //     //comp  js checkout apear view to accept or reject
    //     this.nav.setRoot(TabsPage);
    //     this.nav.push('CompanyCheckInNotificationPage',{notificationType:"checkout",jsid:notification.additionalData.seeker,jobId:notification.additionalData.jobID});
  
    //   }else if(notification.additionalData.redirectTo == "5")
    //   {
    //     //js  comp accept checkin 
    //     this.nav.setRoot(TabsPage);
    //     this.nav.setRoot(TabsPage);
    //     this.nav.push('ActiveJobsPage');
  
    //   }else if(notification.additionalData.redirectTo == "6")
    //   {
    //     //js  comp reject checkin 
    //     this.nav.setRoot(TabsPage);
    //     this.nav.setRoot(TabsPage);
    //         this.nav.push('ActiveJobsPage');
  
    //   }else if(notification.additionalData.redirectTo == "4")
    //   {
    //     //comp  js cancelJOb 
    //     this.nav.setRoot(TabsPage);
    //     this.nav.push('CompanyCheckInNotificationPage',{notificationType:"cancel",jsid:notification.additionalData.seeker,jobId:notification.additionalData.jobID});
  
    //   }else if(notification.additionalData.redirectTo == "7" || notification.additionalData.redirectTo == "8")
    //   {
    //     //confirm company for js late or ontime
    //     this.nav.setRoot(TabsPage);
    // //     var newjobname = "";
    // // if(this.langDirection == "rtl")
    // // newjobname = item.JobTitleAr;
    // // else if(this.langDirection == "ltr")
    // // newjobname = item.JobTitleEn;
    // this.nav.push("CompanyselectedusersPage",{jobId:notification.additionalData.jobID,jobname:""})

    //   }else if(notification.additionalData.redirectTo == "9")
    //   {
    //     //comp  js apply open postedjobs
    //     this.nav.setRoot(TabsPage);
    //    // this.nav.push('CompanyCheckInNotificationPage',{notificationType:"checkIn",jsid:notification.additionalData.seeker,jobId:notification.additionalData.jobID});
    //    this.nav.push(CompanypostedjobPage,{from:"notification"});
    //   }

    //   // 1,2,4 com \n 3 js \n 5,6 js accept , reject 
    //   // if(notification.additionalData.type == "js")
    //   // {
    //   //   // hire =>1
    //   //   if(notification.additionalData.status == "1")
    //   //   {
    //   //     this.nav.setRoot(TabsPage);
    //   //     this.nav.push('ActiveJobsPage');
    //   //   }




    //   // }else if(notification.additionalData.type == "comp"){
    //   //   //candidtate check in => 1
    //   //   //candidtate check out => 2
    //   //   if(notification.additionalData.status == "1"){
    //   //     this.nav.setRoot(TabsPage);
    //   //     this.nav.push('CompanyCheckInNotificationPage',{jsid:1047,jobId:notification.additionalData.jobID});

          
    //   //   }else if(notification.additionalData.status == "1"){

    //   //   }




    //   // }
          
    }
 
  });
  pushObject.on('registration').subscribe((registration: any) => {
    console.log("registraion : ",registration);
    console.log("registrationId " + registration.registrationId)
    this.helper.registration = registration.registrationId;


    if(localStorage.getItem("firebaseRegNoti")){
      if(localStorage.getItem("firebaseRegNoti") == registration.registrationId){
        localStorage.setItem("regChanged","0")
      }
      else{
        localStorage.setItem("regChanged","1")
        localStorage.setItem("firebaseRegNoti",registration.registrationId)
      
      }
    }


  });

  pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
}

}
