import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddressHistoryPage } from './address-history';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AddressHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(AddressHistoryPage),
    TranslateModule.forChild()
  ],
})
export class AddressHistoryPageModule {}
