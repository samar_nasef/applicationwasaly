import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServiceProvider } from '../../providers/service/service';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
  selector: 'page-address-history',
  templateUrl: 'address-history.html',
})
export class AddressHistoryPage {

  scaleClass="";
langDirection = "";

noorders
// addressArr=[{title:"title",address:"address",id:1},{title:"title",address:"address",id:2}]
addressArr=[]
myid

  constructor( public translate: TranslateService,public storage:Storage,public srv:ServiceProvider, public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    console.log("langDirection from complete order :",this.langDirection)
     
    if(this.langDirection == "rtl"){
      this.scaleClass="scaleClassrtl";
      // this.alertdir = "rtldir"
    }
    else{
      this.scaleClass="scaleClassltr";
      // this.alertdir = "ltrdir"
    }
      
    this.noorders = true
  }
 
  ionViewDidEnter(){
    console.log("ionViewDidEnter AddressHistoryPage")
    this.storage.get('info').then(val=>{
      console.log("user ",val)
      this.myid = val.alldata.id
      this.srv.getAddress(this.myid,resp=>{
        console.log("resp from getAddress : ",resp)
        this.addressArr = JSON.parse(resp).address
        if(this.addressArr.length <=0 )
          this.noorders = false
        else
        this.noorders = true

      },err=>{
        console.log("err from getAddress : ",err)
      })
    });
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddressHistoryPage');
  }
  dismiss(){
    this.navCtrl.pop()
  }
  add(){
    console.log("add ")
    var newItem = {placename:"",lat:31.0470131,lng:31.37165749999997}
    this.navCtrl.push("AddEditAddressPage",{from:"add",data:newItem})
  }
  edit(item){
    // var newItem = {placename:"any place",lat:31.0470131,lng:31.37165749999997}
    console.log("edit item: ",item)

    this.navCtrl.push("AddEditAddressPage",{from:"edit",data:item})
  }
  remove(item){
    console.log("remove item: ",item)
    let params={
      'addressId' : item.id,
      'user_id':this.myid
    }
    this.srv.deleteAddress(params,resp=>{
      console.log("resp from deleteAddress: ",resp)
      this.helper.presentToast(this.translate.instant("removedAddressSu"))
      this.storage.get('info').then(val=>{
        console.log("user ",val)
        this.myid = val.alldata.id
        this.srv.getAddress(this.myid,resp=>{
          console.log("resp from getAddress : ",resp)
          this.addressArr = JSON.parse(resp).address
          if(this.addressArr.length <=0 )
            this.noorders = false
          else
          this.noorders = true
  
        },err=>{
          console.log("err from getAddress : ",err)
        })
      });
    },err=>{
      console.log("err from deleteAddress: ",err)
      
    })

  }
}
