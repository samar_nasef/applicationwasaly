import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';


import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { HelperProvider } from '../../providers/helper/helper';
import { HomePage } from '../home/home';
import { ServiceProvider } from '../../providers/service/service';
import { Storage } from '@ionic/storage';
import { DriverHomePage } from '../driver-home/driver-home';
import { UserHomePage } from '../user-home/user-home';

@IonicPage()
@Component({
  selector: 'page-activation-code',
  templateUrl: 'activation-code.html',
})
export class ActivationCodePage {

  private registerForm : FormGroup;
  code 
  mobile
  codeErr
  codetxt
  submitAttempt = false;
  langDirection = "";
  timer;
  time=60;
  accountType

activationcode 
id
xcode
password
  constructor(public events:Events, public storage: Storage,public srv:ServiceProvider, private formBuilder: FormBuilder,public helper:HelperProvider, public translate: TranslateService,public navCtrl: NavController, public navParams: NavParams) {
    this.mobile = this.navParams.get("mobile")
    this.accountType = this.navParams.get("type")
this.xcode = this.navParams.get("activation")
this.id = this.navParams.get("id")
this.password = this.navParams.get("password")
console.log("activation code ",this.xcode)
    //driver, user
    this.langDirection = this.helper.lang_direction;
    this.registerForm = this.formBuilder.group({
     
      code: ['', Validators.compose([Validators.required,Validators.pattern("[0-9]{4}")])]
    });

    this.codetxt = this.translate.instant("codeTxt")
    this.codeErr = this.translate.instant("codeErr")
    this.time=60;
    this.enableTimer();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivationCodePage');
  }
  activatecode22(){
    if(! this.registerForm.valid ){
      this.submitAttempt=true;
  
      if(this.registerForm.controls["code"].errors){
        if(this.registerForm.controls["code"].errors['required'])
        {
          this.codeErr = this.translate.instant("codeErr");
        }else if(this.registerForm.controls["code"].errors['pattern']) {
          this.codeErr = this.translate.instant("invalidcode");
        }else{
          console.log("code errors:",this.registerForm.controls["code"].errors);
        }
      }
  
    }else{
     var data = {mobile : this.mobile,code:this.code,id:this.id ,isverified:0}
     console.log("valid data : ",data)
     console.log("call api for activate ")
     var datax = {password:this.password,mobile:this.mobile}
     console.log("code :",this.code ,"xcode :",this.xcode)
  if(this.code == this.xcode){
       data.isverified = 1
      this.srv.activate(data,resp=>{
        console.log("resp from activate",resp)
        var respdata = JSON.parse(resp)
        console.log("resp .code ",respdata.code)
        if(respdata.code == 200){
          clearTimeout(this.timer);
          console.log("code 200")
          // token
          this.srv.login(datax,resp=>{
            console.log("resp from login",resp)
            var respdata = JSON.parse(resp)
            if(respdata.code == 300){
              this.helper.presentToast(this.translate.instant("connectionErr"))
            }
            else if(respdata.code == 200){
              // 0109963883
      //token
      this.storage.set("token",respdata.token)
              if(respdata.userdata.roles == "2"){
                this.events.publish('user:userLoginSucceeded',{type:"user",name:respdata.userdata.name});
                this.helper.storeInfo({type:"user",alldata:respdata.userdata})
                // this.navCtrl.setRoot(HomePage)
                this.navCtrl.setRoot(UserHomePage)
              }if(respdata.userdata.roles == "3"){
                this.events.publish('user:userLoginSucceeded',{type:"driver",name:respdata.userdata.name});
                this.helper.storeInfo({type:"driver",alldata:respdata.userdata})
                this.navCtrl.setRoot(DriverHomePage)
              }
             
      
               
      
         
         
            }
           
          },err=>{
            console.log("err from login",err)
          })

          // this.storage.set("token",respdata.token)
          // if(this.accountType == "user")
          //   this.navCtrl.setRoot(HomePage)
          // else if(this.accountType == "driver")
          //   this.navCtrl.setRoot(DriverHomePage)
        }else if(respdata.code == "300"){
          clearTimeout(this.timer);
      this.helper.presentToast(this.translate.instant("codeerr"))
          
        }


       },err=>{
         console.log("err from activate",err)
       })
      
     }else{
      this.helper.presentToast(this.translate.instant("codeerr"))
     }
     
   
      
    }
  }

  fbLogin(){

  }
  resendcode(){
    console.log("resend code ");
    if(this.time>0){
      this.helper.presentToast(this.translate.instant("wait")+this.time+this.translate.instant("sec"));
    }else if(this.time == 0){
      this.time = 60;
      this.enableTimer(); 
      var data = {mobile : this.mobile,code:this.code,id:this.id ,isverified:0}
      console.log("valid data : ",data)
      console.log("call api for activate ")
      
      if(this.code == this.xcode){
        data.isverified = 1
       this.srv.activate(data,resp=>{
         console.log("resp from activate",resp)
         var respdata = JSON.parse(resp)
         if(resp.code == "200"){
           clearTimeout(this.timer);
          //  this.navCtrl.setRoot(HomePage)
          if(this.accountType == "user")
          this.navCtrl.setRoot(UserHomePage)
          // this.navCtrl.setRoot(HomePage)
          else if(this.accountType == "driver")
          this.navCtrl.setRoot(DriverHomePage)   
         }else if(resp.code == "300"){
           clearTimeout(this.timer);
       this.helper.presentToast(this.translate.instant("codeerr"))
           
         }
 
 
        },err=>{
          console.log("err from activate",err)
        })
       
      }else{
       this.helper.presentToast(this.translate.instant("codeerr"))
      }
  
       
      console.log("call api for resnd")
    }
  }

  enableTimer(){
    this.timer =setInterval(()=>{
      this.time--;
        if(this.time <= 0){
          console.log("timer off");
       
          clearTimeout(this.timer);
        }
    },1000);
  }


}
