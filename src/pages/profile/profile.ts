import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HelperProvider } from '../../providers/helper/helper';
import { ServiceProvider } from '../../providers/service/service';
import { Storage } from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  scaleClass="";
  scaleClassediticon="";
  langDirection = "";
  userImageUrl = "assets/imgs/default-avatar.png"
  item = {email:"",mobile:"",id:2,name:"",carno:"",carType:"",img:"assets/imgs/default-avatar.png"}
  nametra
  cartypetxttra
  carnotxttra
  mobiletra

  type
  hidedriverdata
  // name 
  // email
  // drivercarno
  // drivercartype

  // phone
emailtra
  constructor(public storage: Storage,public srv:ServiceProvider, public helper:HelperProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    console.log("langDirection from home :",this.langDirection)
         
    this.type = this.navParams.get("from")
    if(this.type == "user"){
      this.hidedriverdata = true
    }else if(this.type == "driver"){
      this.hidedriverdata = false
    }
    console.log("type from orders : ",this.type)

        if(this.langDirection == "rtl"){
          this.scaleClass="scaleClassrtl";
          this.scaleClassediticon = "scaleClassediticonrtl"
          this.nametra = "الإسم"
          this.cartypetxttra = "نوع السيارة"
          this.carnotxttra = "رقم السيارة"
          this.mobiletra = "رقم الهاتف"
          this.emailtra = "الإيميل"
        }
        else{
          this.scaleClass="scaleClassltr";
          this.scaleClassediticon = "scaleClassediticonltr"
          this.nametra = "Name"
          this.cartypetxttra = "Car Type"
          this.carnotxttra = "Car Number"
          this.mobiletra = "Mobile Number" 
          this.emailtra = "Email"

        }
          
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  ionViewDidEnter(){
    console.log('ionViewDidEnter ProfilePage');
    this.storage.get('info').then(val=>{
      console.log("val from info :",val)
      // val.alldata
      this.item.name = val.alldata.name
      this.item.email = val.alldata.email
      this.item.mobile = val.alldata.phone
      this.item.carno = val.alldata.car_num
      this.item.carType =   val.alldata.carType
    
   })
  }
  editProfile(){
    console.log("editProfile")
    this.navCtrl.push("EditProfilePage",{from:this.type,data:this.item})
  }
}
