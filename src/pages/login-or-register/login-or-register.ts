import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';

/**
 * Generated class for the LoginOrRegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-login-or-register',
  templateUrl: 'login-or-register.html',
})
export class LoginOrRegisterPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginOrRegisterPage');
  }

  login(){
    console.log("login")
    // this.navCtrl.push('LoginAndRegisterPage',{from:"login"})
    // this.navCtrl.push('LoginPage')
    this.navCtrl.push(LoginPage)
  }
  register(){
    console.log("register")
    // this.navCtrl.push('LoginAndRegisterPage',{from:"register"})
    this.navCtrl.push("RegisterPage")
  }
  fbLogin(){
    console.log("fbLogin")  
  }

  driverHome(){
    console.log("driver home")
    this.navCtrl.push('DriverHomePage')
  }
  map(){
    console.log("map")
    this.navCtrl.push('MapPage')
  }
}
