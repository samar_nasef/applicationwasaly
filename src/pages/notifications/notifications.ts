import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServiceProvider } from '../../providers/service/service';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {
  scaleClass="";
  langDirection = "";
  noNotifications
  notificationsArr=[]
  // notificationsArr=[{color:"#abacae",notificationimage:"assets/icon/notification.png",txt:"txt",desc:"desc"},
  // {color:"#3fb875",notificationimage:"assets/icon/chat-bubble.png",txt:"txt",desc:"desc"},
  // {color:"#418ccb",notificationimage:"assets/icon/box.png",txt:"txt",desc:"desc"}]

  type
  myid

  constructor(public helper:HelperProvider, public storage: Storage,public srv:ServiceProvider,public navCtrl: NavController, public navParams: NavParams) {
  
    this.langDirection = this.helper.lang_direction
    if(this.langDirection == "rtl")
    {
      this.scaleClass="scaleClassrtl";
    }else{
        this.scaleClass="scaleClassltr";
    }
      this.noNotifications  = true
      this.type = this.navParams.get("from")

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationsPage');
  }
  notificationClickd(item){
console.log("notificationClickd : ",item)
  }

  ionViewDidEnter(){
    console.log("ionViewDidEnter OrdersPage")
      
  if(this.type == "user"){
    console.log("get user orders")
    this.storage.get('info').then(val=>{
      console.log("user ",val)
      this.myid = val.alldata.id
    this.srv.getDriverNotification(this.myid,resp=>{
      console.log("resp ffrom getDriverNotification",resp)

      this.notificationsArr = JSON.parse(resp).Notifications
     
      console.log("notificationsArr : ",this.notificationsArr)
      
      for(var j=0;j<this.notificationsArr.length;j++){
        this.notificationsArr[j].color="#3fb875"
        this.notificationsArr[j].notificationimage="assets/icon/notification.png"
      }

  //     this.ordersArr= JSON.parse(resp).Orders
  //     if(this.ordersArr.length == 0){
  // this.noorders = false
  //     }else{
  //       this.noorders = true
  //     for(var j=0;j<this.ordersArr.length;j++){
  //       this.ordersArr[j].date = this.ordersArr[j].created_at.split(" ")[0]
  //       this.ordersArr[j].time= this.datepipe.transform(this.ordersArr[j].created_at, 'h:m a')
  //       //order status 
  //   //0 create
  //   //1 driver accept
  //   //2 done
  //   //3 user cancel
  //   //4 driver cancel
  //   if(this.ordersArr[j].status == "1"){
  //     this.ordersArr[j].x="#9E9E9E"
  //     this.ordersArr[j].statusTxt = this.translate.instant("orderaccept")
  //   }
  //   else if(this.ordersArr[j].status == "2"){
  //     this.ordersArr[j].x="rgb(94, 130, 94)"
  //     this.ordersArr[j].statusTxt = this.translate.instant("orderdone")
  //   }
  //   else if(this.ordersArr[j].status == "3"){
  //     this.ordersArr[j].x="#e01213"
  //     this.ordersArr[j].statusTxt = this.translate.instant("ordercancelled")
  //   }
    
  
  //     }
  //   }
  
    },err=>{
      console.log("err ffrom getDriverNotification",err)
    })
  })
  }else if(this.type == "driver"){
    console.log("get driver orders")
    this.storage.get('info').then(val=>{
      console.log("user ",val)
      this.myid = val.alldata.id
    this.srv.getDriverNotification(this.myid,resp=>{
      console.log("resp ffrom getDriverNotification",resp)
      this.notificationsArr = JSON.parse(resp).Notifications
     
      console.log("notificationsArr : ",this.notificationsArr)
      
      for(var j=0;j<this.notificationsArr.length;j++){
        this.notificationsArr[j].color="#3fb875"
        this.notificationsArr[j].notificationimage="assets/icon/notification.png"
      }
  //     this.ordersArr= JSON.parse(resp).Orders
  //     if(this.ordersArr.length == 0){
  // this.noorders = false
  //     }else{
  //       this.noorders = true
  //     for(var j=0;j<this.ordersArr.length;j++){
  //       this.ordersArr[j].date = this.ordersArr[j].created_at.split(" ")[0]
  //       this.ordersArr[j].time= this.datepipe.transform(this.ordersArr[j].created_at, 'h:m a')
  //       //order status 
  //   //0 create
  //   //1 driver accept
  //   //2 done
  //   //3 user cancel
  //   //4 driver cancel
  //   if(this.ordersArr[j].status == "1"){
  //     this.ordersArr[j].x="#9E9E9E"
  //     this.ordersArr[j].statusTxt = this.translate.instant("orderaccept")
  //   }
  //   else if(this.ordersArr[j].status == "2"){
  //     this.ordersArr[j].x="rgb(94, 130, 94)"
  //     this.ordersArr[j].statusTxt = this.translate.instant("orderdone")
  //   }
  //   else if(this.ordersArr[j].status == "3"){
  //     this.ordersArr[j].x="#e01213"
  //     this.ordersArr[j].statusTxt = this.translate.instant("ordercancelled")
  //   }
    
  
  //     }
  //   }
    
    },err=>{
      console.log("err ffrom getDriverNotification",err)
    })
  })
  }
  
  }
  
}
