import { Component,NgZone, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { Storage } from '@ionic/storage';

import { LocationPusherProvider } from '../../providers/location-pusher/location-pusher'
import { Broadcaster } from '@ionic-native/broadcaster/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { PusherServiceProvider } from '../../providers/pusher-service/pusher-service'
// import {
//   GoogleMaps,
//   GoogleMap,
//   GoogleMapsEvent,
//   GoogleMapOptions,
//   CameraPosition,
//   MarkerOptions,
//   Marker,
//   Environment,
//   LocationService,
//   MyLocation,
//   HtmlInfoWindow
// } from '@ionic-native/google-maps';
declare var google;


declare const Pusher: any;

// @IonicPage()
@Component({
  selector: 'page-user-home',
  templateUrl: 'user-home.html',
  providers:[LocalNotifications]
  // providers:[Broadcaster]
})
export class UserHomePage {
  GoogleAutocomplete
  //autocomplete
  autocompleteItems=[]
  autocompleteItems2=[]
  geocoder
  desc
  position
  searchtxt=""
  searchtxt2=""
  // map: GoogleMap;
  @ViewChild('map') mapElement:ElementRef;
  map: any;

  scaleClass="";
  langDirection = "";
  autocomplete = { input: '' };
  autocomplete2 = { input: '' };

  hidemap
  //  marker: Marker 
   channel1
   borderClass=""

   mylocation

   fromToClicked 

   allMarkers=[]

   //create order object
  //  {"user_id":39,"description":"A","address":"ديرب الخضر، Dekernes, Egypt","lat":31.0637373,"lng":31.648274000000015,"lat_user":31.0857231,"lng_user":31.58884469999998}
  constructor(private pusher : PusherServiceProvider,private localNotifications: LocalNotifications,private broadcaster: Broadcaster,public events:Events,private pusherlocation : LocationPusherProvider,public storage:Storage,public srv:ServiceProvider, public translate:TranslateService,public helper:HelperProvider, private zone:NgZone,public navCtrl: NavController, public navParams: NavParams) {
  
    this.storage.get('info').then(val=>{
      console.log("user ",val)
      // this.myid = val.alldata.id
      console.log("val.alldata.id : ",val.alldata.id, " : this.helper.registration : ",this.helper.registration)
      this.srv.firebaseregistration(val.alldata.id,this.helper.registration,resp=>{
        console.log("resp from firebaseregistration : ",resp)
      },err=>{
        console.log("err from firebaseregistration : ",err)
      })
    });
    this.fromToClicked ="from"
    this.langDirection = this.helper.lang_direction;
console.log("langDirection from home :",this.langDirection)
    // this.srv.endOrder("15",resp=>{
    //   console.log("resp from end order",resp)
    // },err=>{
    //   console.log("err from end order",err)
    // })
    
    // this.srv.rateOrder("desc","2",23,25,resp=>{
    //   console.log("resp from rate order : ",resp)
    // },err=>{
    //   console.log("err from rate order : ",err)
    // })

const channel = this.pusher.init();
    channel.bind('my-event', (data) => {
      console.log("myevent data from pusher : ",data)
      // alert("my event data : "+JSON.stringify(data))

      // if(data.score >= 1){
      //   this.rating.good = this.rating.good + 1;
      // }
      // else{
      //   this.rating.bad = this.rating.bad + 1;
      // }
      // this.comments.push(data);
      var newData =  JSON.parse(JSON.stringify(data))
      this.storage.get('info').then(val=>{
        console.log("user ",val)
        this.myid = val.alldata.id
      if(newData.type == "3"){
        if(newData.user_id == this.myid){
          console.log(" if ==")
          if(this.langDirection == "rtl")
            this.scheduleNotification(this.translate.instant("orderAccepted2"),newData.driver_id,newData.Notifcation)
          else if(this.langDirection == "ltr")
            this.scheduleNotification(this.translate.instant("orderAccepted2"),newData.driver_id,newData.Notifcation)
        }else{
          console.log(" else ==")
        }
      }
        

      });
      
    });


// this.channel1 = this.pusherlocation.init();
    
//     this.channel1.bind('client-someeventname', (data) => {
//       alert("bind event form channel1"+data)
//     });
// this.helper.getStatus()

// this.helper.createuser(50,"50,60")
// this.helper.onlocationChanged(50)
// this.helper.updateLocation(50,"5555,5555")

// var pusher = new Pusher('6d505d3ed5dd3857b199');
// var channel2 = pusher.subscribe('private-channel');
// // channel.bind('pusher:subscription_succeeded', function() {
// //   var triggered = channel.trigger('client-someeventname', { your: "data" });
// // });
// channel.bind('pusher:subscription_succeeded', ()=> {
//   channel.bind('client-someeventname', (data) => {
//     alert("1-bind event form channel1"+data)
// });
// });
// channel.bind('client-someeventname', (data) => {
//         alert("2-bind event form channel1"+data)
// });

// this.searchtxt =this.translate.instant("search")
this.searchtxt =this.translate.instant("from")
this.searchtxt2 =this.translate.instant("to")
    if(this.langDirection == "rtl")
    {
      this.scaleClass="scaleClassrtl";
      // this.searchtxt ="بحث"
      this.searchtxt ="من .."
      this.searchtxt2="الى .."
      this.borderClass = "linertl"
      
      
    } else{
        this.scaleClass="scaleClassltr";
        // this.searchtxt ="Search"
        this.searchtxt ="From .."
        this.searchtxt2="To .."
        this.borderClass = "lineltr"
      }

    // this.searchtxt =this.translate.instant("search")
    this.hidemap = true

    // const channel = this.pusher.init();
    // channel.bind('my-event', (data) => {
    //   console.log("myevent data from pusher : ",data)
    //   alert("my event data : "+JSON.stringify(data))
    //   // if(data.score >= 1){
    //   //   this.rating.good = this.rating.good + 1;
    //   // }
    //   // else{
    //   //   this.rating.bad = this.rating.bad + 1;
    //   // }
    //   // this.comments.push(data);
    // });
  }

  ionViewDidEnter(){

    // this.autocomplete.input = ""
    // this.autocomplete2.input = ""
    
    if(this.helper.fromLocation){
      if(this.map){

        // this.marker= this.map.addMarkerSync({
        //   title:"From",
        //   icon: 'orange',
        //   animation:'DROP',
        //   position: {
        //     lat:  parseFloat(this.helper.fromLocation.split(",")[0]),
        //     lng: parseFloat(this.helper.fromLocation.split(",")[1])
        //   }
        // });


        for(var j=0;j<this.allMarkers.length;j++)
        {
          this.allMarkers[j].setMap(null);
        }

        let marker = new google.maps.Marker({
    map: this.map,
    animation: google.maps.Animation.DROP,
    position: new google.maps.LatLng(parseFloat(this.helper.fromLocation.split(",")[0]),parseFloat(this.helper.fromLocation.split(",")[1])),
    // icon: { 
    //   url : this.markimage,
    //   size: new google.maps.Size(71, 71),
    //   scaledSize: new google.maps.Size(20, 25) 
    // },
    label:{
      text:this.translate.instant("from"),
      color:"#e01213",      
    }
  });

  this.allMarkers.push(marker)
        this.srv.getaddress(this.helper.fromLocation.split(",")[0],this.helper.fromLocation.split(",")[1]).subscribe(
          resp=>{
            console.log("resp from get address from userHome",resp , "address from google :", JSON.parse(JSON.stringify(resp)).results[0].formatted_address);
   
            this.autocomplete.input =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;
   
   //         this.autocomplete2.input =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;
        
          },err=>{
        console.log("err from get address1",err);
        // this.driverlocation = "";
        })

      }
    }


    if(this.helper.toLocation){
      if(this.map){

        // this.marker= this.map.addMarkerSync({
        //   title:"To",
        //   icon: 'orange',
        //   animation:'DROP',
        //   position: {
        //     lat:  parseFloat(this.helper.toLocation.split(",")[0]),
        //     lng: parseFloat(this.helper.toLocation.split(",")[1])
        //   }
        // });
        for(var j=0;j<this.allMarkers.length;j++)
        {
          this.allMarkers[j].setMap(null);
        }

        let marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: new google.maps.LatLng(parseFloat(this.helper.toLocation.split(",")[0]),parseFloat(this.helper.toLocation.split(",")[1])),
          // icon: { 
          //   url : this.markimage,
          //   size: new google.maps.Size(71, 71),
          //   scaledSize: new google.maps.Size(20, 25) 
          // },
          label:{
            text:this.translate.instant("to"),
            color:"#e01213",      
          }
        });
        this.allMarkers.push(marker)

        this.srv.getaddress(this.helper.toLocation.split(",")[0],this.helper.toLocation.split(",")[1]).subscribe(
          resp=>{
            console.log("resp from get address from userHome",resp , "address from google :", JSON.parse(JSON.stringify(resp)).results[0].formatted_address);
   
            // this.autocomplete.input =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;
   
           this.autocomplete2.input =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;
        this.helper.toAddressDesc =  this.autocomplete2.input;
          },err=>{
        console.log("err from get address1",err);
        // this.driverlocation = "";
        })

      }
    }


  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad UserHomePage');
    // this.loadMap()
    this.position={lat :this.helper.lat,lng:this.helper.lng}
    this.mylocation={lat :this.helper.lat,lng:this.helper.lng}
    this.setMap();

    this.helper.geoLoc((data)=>{
      console.log("data from geo loc user home :",data)
      var respData = JSON.parse(JSON.stringify(data))
    // this.loadMap()
    // let data = {
    //   inspectorLat: resp.coords.latitude,
    //   inspectorLong: resp.coords.longitude,
    //   inspectorLocAccuracy: resp.coords.accuracy
    // }

    // this.position={lat :respData.inspectorLat,lng:respData.inspectorLong}
    // this.mylocation={lat :respData.inspectorLat,lng:respData.inspectorLong}
  //  this.setMap();
    })
    
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.geocoder = new google.maps.Geocoder;
    this.autocomplete = { input: '' };
    this.autocomplete2 = { input: '' };
    this.autocompleteItems = [];
    this.autocompleteItems2 = [];
  }
  updateSearchResults(){
    console.log("updateSearchResults");
    this.fromToClicked = "from"
    if (this.autocomplete.input == '') {
      this.autocompleteItems = [];
      // this.driversArr = []
      return;
    }
    console.log("this.autocomplete.input: ",this.autocomplete.input)
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input },
    (predictions, status) => {
      this.autocompleteItems = [];
      this.zone.run(() => {
        predictions.forEach((prediction) => {
          this.autocompleteItems.push(prediction);
        });
      });
    });
  }
  selectSearchResult(item){
    console.log("selectSearchResult , ",item)
    // alert("select search res")
    this.hidemap = false
//    this.loadMap()
    this.desc = item.description
    this.autocompleteItems= []
    
    // 'placeId': item.place_id
    //'address':item.description
    this.geocoder.geocode({'placeId': item.place_id}, (results, status) => {
      console.log("geocoder.geocode results : ",results)
      console.log("geocoder.geocode status : ",status)
      console.log("results[0].geometry.location.lat : ",results[0].geometry.location.lat())
      console.log("results[0].geometry.location.lng : ",results[0].geometry.location.lng())
      if(status === 'OK' && results[0]){
        this.position = {
            lat: results[0].geometry.location.lat(),
            lng: results[0].geometry.location.lng()
        };
        console.log("item.position : ",this.position);
        
        
        
        // Environment.setEnv({
        //   'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY',
        //   'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY'
        // });
    
        // let mapOptions: GoogleMapOptions = {
        //   camera: {
        //     //  target:AIR_PORTS
        //     target:
        //       {
        //        lat: this.position.lat,
        //        lng: this.position.lng
        //      }
        //      ,
        //      zoom: 18,
        //      tilt: 30
        //    },controls: {
        //     zoom: false,//true
        //     myLocationButton : true,
        //     myLocation:true
            
        // }
        // };
    
        // if(this.map)
        // this.map.remove()

        // this.map = GoogleMaps.create('map_canvas', mapOptions);
        let latlng = new google.maps.LatLng(this.position.lat, this.position.lng);
        var mapOptions={
          center:latlng,
          zoom:15,
          // zoom:20,
          streetViewControl:false,
          fullscreenControl:false,
          mapTypeId:google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true,
          styles:this.helper.mapStyle
        };
      
        this.map=  new google.maps.Map(this.mapElement.nativeElement,mapOptions);
      
        var markers, i;
        
        for(var j=0;j<this.allMarkers.length;j++)
        {
          this.allMarkers[j].setMap(null);
        }
     
        let marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: new google.maps.LatLng(this.position.lat,this.position.lng),
          // icon: { 
          //   url : this.markimage,
          //   size: new google.maps.Size(71, 71),
          //   scaledSize: new google.maps.Size(20, 25) 
          // },
          // label:{
          //   text:"From",
          //   color:"black",      
          // }
        });

this.allMarkers.push(marker)
        // this.marker= this.map.addMarkerSync({
        //   // title: 'Ionic\n <br>hello',
        //   // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
        //   icon: 'orange',
        //   animation:'DROP',// 'BOUNCE',//'DROP',
        //   position: {
        //     lat:  this.position.lat,
        //     lng:  this.position.lng
        //   }
        // });

        this.navCtrl.push("HalfMapToMarkLocationPage",{from:'from',lat:this.position.lat,lng:this.position.lng})

        // this.addMapClick()

      //   this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe((location: any[]) => {
      //     console.log("click location from map on :",location);
      //    // alert("map location : "+JSON.stringify(location));
      //    this.position = {
      //     lat: location[0].lat,
      //     lng: location[0].lng
      // };
      // this.srv.getaddress(this.position.lat,this.position.lng).subscribe(
      //   resp=>{
      //     console.log("resp from get address from userHome",resp , "address from google :", JSON.parse(JSON.stringify(resp)).results[0].formatted_address);
      //      this.autocomplete.input =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;
      // },err=>{
      // console.log("err from get address1",err);
      // // this.driverlocation = "";
      // })

      // this.marker.remove()

      //     this.marker = this.map.addMarkerSync({
      //       // title: 'Ionic\n <br>hello',
      //       // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
      //       icon: 'orange',
      //       animation: 'DROP',
      //       position: {
      //         lat:  location[0].lat,
      //         lng:  location[0].lng
      //       }
      //     });
      //   });

        // this.map.on(GoogleMapsEvent.MY_LOCATION_CLICK).subscribe((location: any[]) => {
        //   console.log("location from map on :",location);
        //   alert("location : "+JSON.stringify(location));
        // });

      }
    })
  }
  updateSearchResults2(){
    console.log("updateSearchResults");
    this.fromToClicked ="to"
    if (this.autocomplete2.input == '') {
      this.autocompleteItems2 = [];
      // this.driversArr = []
      return;
    }
    console.log("this.autocomplete2.input: ",this.autocomplete2.input)
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete2.input },
    (predictions, status) => {
      this.autocompleteItems2 = [];
      this.zone.run(() => {
        predictions.forEach((prediction) => {
          this.autocompleteItems2.push(prediction);
        });
      });
    });
  }
  mydesc

  selectSearchResult2(item){
    console.log("selectSearchResult , ",item)
    // alert("select search res")
    this.hidemap = false
//    this.loadMap()
    this.mydesc = item.description
    this.autocompleteItems2= []
    
    // 'placeId': item.place_id
    //'address':item.description
    this.geocoder.geocode({'placeId': item.place_id}, (results, status) => {
      console.log("geocoder.geocode results : ",results)
      console.log("geocoder.geocode status : ",status)
      console.log("results[0].geometry.location.lat : ",results[0].geometry.location.lat())
      console.log("results[0].geometry.location.lng : ",results[0].geometry.location.lng())
      if(status === 'OK' && results[0]){
        this.mylocation = {
            lat: results[0].geometry.location.lat(),
            lng: results[0].geometry.location.lng()
        };
        console.log("item.mylocation : ",this.mylocation);
        
        
        
        // Environment.setEnv({
        //   'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY',
        //   'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY'
        // });
    
        // let mapOptions: GoogleMapOptions = {
        //   camera: {
        //     //  target:AIR_PORTS
        //     target:
        //       {
        //        lat: this.mylocation.lat,
        //        lng: this.mylocation.lng
        //      }
        //      ,
        //      zoom: 18,
        //      tilt: 30
        //    },controls: {
        //     zoom: false,//true
        //     myLocationButton : true,
        //     myLocation:true
            
        // }
        // };
    
        // if(this.map)
        // this.map.remove()

        // this.map = GoogleMaps.create('map_canvas', mapOptions);

        // this.marker= this.map.addMarkerSync({
        //   title:this.searchtxt,
        //   // title: 'Ionic\n <br>hello',
        //   // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
        //   icon: 'orange',
        //   animation:'DROP',// 'BOUNCE',//'DROP',
        //   position: {
        //     lat:  this.mylocation.lat,
        //     lng:  this.mylocation.lng
        //   }
        // });

        let latlng = new google.maps.LatLng(this.mylocation.lat, this.mylocation.lng);
        var mapOptions={
          center:latlng,
          zoom:15,
          // zoom:20,
          streetViewControl:false,
          fullscreenControl:false,
          mapTypeId:google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true,
          styles:this.helper.mapStyle
        };
      
        this.map=  new google.maps.Map(this.mapElement.nativeElement,mapOptions);
      
        var markers, i;
        
        for(var j=0;j<this.allMarkers.length;j++)
        {
          this.allMarkers[j].setMap(null);
        }
     
        let marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: new google.maps.LatLng(this.mylocation.lat,this.mylocation.lng),
          // icon: { 
          //   url : this.markimage,
          //   size: new google.maps.Size(71, 71),
          //   scaledSize: new google.maps.Size(20, 25) 
          // },
          // label:{
          //   text:"From",
          //   color:"black",      
          // }
        });
        this.navCtrl.push("HalfMapToMarkLocationPage",{from:'to',lat:this.mylocation.lat,lng:this.mylocation.lng})
        // this.addMapClick()

      //   this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe((location: any[]) => {
      //     console.log("click location from map on :",location);
      //    // alert("map location : "+JSON.stringify(location));
      //    this.position = {
      //     lat: location[0].lat,
      //     lng: location[0].lng
      // };
      // this.srv.getaddress(this.position.lat,this.position.lng).subscribe(
      //   resp=>{
      //     console.log("resp from get address from userHome",resp , "address from google :", JSON.parse(JSON.stringify(resp)).results[0].formatted_address);
      //      this.autocomplete.input =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;
      // },err=>{
      // console.log("err from get address1",err);
      // // this.driverlocation = "";
      // })

      // this.marker.remove()

      //     this.marker = this.map.addMarkerSync({
      //       // title: 'Ionic\n <br>hello',
      //       // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
      //       icon: 'orange',
      //       animation: 'DROP',
      //       position: {
      //         lat:  location[0].lat,
      //         lng:  location[0].lng
      //       }
      //     });
      //   });

        // this.map.on(GoogleMapsEvent.MY_LOCATION_CLICK).subscribe((location: any[]) => {
        //   console.log("location from map on :",location);
        //   alert("location : "+JSON.stringify(location));
        // });

      }
    })
  }

  loadMap() {

//     var HND_AIR_PORT = {lat: 35.548852, lng: 139.784086};
// var SFO_AIR_PORT = {lat: 37.615223, lng: -122.389979};
// var HNL_AIR_PORT = {lat: 21.324513, lng: -157.925074};
// var AIR_PORTS = [
//   HND_AIR_PORT,
//   HNL_AIR_PORT,
//   SFO_AIR_PORT
// ];

    // This code is necessary for browser
    // Environment.setEnv({
    //   'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY',
    //   'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY'
    // });

    // let mapOptions: GoogleMapOptions = {
    //   camera: {
    //     //  target:AIR_PORTS
    //     target:
    //       {
    //        lat: this.helper.lat,
    //        lng: this.helper.lng
    //      }
    //      ,
    //      zoom: 18,
    //      tilt: 30
    //    },controls: {
    //     zoom: false,//true
    //     myLocationButton : true,
    //     myLocation:true
        
    // }
    // };

    // this.map = GoogleMaps.create('map_canvas', mapOptions);

    // let marker: Marker = this.map.addMarkerSync({
    //   // title: 'Ionic\n <br>hello',
    //   // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
    //   icon: 'orange',
    //   animation: 'DROP',
    //   position: {
    //     lat: 43.0741904,
    //     lng: -89.3809802
    //   }
    // });
  
  
    // let htmlInfoWindow = new HtmlInfoWindow();
    // htmlInfoWindow.setContent('hello <br> hi<br> <button ion-button style="background-color:red;color:white" (click)="himarker(5)">Demo</button>' 
    // );

//     var html = "<img src='./House-icon.png' width='64' height='64' >" +
//     "<br>" +
//     "This is an example";
// htmlInfoWindow.setContent(html);
    // {
    //   width: "280px",
    //   height: "330px"
    // }

    // marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
    //   alert('clicked');
    //   htmlInfoWindow.open(marker);
    // });

    // this.map.addPolyline({
    //   'points': AIR_PORTS,
    //   'color' : '#AA00FF',
    //   'width': 10,
    //   'geodesic': true
    // });

   
    // this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe((location: any[]) => {
    //   console.log("map location from map on :",location);
    // //  alert("map location : "+JSON.stringify(location));
    // });


    // this.map.on(GoogleMapsEvent.MY_LOCATION_CLICK).subscribe((location: any[]) => {
    //   console.log("location from map on :",location);
    //  // alert("location : "+JSON.stringify(location));
    // //   let marker: Marker = this.map.addMarkerSync({
    // //     'title': ["Current your location:\n",
    // //         "latitude:" + location.latLng.lat.toFixed(3),
    // //         "longitude:" + location.latLng.lng.toFixed(3),
    // //         "speed:" + location.speed,
    // //         "time:" + location.time,
    // //         "bearing:" + location.bearing].join("\n"),
    // //     'position': location.latLng,
    // //     'animation': plugin.google.maps.Animation.DROP,
    // //     'disableAutoPan': true
    // //   });
    // //   marker.showInfoWindow();
    // // });

    // })
  }
// mylocation(){
//   LocationService.getMyLocation().then((myLocation: MyLocation) => {
// console.log("myloc from mylocation:  ",myLocation);
//     // let options: GoogleMapOptions = {
//     //   camera: {
//     //     target:   {
//     //          lat: myLocation.latLng.lat,
//     //          lng:myLocation.latLng.lng
//     //        } 
//     //   }
//     // };
//     // this.map = GoogleMaps.create('map_canvas', options);
//     let marker: Marker = this.map.addMarkerSync({
//       title: 'my loc '+myLocation.latLng,
//       icon: 'red',
//       animation: 'DROP',
//       position: {
//         lat: myLocation.latLng.lat,
//         lng: myLocation.latLng.lng
//       }
//     });

//   });  
// }

// himarker(val){
//   alert("hi marker "+ val)
// }
dismiss(){
  console.log("dismiss")
  this.navCtrl.pop();
}
myid
continue(){
  console.log("continue")
  this.navCtrl.push("CompleteOrderPage",{lat:this.position.lat,lng:this.position.lng,address:this.desc,userLat:this.mylocation.lat,userLng:this.mylocation.lng})
//   this.storage.get('info').then(val=>{
//     console.log("user ",val)
//     this.myid = val.alldata.id
//   var data = {"user_id":this.myid,
//   "description":"trip desc",
//   "address":this.desc,
//   "lat":this.position.lat,
//   "lng":this.position.lng,
//   "userlat":this.helper.lat,
//   "userlng":this.helper.lng}
//   this.srv.createnewOrder(data,resp=>{
//     console.log("resp from create new order : ",resp)
//   },err=>{
//     console.log("err from create new order : ",err)
//   })
// });
}
publishEvent(){
  // alert("clicked")
  this.events.publish('userevent',{data:"xyz"});
}

showMap(){
  // MapPage
  this.navCtrl.push("MapPage")
}
showorderdeatisl(){
  this.navCtrl.push("ShowOrderDetailsPage")
}
sendpusherdata(){
  console.log("sendpusherdata")
  // this.pusher.
  // // Listen to events from Native
  // this.broadcaster.addEventListener('eventName').subscribe((event) => console.log(event));

// Send event to Native
this.broadcaster.fireNativeEvent('eventName', {}).then(() => {
  console.log('success')
  alert("eventName fired")
});

}

scheduleNotification(txt,driverId,orderId) {
    
  
  this.localNotifications.schedule({
    id: driverId,
    title: this.translate.instant("appTitle"),
    text:   txt,
    data: { mydata: "",orderid:orderId},
    trigger:{ at: new Date(new Date().getTime())}

  });
  this.navCtrl.setRoot("MapPage",{driver_id:driverId,order_id:orderId})


//  this.localNotifications.on('click').subscribe(resp=>{
//   // alert ("notification resp : "+JSON.stringify( resp))
//   console.log("notification resp : ",resp)
//   var nresp = JSON.parse(JSON.stringify(resp))
//   // var xdata= {orderId : nresp.id,desc:nresp.data.mydata,userId:nresp.data.x,driverId:nresp.data.driverId}
//   // console.log("xdata: ",xdata)
//   this.navCtrl.setRoot("MapPage",{driver_id:nresp.id,order_id:nresp.data.orderid})
//  },err=>{
//   //  alert ("notification err : "+JSON.stringify( err))
//  })


}

waiting(){
  console.log("WatingViewPage")
  this.navCtrl.setRoot('WatingViewPage')
}
setMap(){
  // Environment.setEnv({
  //   'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY',
  //   'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY'
  // });

  // let mapOptions: GoogleMapOptions = {
  //   camera: {
  //     //  target:AIR_PORTS
  //     target:
  //       {
  //        lat: this.position.lat,
  //        lng: this.position.lng
  //      }
  //      ,
  //      zoom: 18,
  //      tilt: 30
  //    },controls: {
  //     zoom: false,//true
  //     myLocationButton : true,
  //     myLocation:true
      
  // }
  // };

  // if(this.map)
  // this.map.remove()

  // this.map = GoogleMaps.create('map_canvas', mapOptions);

  let latlng = new google.maps.LatLng(this.position.lat, this.position.lng);
  var mapOptions={
    center:latlng,
    zoom:15,
    // zoom:20,
    streetViewControl:false,
    fullscreenControl:false,
    mapTypeId:google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true,
    styles:this.helper.mapStyle
   

  };

  this.map=  new google.maps.Map(this.mapElement.nativeElement,mapOptions);


//   this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe((location: any[]) => {
//     console.log("click location from map on :",location);
//    // alert("map location : "+JSON.stringify(location));
//    this.hidemap = false
//    var loc
//    if(this.fromToClicked == "from"){
//     this.position = {
//       lat: location[0].lat,
//       lng: location[0].lng
//   }; 
//   loc = {
//     lat: location[0].lat,
//     lng: location[0].lng
// };
//    }else  if(this.fromToClicked == "to"){
//     this.mylocation = {
//       lat: location[0].lat,
//       lng: location[0].lng
//   }; 
//   loc = {
//     lat: location[0].lat,
//     lng: location[0].lng
// };
//    }else{
//      console.log("else")
//    }
  
// this.srv.getaddress(loc.lat,loc.lng).subscribe(
//   resp=>{
//     console.log("resp from get address from userHome",resp , "address from google :", JSON.parse(JSON.stringify(resp)).results[0].formatted_address);
// if(this.fromToClicked == "from")
//     this.autocomplete.input =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;
//    else if(this.fromToClicked == "to")
//     this.autocomplete2.input =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;

//   },err=>{
// console.log("err from get address1",err);
// // this.driverlocation = "";
// })
// if(this.marker)
// this.marker.remove()

// // var markerTitle
// // if(this.fromToClicked == "from")
// // markerTitle = 
//     this.marker = this.map.addMarkerSync({
//       // title: 'Ionic\n <br>hello',
//       // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
//       icon: 'orange',
//       animation: 'DROP',
//       position: {
//         lat:  location[0].lat,
//         lng:  location[0].lng
//       }
//     });
//   });


}
// addMapClick(){

//   this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe((location: any[]) => {
//     console.log("click location from map on :",location);
//    // alert("map location : "+JSON.stringify(location));
//    this.hidemap = false
//    var loc
//    if(this.fromToClicked == "from"){
//     this.position = {
//       lat: location[0].lat,
//       lng: location[0].lng
//   }; 
//   loc = {
//     lat: location[0].lat,
//     lng: location[0].lng
// }; 

//    }else  if(this.fromToClicked == "to"){
//     this.mylocation = {
//       lat: location[0].lat,
//       lng: location[0].lng
//   }; 
//   loc = {
//     lat: location[0].lat,
//     lng: location[0].lng
// };
//    }else{
//      console.log("else")
//    }
  
// this.srv.getaddress(loc.lat,loc.lng).subscribe(
//   resp=>{
//     console.log("resp from get address from userHome",resp , "address from google :", JSON.parse(JSON.stringify(resp)).results[0].formatted_address);
// if(this.fromToClicked == "from")
//     this.autocomplete.input =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;
//    else if(this.fromToClicked == "to")
//     this.autocomplete2.input =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;

//   },err=>{
// console.log("err from get address1",err);
// // this.driverlocation = "";
// })
// if(this.marker)
// this.marker.remove()

// // var markerTitle
// // if(this.fromToClicked == "from")
// // markerTitle = 
//     this.marker = this.map.addMarkerSync({
//       // title: 'Ionic\n <br>hello',
//       // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
//       icon: 'orange',
//       animation: 'DROP',
//       position: {
//         lat:  location[0].lat,
//         lng:  location[0].lng
//       }
//     });
//   });

// }

createOrder(){

  var data = {"user_id":39,
  "description":"A","address":"ديرب الخضر، Dekernes, Egypt",
  "lat":31.0637373,"lng":31.648274000000015,
  "userlat":31.0857231,"userlng":31.58884469999998}

  // var data = {
  //   "user_id":this.myid,
  //   "description":this.orderdetails,
  //   "address":this.address,
  //   "lat":this.locationLat,
  //   "lng":this.locationLng,
  //   "userlat":this.userLat,
  //   "userlng":this.userLng
  // }
  // "userlat":this.helper.lat,
  // "userlng":this.helper.lng
    console.log("order data : ",data)
    this.srv.createnewOrder(data,resp=>{
      console.log("resp from create new order : ",resp)
      if(JSON.parse(resp).code == 200){
        console.log("wating..")
        // this.helper.presentToast(ordersent)
        // this.presentThanks("",this.translate.instant("ordersent"))
        // this.navCtrl.setRoot('WatingViewPage')
      }
       
      else 
        this.helper.presentToast(this.translate.instant("connectionErr"))
    },err=>{
      console.log("err from create new order : ",err)
    })
}
}
