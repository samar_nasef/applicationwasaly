import { Component } from '@angular/core';
import { MenuController } from 'ionic-angular';
// import { AboutPage } from '../about/about';
// import { ContactPage } from '../contact/contact';
// import { HomePage } from '../home/home';
// import { DashboardPage } from '../dashboard/dashboard';
// import { SetupPage } from '../setup/setup';
// import { SitePage } from '../site/site';
// import { ScanTrapPage } from '../scan-trap/scan-trap';



@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  moreicon="moreicon";

  // tab1Root = DashboardPage;
  // tab2Root = SetupPage;
  // tab3Root = SitePage;
  // tab4Root = ScanTrapPage;
  // tab5Root = ContactPage;

  constructor(public menu:MenuController) {

  }

  onTabSelected()
  {
    console.log("openmenu");
    this.menu.open();
  }
  
}
