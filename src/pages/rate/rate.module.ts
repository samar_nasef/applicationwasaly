import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RatePage } from './rate';
import { Ionic2RatingModule } from "ionic2-rating";
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    RatePage,
  ],
  imports: [
    IonicPageModule.forChild(RatePage),
    Ionic2RatingModule,
    TranslateModule.forChild()
  ],
})
export class RatePageModule {}
