import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';

import { ServiceProvider } from '../../providers/service/service';
import { Storage } from '@ionic/storage';
import { UserHomePage } from '../user-home/user-home';
@IonicPage()
@Component({
  selector: 'page-rate',
  templateUrl: 'rate.html',
})
export class RatePage {
  scaleClass="";
  langDirection = "";
  userImageUrl = "assets/imgs/default-avatar.png"

  rate
  notes
  
  driverId
  userid


  constructor(public storage: Storage,public srv:ServiceProvider, public translate: TranslateService,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    console.log("langDirection from home :",this.langDirection)
    if(this.langDirection == "rtl"){
      this.scaleClass="scaleClassrtl";} else{
        this.scaleClass="scaleClassltr";}

      this.driverId =   this.navParams.get("driverId")
console.log("driverId from rate: ",this.driverId)

this.helper.removeUserObject(this.driverId);
console.log("remove driver removeUserObject from rate Page ",this.driverId)

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RatePage');
  }
  dismiss(){
    // this.navCtrl.pop()
    this.navCtrl.setRoot(UserHomePage)
  }
  rateFunc(){
    console.log("rate :",this.rate," , notes: ",this.notes)
    if(! this.rate){
      this.helper.presentToast(this.translate.instant("chooseRate"))
      return
    }else if(! this.notes){
      
      this.helper.presentToast(this.translate.instant("enterNotes"))
      return
    }else{
      console.log("rate")
      this.storage.get('info').then(val=>{
        console.log("user ",val)
        this.userid = val.alldata.id
        console.log("userid from rate ; ",this.userid)
        this.srv.rateOrder(this.notes,this.rate,this.userid,this.driverId,resp=>{
          console.log("resp from rate order : ",resp)
          // {"message":"true","rate":"success","code":"200"}

          var respData = JSON.parse(resp)
    console.log("code from resp end order: ",respData.code)
  
    if(respData.code == 200){
      console.log("200 status")
      this.helper.presentToast(this.translate.instant("rateEnd"))
      this.helper.fromLocation = ""
      this.helper.toLocation=""
      this.navCtrl.setRoot(UserHomePage)
    }else{
      this.helper.presentToast(this.translate.instant("connectionErr"))
    }

        },err=>{
          console.log("err from rate order : ",err)
        })
      });
   
    }
      
  }
}
