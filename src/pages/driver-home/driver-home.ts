import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,Events} from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServiceProvider } from '../../providers/service/service';
import { Storage } from '@ionic/storage';
import { PusherServiceProvider } from '../../providers/pusher-service/pusher-service'
import { Broadcaster } from '@ionic-native/broadcaster/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { TranslateService } from '@ngx-translate/core';
import { LocationPusherProvider } from '../../providers/location-pusher/location-pusher';

declare const Pusher: any;
// @IonicPage()
@Component({
  selector: 'page-driver-home',
  templateUrl: 'driver-home.html',
  providers:[LocalNotifications]
})
export class DriverHomePage {

  scaleClass="";
  langDirection = "";
  myid
  ordersArr=[]
  noorders
  statusColor
  statusVal 
  channel1
  // data=[{color:"#abacae",notificationimage:"assets/icon/notification.png",txt:"txt",desc:"desc"},
  // {color:"#3fb875",notificationimage:"assets/icon/chat-bubble.png",txt:"txt",desc:"desc"},
  // {color:"#418ccb",notificationimage:"assets/icon/box.png",txt:"txt",desc:"desc"}]
 
 
  constructor(public locationpusher:LocationPusherProvider,  public translate: TranslateService,private localNotifications: LocalNotifications,private broadcaster: Broadcaster,public events:Events,private pusher : PusherServiceProvider,public storage:Storage,public srv:ServiceProvider, public helper:HelperProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    console.log("langDirection from driver home :",this.langDirection)
   
    this.storage.get('info').then(val=>{
      console.log("user ",val)
      // this.myid = val.alldata.id
      console.log("val.alldata.id : ",val.alldata.id , " : this.helper.registration : ",this.helper.registration)
      this.srv.firebaseregistration(val.alldata.id,this.helper.registration,resp=>{
        console.log("resp from firebaseregistration : ",resp)
      },err=>{
        console.log("err from firebaseregistration : ",err)
      })
    });
   
    if(this.langDirection == "rtl"){
      this.scaleClass="scaleClassrtl";}else{
        this.scaleClass="scaleClassltr";}

    // this.statusVal = "0"

//     this.helper.createuser("15",this.helper.lat+","+this.helper.lng)
// this.helper.startTimerTogetLocation("15")

    this.events.subscribe('userevent', (data) => {
      alert("received")
      console.log("data from driver home: ",data)
    });
    
    // Listen to events from Native
//   this.broadcaster.addEventListener('eventName').subscribe((event) => {console.log(event)
//   alert("event name Listener")
// });

    const channel = this.pusher.init();
    channel.bind('my-event', (data) => {
      console.log("myevent data from pusher : ",data)
      // alert("my event data : "+JSON.stringify(data))

      // if(data.score >= 1){
      //   this.rating.good = this.rating.good + 1;
      // }
      // else{
      //   this.rating.bad = this.rating.bad + 1;
      // }
      // this.comments.push(data);
      var newData =  JSON.parse(JSON.stringify(data))
      this.storage.get('info').then(val=>{
        console.log("user ",val)
        this.myid = val.alldata.id

        // type=1 => accept
        //4 finished
        //3 resjected
        
        //1 create 
      if(newData.type != "3" && newData.type != "4"){
        if(newData.driver_id == this.myid){
          console.log(" if ==")

//     Notifcation.price: null
// Notifcation.type: "0"
          if(this.langDirection == "rtl")
            this.scheduleNotification(newData.Notifcation.ar_title,newData.Notifcation.order_id,newData.Notifcation.description,newData.Notifcation.user_id,newData.driver_id,newData.Order.lat,newData.Order.lng,newData.Notifcation.price,newData.Notifcation.type,newData.Order.lat_user,newData.Order.lng_user)
          else if(this.langDirection == "ltr")
            this.scheduleNotification(newData.Notifcation.en_title,newData.Notifcation.order_id,newData.Notifcation.description,newData.Notifcation.user_id,newData.driver_id,newData.Order.lat,newData.Order.lng,newData.Notifcation.price,newData.Notifcation.type,newData.Order.lat_user,newData.Order.lng_user)
        }else{
          console.log(" else ==")
        }
      }else if(newData.type == "4"){
        this.scheduleNotification2()
        this.navCtrl.setRoot(DriverHomePage)
      }
        

      });
      
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DriverHomePage');
    this.helper.geoLoc((data)=>{
      console.log("driver location data :",data)
      if(data != "-1")
      {
        console.log("!= -1 ")
        this.storage.get('info').then(val=>{
          console.log("user ",val)
          this.myid = val.alldata.id
          this.srv.updateDriverLocation(this.myid,data.inspectorLat,data.inspectorLong,resp=>{
            console.log("resp from updateDriverLocation : ",resp)

          },err=>{
            console.log("err from updateDriverLocation : ",err)
          })  
          
          
        
        });
        
      } 

    })
    this.noorders = true
    this.storage.get('info').then(val=>{
      console.log("user ",val)
      this.myid = val.alldata.id
      this.statusVal = val.alldata.online
      if(this.statusVal == "0")
        this.statusColor = "#a29b9b"
      else if (this.statusVal == "1")
        this.statusColor = "#158e15"
    this.srv.getDriverOrders(this.myid,resp=>{
      console.log("resp ffrom getUserOrders",resp)
  
      this.ordersArr= JSON.parse(resp).Orders
      if(this.ordersArr.length == 0){
  this.noorders = false
      }else{
        this.noorders = true
      for(var j=0;j<this.ordersArr.length;j++){
        this.ordersArr[j].color="#abacae"
        this.ordersArr[j].notificationimage="assets/icon/notification.png"
        this.ordersArr[j].txt =this.ordersArr[j].description
        this.ordersArr[j].date = this.ordersArr[j].created_at.split(" ")[0]
        // this.ordersArr[j].time= this.datepipe.transform(this.ordersArr[j].created_at, 'h:m a')
        //order status 
    //0 create
    //1 driver accept
    //2 done
    //3 user cancel
    //4 driver cancel
    if(this.ordersArr[j].status == "1"){
      this.ordersArr[j].x="#9E9E9E"
      // this.ordersArr[j].statusTxt = this.translate.instant("orderaccept")
    }
    else if(this.ordersArr[j].status == "2"){
      this.ordersArr[j].x="rgb(94, 130, 94)"
      // this.ordersArr[j].statusTxt = this.translate.instant("orderdone")
    }
    else if(this.ordersArr[j].status == "3"){
      this.ordersArr[j].x="#e01213"
      // this.ordersArr[j].statusTxt = this.translate.instant("ordercancelled")
    }
    
  
      }
    }
    
    },err=>{
      console.log("err ffrom getUserOrders",err)
      this.noorders = true
    })
  })

  }

  opendriverProfile(){
    this.navCtrl.setRoot('ProfilePage',{from:"driver"})
  }
  changeStatus(ev){
    
    console.log("change status : ",ev," , status val :",this.statusVal)
    if(this.statusVal == "0")
      this.statusColor = "#a29b9b"
    else if (this.statusVal == "1")
      this.statusColor = "#158e15"

        //0 off
//1 online
// online
//id
// changeOnline
this.storage.get('info').then(val=>{
  console.log("user ",val)
  var userdata = val
  this.myid = val.alldata.id
  this.srv.driverOnlineOffline(this.myid,this.statusVal,resp=>{
    console.log("resp from driverOnlineOffline : ",resp)
    
    userdata.alldata.online = this.statusVal
    console.log("user data with online status :",userdata)
    this.storage.set('info',userdata);
  },err=>{
    console.log("err from driverOnlineOffline : ",err)
  });
});
     

  }

  scheduleNotification(txt,orderId,desc,userid,driver_id,lat1,lng1,price,type,userLat,userLng) {
    
  
    this.localNotifications.schedule({
      id: orderId,
      title: this.translate.instant("appTitle"),
      text:   txt,
      data: { mydata: desc,x:userid,driverId:driver_id},
      trigger:{ at: new Date(new Date().getTime())}
  
    });

    var xdata= {orderId : orderId,desc: desc,userId:userid,driverId:driver_id,lat:lat1,lng:lng1,price:price,type:type,userLat:userLat,userLng:userLng}
    console.log("xdata: ",xdata)
    this.navCtrl.setRoot("ShowOrderDetailsPage",xdata)

  //  this.localNotifications.on('click').subscribe(resp=>{
  //   // alert ("notification resp : "+JSON.stringify( resp))
  //   console.log("notification resp : ",resp)
  //   var nresp = JSON.parse(JSON.stringify(resp))
  //   var xdata= {orderId : nresp.id,desc:nresp.data.mydata,userId:nresp.data.x,driverId:nresp.data.driverId}
  //   console.log("xdata: ",xdata)
  //   this.navCtrl.setRoot("ShowOrderDetailsPage",xdata)
  //  },err=>{
  //   //  alert ("notification err : "+JSON.stringify( err))
  //  })


  }

 
  scheduleNotification2() {
    
  
    this.localNotifications.schedule({
      // id: orderId,
      title: this.translate.instant("appTitle"),
      text:   this.translate.instant("orderFinished"),
      // data: { mydata: desc,x:userid,driverId:driver_id},
      trigger:{ at: new Date(new Date().getTime())}
  
    });

    // var xdata= {orderId : orderId,desc: desc,userId:userid,driverId:driver_id,lat:lat1,lng:lng1,price:price,type:type,userLat:userLat,userLng:userLng}
    // console.log("xdata: ",xdata)
    // this.navCtrl.setRoot("ShowOrderDetailsPage",xdata)

  //  this.localNotifications.on('click').subscribe(resp=>{
  //   // alert ("notification resp : "+JSON.stringify( resp))
  //   console.log("notification resp : ",resp)
  //   var nresp = JSON.parse(JSON.stringify(resp))
  //   var xdata= {orderId : nresp.id,desc:nresp.data.mydata,userId:nresp.data.x,driverId:nresp.data.driverId}
  //   console.log("xdata: ",xdata)
  //   this.navCtrl.setRoot("ShowOrderDetailsPage",xdata)
  //  },err=>{
  //   //  alert ("notification err : "+JSON.stringify( err))
  //  })


  }


pusherLocation(){
  // console.log("pusher trigger")
  // this.channel1 = this.locationpusher.init();
  // this.channel1.bind('pusher:subscription_succeeded', (resp) =>{
  //   console.log("resp from bind : ",resp)
  //   var triggered = this.channel1.trigger('client-someeventname', { your: "data" });
  //   console.log("trigger :",triggered)
  // },err=>{
  //   console.log("err from bind : ",err)
  // });
  
  //  channel2.bind('pusher:subscription_succeeded', function() {
    // var triggered = this.channel1.trigger('client-someeventname', { data:"samar"});
    // console.log("trigger :",triggered)
  // });
  var pusher = new Pusher('6d505d3ed5dd3857b199');
var channel = pusher.subscribe('private-channel');
channel.bind('pusher:subscription_succeeded', (resp)=> {
  console.log("resp from subscription_succeeded : ",resp)
  var triggered = channel.trigger('client-someeventname', { your: "data" });
},err=>{
  console.log("err from subscription_succeeded : ",err)
});



}

openmap(){
  console.log("openmap ")
  this.navCtrl.push("MapPage")
}
}
