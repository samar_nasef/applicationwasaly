import { Component ,NgZone} from '@angular/core';
import { NavController,AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { HelperProvider } from '../../providers/helper/helper';
import { ServiceProvider } from '../../providers/service/service';
import { Storage } from '@ionic/storage';
import { PusherServiceProvider } from '../../providers/pusher-service/pusher-service'
declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  scaleClass="";
  langDirection = "";

  search
  searchtxt
  alertdir


  // driversArr =[{id:1,name:"name",carno:"12356",carType:"aaaa",img:"assets/imgs/default-avatar.png"},
  // {id:2,name:"name",carno:"12356",carType:"aaaa",img:"assets/imgs/default-avatar.png"},
  // {id:3,name:"name",carno:"12356",carType:"aaaa",img:"assets/imgs/default-avatar.png"}]

  driversArr =[]
  GoogleAutocomplete
  autocomplete
  autocompleteItems
  geocoder

  searchtra
  nametra
  cartypetxttra
  carnotxttra
  requestOrdertra
  position
  myid
  desc
  nodata = true
  constructor(private pusher : PusherServiceProvider,public storage:Storage, public srv:ServiceProvider, private zone:NgZone,public alertController: AlertController, public helper:HelperProvider, public translate: TranslateService,public navCtrl: NavController) {

    // const channel = this.pusher.init();
    // channel.bind('my-event', (data) => {
    //   console.log("myevent data from pusher : ",data)
    //   alert("my event data : "+JSON.stringify(data))
    //   // if(data.score >= 1){
    //   //   this.rating.good = this.rating.good + 1;
    //   // }
    //   // else{
    //   //   this.rating.bad = this.rating.bad + 1;
    //   // }
    //   // this.comments.push(data);
    // });
    // channel.bind('mychannel', (data) => {
    //   console.log("mychannel data from pusher : ",data)
    // });
    //0 off
//1 online
// online
//id
// changeOnline
this.nodata = true
    // this.srv.driverOnlineOffline(6,"1",resp=>{
    //   console.log("resp from driverOnlineOffline : ",resp)
    // },err=>{
    //   console.log("err from driverOnlineOffline : ",err)
    // });
    this.langDirection = this.helper.lang_direction;
console.log("langDirection from home :",this.langDirection)
     
// translate.get('init').subscribe((text:string) => {
//   //use instant call here
//   this.searchtxt = this.translate.instant("search")
//   console.log("this.searchtxt from init: ",this.searchtxt)

//   });

    if(this.langDirection == "rtl"){
      this.scaleClass="scaleClassrtl";
      this.alertdir = "rtldir"
      this.searchtra = "بحث"
      this.searchtxt = "بحث"
      this.nametra = "الإسم"
      this.cartypetxttra = "نوع السيارة"
      this.carnotxttra = "رقم السيارة"
      this.requestOrdertra = "اطلب"
      // this.translate.setDefaultLang('ar');
      // this.translate.use('ar')
//      console.log("ar this.translate.currentLang : ",this.translate.currentLang);
      // .subscribe(resp=>{
      //   console.log("resp from use ar ",resp)
      // },err=>{
      //   console.log("err from use ar ",err)
      // });;
      // this.searchtxt = this.translate.instant("search")
      // console.log("this.searchtxt from rtl: ",this.searchtxt)
    }
    else{
      this.scaleClass="scaleClassltr";
      this.alertdir = "ltrdir"
      this.searchtra = "Search"
      this.searchtxt = "Search"
      this.nametra = "Name"
      this.cartypetxttra = "Car Type"
      this.carnotxttra = "Car Number"
      this.requestOrdertra = "Order"
      // this.translate.setDefaultLang('en');
      // this.translate.use('en')
      // console.log("en this.translate.currentLang : ",this.translate.currentLang);
      // .subscribe(resp=>{
      //   console.log("resp from use en ",resp)
      // },err=>{
      //   console.log("err from use en ",err)
      // });
      // this.searchtxt = this.translate.instant("search")
      // console.log("this.searchtxt from ltr: ",this.searchtxt)

    }
      

    // this.searchtxt = this.translate.instant("search")
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.geocoder = new google.maps.Geocoder;
    this.autocomplete = { input: '' };
    this.autocompleteItems = [];

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad home');
    this.nodata = true
    this.helper.geoLoc(()=>{})
  
  }
  ionViewDidEnter(){
    console.log("ionViewDidEnter home")
    
    this.storage.get('info').then(val=>{
      console.log("user ",val)
      this.myid = val.alldata.id
    })
    // console.log("from enter  this.translate.instant(search) :", this.translate.instant("search"))
    // this.searchtxt = this.translate.instant("search")
  }
  searchFunc(){
    console.log("searchFunc")
    if(!this.autocomplete.input)
      this.helper.presentToast(this.translate.instant("entersearchword"))
    else{
      this.srv.searchForDrivers(this.position,resp=>{
        console.log("resp from search",resp)
        var respdata = JSON.parse(resp)
        if(respdata.code == 200){
          console.log("respdata.data.length: ",respdata.data.length)
          if(respdata.data.length != 0 ){
            this.driversArr  = respdata.data
            this.nodata = true
          }
          else{
            this.driversArr  =[]
            this.nodata = false
          //  this.helper.presentToast(this.translate.instant("nodrivers"))
          }
            
        }else if(respdata.code == 300){

        }
        
      },err=>{
        console.log("err from search",err)
      })
    }
  }
  requestOrder(item){
    console.log("requestOrder item : ",item);
    this.presentAlertPrompt(item)
  }
  presentAlertPrompt(item) {
console.log("item",item)
    var arrtra = []
    if(this.helper.lang_direction == "rtl"){
      arrtra[0] = "اطلب"
      arrtra[1] = "اسم الطلب"
      arrtra[2] = "تفاصيل الرحلة"
      arrtra[3] = "إلغاء"
      arrtra[4] = "اطلب الآن "

    }else if(this.helper.lang_direction == "ltr"){
      arrtra[0] = "Order"
      arrtra[1] = "Order Name"
      arrtra[2] = "Trip Details"
      arrtra[3] = "Cancel"
      arrtra[4] = "Order Now"
    }
    const alert = this.alertController.create({
      title: arrtra[4],
      cssClass: this.alertdir,
      inputs: [
        // {
        //   name: "orderName",
        //   type: 'text',
        //   placeholder: arrtra[1]
        // },
        {
          name: 'orderDetails',
          type: 'text',
          placeholder: arrtra[2]
        }
      ],
      buttons: [
        {
          text: arrtra[3],
          role: 'cancel',
          // cssClass: this.alertdir,
          handler: () => {
            console.log('Confirm Cancel');
            if(this.langDirection == "rtl")
              this.helper.presentToast("لم يتم إرسال الطلب")
            else 
              this.helper.presentToast("The order wasn't sent")
          
          }
        }, {
          text:arrtra[4],
          handler: (data) => {
            console.log('Confirm Ok',data);
            
            var ordernameerr 
            var orderDetailserr
            var ordersent
            if(this.langDirection == "rtl"){
              ordernameerr = "الرجاء إدخال اسم الطلب"
              orderDetailserr = "الرجاء إدخال تفاصيل الرحلة"
              ordersent = "تم إرسال الطلب بنجاح"
            }
            else{
              ordernameerr = "Please Enter Order Name"
              orderDetailserr = "Please Enter Trip Details"
              ordersent = "Order Sent Successfully"
            }
            // if(!data.orderName){
            //   this.helper.presentToast(ordernameerr)
            //   return false
            // }
              
            if(!data.orderDetails){
              this.helper.presentToast(orderDetailserr)
              return false
            }

            var apidata = {
            'driver_id':item.id,
      "user_id":this.myid,
      "description":data.orderDetails,
      "address":this.desc,
      "lat":this.position.lat,
      "lng":this.position.lng,
            }
            console.log("apidata :",apidata)
            this.srv.createOrder(apidata,resp=>{
              console.log("resp from create order",resp)
              if(JSON.parse(resp).code == 200){
                // this.helper.presentToast(ordersent)
                this.presentThanks("",ordersent)
              }
               
              else 
                this.helper.presentToast(this.translate.instant("connectionErr"))
            },err=>{
              console.log("err from create order",err)
            })
            

          }
        }
      ]
      //,enableBackdropDismiss: false
    });

    alert.present();
  }
  updateSearchResults(){
    console.log("updateSearchResults");
    if (this.autocomplete.input == '') {
      this.autocompleteItems = [];
      this.driversArr = []
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input },
    (predictions, status) => {
      this.autocompleteItems = [];
      this.zone.run(() => {
        predictions.forEach((prediction) => {
          this.autocompleteItems.push(prediction);
        });
      });
    });
  }
  selectSearchResult(item){
    console.log("selectSearchResult , ",item)
    this.desc = item.description
    this.autocompleteItems= []
    
    // 'placeId': item.place_id
    //'address':item.description
    this.geocoder.geocode({'placeId': item.place_id}, (results, status) => {
      console.log("geocoder.geocode results : ",results)
      console.log("geocoder.geocode status : ",status)
      console.log("results[0].geometry.location.lat : ",results[0].geometry.location.lat())
      console.log("results[0].geometry.location.lng : ",results[0].geometry.location.lng())
      if(status === 'OK' && results[0]){
        this.position = {
            lat: results[0].geometry.location.lat(),
            lng: results[0].geometry.location.lng()
        };
        console.log("item.position : ",this.position);
        this.srv.searchForDrivers(this.position,resp=>{
          console.log("resp from search",resp)
          var respdata = JSON.parse(resp)
          if(respdata.code == 200){
            console.log("respdata.data.length: ",respdata.data.length)
            if(respdata.data.length != 0 ){
              this.driversArr  = respdata.data
              this.nodata = true
            }
            else{
              this.driversArr  =[]
              this.nodata = false
            //  this.helper.presentToast(this.translate.instant("nodrivers"))
            }
              
          }else if(respdata.code == 300){

          }
          
        },err=>{
          console.log("err from search",err)
        })
        // let marker = new google.maps.Marker({
        //   position: results[0].geometry.location,
        //   map: this.map,
        // });
        // this.markers.push(marker);
        // this.map.setCenter(results[0].geometry.location);
      }
    })
  }
  // doRefresh(event){

  // }

  details(item){
    console.log("item : ",item)
    // this.navCtrl.push("DetailsPage",{item:item})
  }
  presentThanks(txt,msg) {
    console.log("txt",txt)
        
        const alert = this.alertController.create({
          // title: txt,
          message:msg,
          cssClass: this.alertdir,
          buttons: [this.translate.instant("agree")]
        });
    
        alert.present();
      }
      mapwithsearch(){
        console.log("mapwithsearch")
        this.navCtrl.push("UserHomePage")
      }
}
