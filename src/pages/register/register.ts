import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content,ActionSheetController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { HelperProvider } from '../../providers/helper/helper';
import { matchOtherValidator } from '../../validators/passwordValidator';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ServiceProvider } from '../../providers/service/service';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  @ViewChild(Content) content: Content;

  userimg
  private registerForm : FormGroup;
  email
  password
  confirmpassword
  mobile;
  name
  accounttype
  carSize
  submitAttempt = false;
  errors = {
    nameErr:"",
    passworrdErr:"",
    mobileErr:"",
    confirmpasswordErr:"",
    emailErr:""
  };

  langDirection = "";
  placeholder = {cartype:"",carnumber:"",mobile:"",passworrd:"",confirmpassword:"",name:"",email:""};

  NationalIDImg=""
  NationalIDImgdata=""

  drivinglicenseImg=""
  drivinglicenseImgdata=""

  CriminalrecordImg = ""
  Criminalrecorddata = ""

  carnumber
  scaleClass="";

  cartype
  userImg=""
  userimgdata=""


  constructor(public srv:ServiceProvider, public camera:Camera, public actionSheetCtrl: ActionSheetController,public helper:HelperProvider, public translate: TranslateService,private formBuilder: FormBuilder,public navCtrl: NavController, public navParams: NavParams) {
    this.accounttype="1"

    this.langDirection = this.helper.lang_direction;
    
    if(this.langDirection == "rtl")
      this.scaleClass="scaleClassrtl";
    else
      this.scaleClass="scaleClassltr";

    this.registerForm = this.formBuilder.group({
      email:['', Validators.compose([Validators.required,Validators.pattern("^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$")])],
      confirmpassword: ['', Validators.compose([Validators.minLength(6),  Validators.required, matchOtherValidator('password')])],
      name :['',Validators.required],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      // mobile: ['', Validators.compose([Validators.required,Validators.pattern("[0-9]{11}")])]
      mobile: ['', Validators.required]
    });
this.placeholder.mobile = this.translate.instant("mobile")
this.placeholder.passworrd = this.translate.instant("passworrd")
this.errors.passworrdErr = this.translate.instant("passworrdErr")
this.errors.mobileErr = this.translate.instant("mobileErr")
this.placeholder.confirmpassword = this.translate.instant("enterconfirmPassword")
this.errors.confirmpasswordErr = this.translate.instant("confirmpasswordErr")
  
this.placeholder.name = this.translate.instant("enterName")
this.errors.nameErr = this.translate.instant("nameerr")

this.placeholder.email = this.translate.instant("entermail")
this.errors.emailErr = this.translate.instant("mailerr")

this.placeholder.carnumber = this.translate.instant("entercarno")
this.placeholder.cartype = this.translate.instant("entercartype")
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
    this.accounttype="1"
  }

  fbLogin(){
    console.log("fbLogin")
  }

  loginOrRegister(){
  if(! this.registerForm.valid ){
    this.submitAttempt=true;
    this.content.scrollToTop(3000);

    
    if(this.registerForm.controls["password"].errors){
      if(this.registerForm.controls["password"].errors['required'])
        this.errors.passworrdErr = this.translate.instant("passworrdErr");
      else if (this.registerForm.controls["password"].errors['minlength'])
        this.errors.passworrdErr = this.translate.instant("passinvaild");
      else
        console.log("passErrors:",this.registerForm.controls["password"].errors);
    
    }


    if(this.registerForm.controls["confirmpassword"].errors){
      if(this.registerForm.controls["confirmpassword"].errors['required'])
        this.errors.confirmpasswordErr = this.translate.instant("confirmpasswordErr");
      else if (this.registerForm.controls["confirmpassword"].errors['minlength'])
        this.errors.confirmpasswordErr = this.translate.instant("passinvaild");
      else if(this.registerForm.controls["confirmpassword"].errors['matchOther'])
        this.errors.confirmpasswordErr = this.translate.instant("passwordNotConfirmed");
      else
        console.log("confirmpasswordErrors:",this.registerForm.controls["confirmpassword"].errors);
    
    }


    if(this.registerForm.controls["mobile"].errors){
      if(this.registerForm.controls["mobile"].errors['required'])
      {
        this.errors.mobileErr = this.translate.instant("mobileErr");
      }else if(this.registerForm.controls["mobile"].errors['pattern']) {
        this.errors.mobileErr = this.translate.instant("invalidMobile");
      }else{
        console.log("phone errors:",this.registerForm.controls["mobile"].errors);
      }
    }


    if(this.registerForm.controls["email"].errors){
      if(this.registerForm.controls["email"].errors['required'])
      {
        this.errors.emailErr = this.translate.instant("mailerr");
      }else if(this.registerForm.controls["email"].errors['pattern']) {
        this.errors.emailErr = this.translate.instant("invalidmail");
      }else{
        console.log("email errors:",this.registerForm.controls["email"].errors);
      }
    }

  }else{
    var data = {password:this.password,mobile:this.mobile}
    console.log("valid data : ",data)
    if(this.accounttype == "2"){
      console.log("data for driver : accounttype == 2")

      if(!this.carnumber)
      {
        this.helper.presentToast(this.translate.instant("carNoerr"))
        return
      }

      if(!this.cartype)
      {
        this.helper.presentToast(this.translate.instant("cartypeerr"))
        return
      }

      
      if(!this.carSize)
      {
        this.helper.presentToast(this.translate.instant("carsizeerr2"))
        return
      }

      if(!this.NationalIDImg)
      {
        this.helper.presentToast(this.translate.instant("NationalIDerr"))
        return
      }

      if(!this.drivinglicenseImg)
      {
        this.helper.presentToast(this.translate.instant("drivinglicenseerr"))
        return
      }

      if(!this.CriminalrecordImg)
      {
        this.helper.presentToast(this.translate.instant("Criminalrecorderr"))
        return
      }

      console.log("call api for driver")
      var driverData ={
        'name' : this.name,
        'phone': this.mobile,
        'password':this.password,
        'img':this.userimgdata,
        'roles':3,
        'email':this.email,
        'id_img':this.NationalIDImgdata,
        'id_car':this.drivinglicenseImgdata,
        'car_type':this.cartype,
        "car_num":this.carnumber,
        "criminal_img":this.Criminalrecorddata,
        "type":this.carSize
      }
      
this.srv.register(driverData,resp=>{
  console.log("resp from driver register",resp)
  
  var respData = JSON.parse(resp)
  if(respData.code  == "200"){
    console.log("code 200")
    this.navCtrl.setRoot("ActivationCodePage",{from:"register",type:"driver",mobile:this.mobile,password:this.password,activation:respData.userdata.verificationcode,id:respData.userdata.id})
    
  }else  if(respData.code  == "300"){
    console.log("code 300,",respData.error)
    if(respData.error.phone){
      this.helper.presentToast(respData.error.phone[0])
      console.log("respData.error.phone",respData.error.phone)
        }
     
    else if(respData.error.email[0])
    {
      this.helper.presentToast(respData.error.email[0])
      console.log("respData.error.email",respData.error.email[0])
        }else{
          this.helper.presentToast(this.translate.instant("connectionErr"))
        }
      
  }
},err=>{
  console.log("err from driver register ",err);
})


    }else if(this.accounttype == "1"){
      console.log("data for user : accounttype == 1")
      console.log("call api for user")
      var userdata={
        'name' : this.name,
        'phone': this.mobile,
        'password':this.password,
        'img':this.userimgdata,
        'roles':2,
        'email':this.email,
        'id_img':"",
        'id_car':"",
        'car_type':"",
        "car_num":"",
        "criminal_img":""
      }

      this.srv.register(userdata,resp=>{
        console.log("resp from user register",resp)
        var respData = JSON.parse(resp)
        if(respData.code  == "200"){
          console.log("code 200 v",respData.userdata.verificationcode)
          this.navCtrl.setRoot("ActivationCodePage",{from:"register",type:"user",mobile:this.mobile,password:this.password,activation:respData.userdata.verificationcode,id:respData.userdata.id})

        }else  if(respData.code  == "300"){
          console.log("code 300")

          if(respData.error.phone){
            this.helper.presentToast(respData.error.phone[0])
            console.log("respData.error.phone",respData.error.phone)
              }
           
          else if(respData.error.email[0])
          {
            this.helper.presentToast(respData.error.email[0])
            console.log("respData.error.email",respData.error.email[0])
              }else{
                this.helper.presentToast(this.translate.instant("connectionErr"))
              }
        }
        
      },err=>{
        console.log("err from user register ",err);
      })
   
    }
    // if(this.type == "login"){
    //   console.log("login")
    //   // this.navCtrl.setRoot(TabsPage)
    // }else if (this.type == "register"){
    //   console.log("register")
    //   this.navCtrl.setRoot("ActivationCodePage",{from:"register",mobile:data.mobile})
    // }
  }
}


presentActionSheet(imgType) {

  let actionSheet = this.actionSheetCtrl.create({
    title: this.translate.instant("SelectImageSource"),
    buttons: [
      {
        text: this.translate.instant("LoadfromLibrary"),
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY,imgType);
        }
      },
      {
        text: this.translate.instant("UseCamera"),
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA,imgType);
        }
      },
      {
        text: this.translate.instant("cancel"),
        role: 'cancel'
      }
    ]
  });
  actionSheet.present();

}

public takePicture(sourceType,imgType) {
  // Create options for the Camera Dialog
  // allowEdit:true,
  // targetWidth:200,
  // targetHeight:200
  var options = {
    quality: 80, //50
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    sourceType: sourceType,
    saveToPhotoAlbum: false,
    correctOrientation: true
   
  };
  this.camera.getPicture(options).then((imageData: string) => {
 
    console.log("imgType from get picture :",imgType)
    if(imgType == '1'){
      console.log("from if NationalID ")
      this.NationalIDImg = 'data:image/jpeg;base64,' + imageData;
      this.NationalIDImgdata = encodeURIComponent(imageData)
      // api -> NationalIDImgdata, 'jpeg'
    }else if (imgType == '2'){
      console.log("from if drivinglicense ")
      this.drivinglicenseImg = 'data:image/jpeg;base64,' + imageData;
      this.drivinglicenseImgdata = encodeURIComponent(imageData)
    }else if (imgType == '3'){
      console.log("from if Criminalrecord ")
      this.CriminalrecordImg = 'data:image/jpeg;base64,' + imageData;
      this.Criminalrecorddata = encodeURIComponent(imageData)
    }else if (imgType == '4'){
      console.log("from if user img ")
      this.userImg = 'data:image/jpeg;base64,' + imageData;
      this.userimgdata = encodeURIComponent(imageData)
    }else{
      console.log("from else img type")

    }
   

   


    
  }, (err) => {
    // Handle error
  });
}

dismiss(){
  this.navCtrl.pop();
}



}
