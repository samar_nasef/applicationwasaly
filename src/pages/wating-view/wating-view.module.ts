import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WatingViewPage } from './wating-view';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    WatingViewPage,
  ],
  imports: [
    IonicPageModule.forChild(WatingViewPage),
    TranslateModule.forChild()
  ],
})
export class WatingViewPageModule {}
