import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,Events} from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { UserHomePage } from '../user-home/user-home';
/**
 * Generated class for the WatingViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-wating-view',
  templateUrl: 'wating-view.html',
})
export class WatingViewPage {

  langDirection = "";
  scaleClass="";
  timer;
  time=60;
  show
  constructor(public events: Events,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
this.show = true

events.subscribe('user:Cleartimer', () => {
  console.log("su clear time ")
  if(this.timer)
  {
    clearTimeout(this.timer);
    console.log("timer stopped")
  } 

});     
    if(this.langDirection == "rtl")
      this.scaleClass="scaleClassrtl";
    else
      this.scaleClass="scaleClassltr";


      this.enableTimer();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WatingViewPage');
  }
  enableTimer(){
    this.timer =setInterval(()=>{
      this.time--;
        if(this.time <= 0){
          console.log("timer off");
       
          clearTimeout(this.timer);
          this.show = false;
        }
    },1000);
  }

  dismiss(){
    this.helper.fromLocation = ""
    this.helper.toLocation=""
    
    this.navCtrl.setRoot(UserHomePage);

  }
}
