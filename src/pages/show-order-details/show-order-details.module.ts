import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowOrderDetailsPage } from './show-order-details';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ShowOrderDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowOrderDetailsPage),
    TranslateModule.forChild()
  ],
})
export class ShowOrderDetailsPageModule {}
