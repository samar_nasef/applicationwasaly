import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { HelperProvider } from '../../providers/helper/helper';
import { ServiceProvider } from '../../providers/service/service';
import { TranslateService } from '@ngx-translate/core';
import { DriverHomePage } from '../driver-home/driver-home';
// import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
// import { Geolocation, Geoposition } from '@ionic-native/geolocation';

// const config: BackgroundGeolocationConfig = {
//   desiredAccuracy: 10,
//   stationaryRadius: 20,
//   distanceFilter: 30,
//   debug: true, //  enable this hear sounds for background-geolocation life-cycle.
//   stopOnTerminate: false, // enable this to clear background location settings when the app terminates
//     };

@IonicPage()
@Component({
  selector: 'page-show-order-details',
  templateUrl: 'show-order-details.html',
})
export class ShowOrderDetailsPage {

  scaleClass="";
  langDirection = "";
  userImageUrl = "assets/imgs/default-avatar.png"
  item = {mobile:"12345632123",id:2,name:"name",carno:"12356",carType:"aaaa",img:"assets/imgs/default-avatar.png"}
  nametra
  cartypetxttra
  carnotxttra
  driverdata
  mobiletra
  
  orderId
  desc
  userId
  driverId
  watch

  lngfromPusher
  latfromPusher

  price
  type

  userLng
  userLat

  constructor(public translate:TranslateService,
    // private geolocation:Geolocation, private backgroundGeolocation: BackgroundGeolocation,
    public srv:ServiceProvider, public helper:HelperProvider, public navCtrl: NavController, public navParams: NavParams) {
   
//     this.backgroundGeolocation.configure(config)
//     .subscribe((location: BackgroundGeolocationResponse) => {
  
//       console.log("location : ",location);
  
//       // IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
//       // and the background-task may be completed.  You must do this regardless if your HTTP request is successful or not.
//       // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
//       this.backgroundGeolocation.finish(); // FOR IOS ONLY
  
//     });

//    // start recording location
// this.backgroundGeolocation.start();


// If you wish to turn OFF background-tracking, call the #stop method.
// this.backgroundGeolocation.stop();


// this.startTracking()
   
    this.langDirection = this.helper.lang_direction
    if(this.langDirection == "rtl")
    {
      this.scaleClass="scaleClassrtl";
    }  
    else{
      this.scaleClass="scaleClassltr";
     
    }
    // {orderId : nresp.id,desc:nresp.data.mydata,userId:nresp.data.x}

//     price: null
// type: "0"
    this.orderId = this.navParams.get("orderId")
    this.desc = this.navParams.get("desc")
    this.userId = this.navParams.get("userId")
    this.driverId = this.navParams.get("driverId")
    this.latfromPusher = this.navParams.get("lat")
    this.lngfromPusher =  this.navParams.get("lng")
    this.userLat = this.navParams.get("userLat")
    this.userLng = this.navParams.get("userLng")
    console.log("userLat : ",this.userLat , " , userLng :",this.userLng)
    // price:price,type:type
    this.price = this.navParams.get("price")
    
    if(this.navParams.get("type") == 0)
      this.type = this.translate.instant("smallcar")
    else if(this.navParams.get("type") == 1)
      this.type = this.translate.instant("bigcar")

    console.log("order id : ",this.orderId , ", desc : ",this.desc," userId : ",this.userId," , driverId: ",this.driverId)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShowOrderDetailsPage');
  }

  reject(){
    var userdata={
      'user_id' : this.userId,
      'Drvier_id': this.driverId,
      'order_id':this.orderId
    }
    console.log("reject data :",userdata)
this.srv.rejectOrder(userdata,resp=>{
  console.log("resp from rejectOrder : ",resp)
  // if(JSON.parse(resp).code ==  "200"){
    this.helper.presentToast(this.translate.instant("orderRejected"))
    // this.navCtrl.pop()
    this.navCtrl.setRoot(DriverHomePage)
  // }
  
},err=>{
  console.log("err from rejectOrder : ",err)
})
  }


  accept(){
    var userdata={
      'user_id' : this.userId,
      'Drvier_id': this.driverId,
      'order_id':this.orderId
    }
    console.log("accept data :",userdata)
this.srv.acceptOrder(userdata,resp=>{
  console.log("resp from acceptOrder : ",resp , " , code : ",JSON.parse(JSON.stringify(resp)).code )
  // if(JSON.parse(resp).code ==  200){
    console.log("status 200")
    this.helper.presentToast(this.translate.instant("orderaccepted"))
    this.helper.createuser(this.driverId,this.helper.lat+","+this.helper.lng,this.userLat+","+this.userLng,this.latfromPusher+","+this.lngfromPusher)
    this.helper.startTimerTogetLocation(this.driverId)
    // this.navCtrl.pop()
    // this.navCtrl.setRoot(DriverHomePage)
    // DriverMapPage
this.navCtrl.setRoot('DriverMapPage',{lat:this.latfromPusher,lng:this.lngfromPusher,userLat:this.userLat,userLng:this.userLng})
  // }
  

},err=>{
  console.log("err from acceptOrder : ",err)
})
  }

  dismiss(){
    // this.navCtrl.pop()
this.navCtrl.setRoot(DriverHomePage)
  }

 

  // startTracking() {

  //   // Background Tracking

  //   let config = {
  //     desiredAccuracy: 0,
  //     stationaryRadius: 20,
  //     distanceFilter: 10, 
  //     debug: true,
  //     interval: 2000 
  //   };

  //   this.backgroundGeolocation.configure(config).subscribe((location) => {

  //     console.log('BackgroundGeolocation:  ' + location.latitude + ',' + location.longitude);

  //     // Run update inside of Angular's zone
  //     // this.zone.run(() => {
  //     //   this.lat = location.latitude;
  //     //   this.lng = location.longitude;
  //     // });

  //   }, (err) => {

  //     console.log(err);

  //   });

  //   // Turn ON the background-geolocation system.
  //   this.backgroundGeolocation.start();


  //   // Foreground Tracking

  // let options = {
  //   frequency: 3000, 
  //   enableHighAccuracy: true
  // };

  // this.watch = this.geolocation.watchPosition(options).filter((p: any) => p.code === undefined).subscribe((position: Geoposition) => {

  //   console.log( "position : ", position);

  //   // Run update inside of Angular's zone
  //   // this.zone.run(() => {
  //   //   this.lat = position.coords.latitude;
  //   //   this.lng = position.coords.longitude;
  //   // });

  // });

  // }

  // stopTracking() {

  //   console.log('stopTracking');

  //   this.backgroundGeolocation.finish();
  //   this.watch.unsubscribe();

  // }
  
}
