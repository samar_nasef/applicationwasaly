import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HelperProvider } from '../../providers/helper/helper';

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {

  scaleClass="";
  langDirection = "";
  userImageUrl = "assets/imgs/default-avatar.png"
  // item = {mobile:"12345632123",id:2,name:"name",carno:"12356",carType:"aaaa",img:"assets/imgs/default-avatar.png"}
  // item = {phone:"",id:2,name:"",car_num:"",carType:"",img:"assets/imgs/default-avatar.png"}
  phone
  id
  name
  car_num
  carType
  img="assets/imgs/default-avatar.png"

  nametra
  cartypetxttra
  carnotxttra
  driverdata
  mobiletra
driverObjec
  constructor(public helper:HelperProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    console.log("langDirection from home :",this.langDirection)
         
   
    this.driverdata =  this.navParams.get("item")
    console.log("driver datav from  detials : ",this.driverdata)
    this.phone = this.driverdata.phone
    this.id = this.driverdata.id
    this.name = this.driverdata.name
    this.car_num = this.driverdata.car_num
    console.log("this.driverdata.carType : ",this.driverdata.carType)
    this.carType = this.driverdata.carType

        if(this.langDirection == "rtl"){
          this.scaleClass="scaleClassrtl";
          this.nametra = "الإسم"
          this.cartypetxttra = "نوع السيارة"
          this.carnotxttra = "رقم السيارة"
          this.mobiletra = "رقم الهاتف"
        }
        else{
          this.scaleClass="scaleClassltr";
          this.nametra = "Name"
          this.cartypetxttra = "Car Type"
          this.carnotxttra = "Car Number"
          this.mobiletra = "Mobile Number"

        }
          
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsPage');
  }
  dismiss(){
    this.navCtrl.pop();
  }

}
