import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,Events} from 'ionic-angular';


import { ServiceProvider } from '../../providers/service/service';


import { Storage } from '@ionic/storage';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { HelperProvider } from '../../providers/helper/helper';
import { HomePage } from '../home/home';
// import { ServiceProvider } from '../../providers/service/service';
import { DriverHomePage } from '../driver-home/driver-home';
import { UserHomePage } from '../user-home/user-home';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  private registerForm : FormGroup;
  password
  mobile;
  submitAttempt = false;
  errors = {
    passworrdErr:"",
    mobileErr:""
  };

  langDirection = "";
  placeholder = {mobile:"",passworrd:""};

  loginorregister
 
  scaleClass="";
 
  constructor(public srv:ServiceProvider, public events:Events, public helper:HelperProvider, public translate: TranslateService,private formBuilder: FormBuilder,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;

     
    if(this.langDirection == "rtl")
      this.scaleClass="scaleClassrtl";
    else
      this.scaleClass="scaleClassltr";
 

    this.registerForm = this.formBuilder.group({
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      // mobile: ['', Validators.compose([Validators.required,Validators.pattern("[0-9]{11}")])]
      mobile: ['', Validators.required]
    });
this.placeholder.mobile = this.translate.instant("mobile")
this.placeholder.passworrd = this.translate.instant("passworrd")
this.errors.passworrdErr = this.translate.instant("passworrdErr")
this.errors.mobileErr = this.translate.instant("mobileErr")

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  fbLogin(){
    console.log("fbLogin")
  }

  loginOrRegister(){
  if(! this.registerForm.valid ){
    this.submitAttempt=true;

    if(this.registerForm.controls["password"].errors){
      if(this.registerForm.controls["password"].errors['required'])
        this.errors.passworrdErr = this.translate.instant("passworrdErr");
      else if (this.registerForm.controls["password"].errors['minlength'])
        this.errors.passworrdErr = this.translate.instant("passinvaild");
      else
        console.log("passErrors:",this.registerForm.controls["password"].errors);
    
    }

    if(this.registerForm.controls["mobile"].errors){
      if(this.registerForm.controls["mobile"].errors['required'])
      {
        this.errors.mobileErr = this.translate.instant("mobileErr");
      }else if(this.registerForm.controls["mobile"].errors['pattern']) {
        this.errors.mobileErr = this.translate.instant("invalidMobile");
      }else{
        console.log("phone errors:",this.registerForm.controls["mobile"].errors);
      }
    }

  }else{
    var datax = {password:this.password,mobile:this.mobile}
   
    // if(this.type == "login"){
    //   console.log("login")
    //   // this.navCtrl.setRoot(TabsPage)
    // }else if (this.type == "register"){
    //   console.log("register")
    //   this.navCtrl.setRoot("ActivationCodePage",{from:"register",mobile:data.mobile})
    // }
    
   

    
    
    console.log("valid data : ",datax)
    this.srv.login(datax,resp=>{
      console.log("resp from login",resp)
      var respdata = JSON.parse(resp)
      if(respdata.code == 300){
        // this.helper.presentToast(this.translate.instant("connectionErr"))
        this.helper.presentToast(this.translate.instant("InvalidPhoneOrPassword"))
      }
      else if(respdata.code == 200){
        // 0109963883
//token
        if(respdata.userdata.roles == "2"){
          this.events.publish('user:userLoginSucceeded',{type:"user",name:respdata.userdata.name});
          this.helper.storeInfo({type:"user",alldata:respdata.userdata})
          // this.navCtrl.setRoot(HomePage)
          this.navCtrl.setRoot(UserHomePage)
        }if(respdata.userdata.roles == "3"){
          this.events.publish('user:userLoginSucceeded',{type:"driver",name:respdata.userdata.name});
          this.helper.storeInfo({type:"driver",alldata:respdata.userdata})
          this.navCtrl.setRoot(DriverHomePage)
        }
       

         

   
   
      }
     
    },err=>{
      console.log("err from login",err)
    })
//     this.srv.login({phone:this.mobile,pas:this.password},resp=>{
// console.log("resp fro login",resp)
//     },err=>{
//       console.log("err fro login",err)
//     })
  }
}

dismiss(){
  this.navCtrl.pop();
}

}
