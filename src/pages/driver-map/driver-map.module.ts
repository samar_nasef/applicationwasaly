import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverMapPage } from './driver-map';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    DriverMapPage,
  ],
  imports: [
    IonicPageModule.forChild(DriverMapPage),
    TranslateModule.forChild()
  ],
})
export class DriverMapPageModule {}
