import { Component , ViewChild , ElementRef} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DriverHomePage } from '../driver-home/driver-home';

import { Storage } from '@ionic/storage';

import { HelperProvider } from '../../providers/helper/helper';
// import { ServiceProvider } from '../../providers/service/service';
// import { TranslateService } from '@ngx-translate/core';

// import {
//   GoogleMaps,
//   GoogleMap,
//   GoogleMapsEvent,
//   GoogleMapOptions,
//   CameraPosition,
//   MarkerOptions,
//   Marker,
//   Environment,
//   LocationService,
//   MyLocation,
//   HtmlInfoWindow
// } from '@ionic-native/google-maps';
declare var google:any;

@IonicPage()
@Component({
  selector: 'page-driver-map',
  templateUrl: 'driver-map.html',
})
export class DriverMapPage {

  @ViewChild('map') mapElement:ElementRef;
  map: any;

  // map: GoogleMap;
  scaleClass="";
  langDirection = "";
  position

  latfromPusher
  lngfromPusher

  directionsService
  directionsDisplay

  userLat
  userLng

  constructor(public storage:Storage,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
    {
      this.scaleClass="scaleClassrtl";} 
      else{
        this.scaleClass="scaleClassltr";}
  
    
    this.latfromPusher = this.navParams.get("lat")
    this.lngfromPusher = this.navParams.get("lng")

    this.userLat = this.navParams.get("userLat")
    this.userLng = this.navParams.get("userLng")
     

    this.position = {lat :this.helper.lat,lng : this.helper.lng}
// this.initmap()

  }
  timer2
  startTimerTogetmylocfordrivermap(){
   this.timer2 =   setInterval(()=>{
      this.helper.geoLoc((data)=>{
        console.log("location data :" ,data)
//         inspectorLat: 31.0657428
// inspectorLocAccuracy: 10.914999961853027
// inspectorLong: 31.6421449
var resp = JSON.parse(JSON.stringify(data))
this.position = {lat :this.helper.lat,lng : this.helper.lng}
this.addanotherRoute()
      })
    },2000)
    
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad DriverMapPage');
   this.directionsService = new google.maps.DirectionsService;
   this.directionsDisplay = new google.maps.DirectionsRenderer;
    this.initMap2()
this.startTimerTogetmylocfordrivermap()
  }
  dismiss(){
    console.log("dismiss")
    // this.navCtrl.pop();
    this.navCtrl.setRoot(DriverHomePage)
  }

  // initmap(){

  //   Environment.setEnv({
  //     'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY',
  //     'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY'
  //   });
  
  //   let mapOptions: GoogleMapOptions = {
  //     camera: {
  //       //  target:AIR_PORTS
  //       target:
  //         {
  //          lat: this.position.lat,
  //          lng: this.position.lng
  //        }
  //        ,
  //        zoom: 18,
  //        tilt: 30
  //      },controls: {
  //       zoom: false,//true
  //       myLocationButton : true,
  //       myLocation:true
        
  //   }
  //   };
  
  //   if(this.map)
  //   this.map.remove()
  
  //   this.map = GoogleMaps.create('map_canvas', mapOptions);
  
  
  // }
  
  allMarkers=[]

initMap2(){

  console.log("init map");

  let latlng = new google.maps.LatLng(this.position.lat, this.position.lng);
  var mapOptions={
    center:latlng,
    // zoom:15,
    zoom:20,
    streetViewControl:false,
    fullscreenControl:false,
    mapTypeId:google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true,
    styles:this.helper.mapStyle
  };

  this.map=  new google.maps.Map(this.mapElement.nativeElement,mapOptions);

  var markers, i;
  
  for(var j=0;j<this.allMarkers.length;j++)
  {
    this.allMarkers[j].setMap(null);
  }


  // let marker = new google.maps.Marker({
  //   map: this.map,
  //   animation: google.maps.Animation.DROP,
  //   position: new google.maps.LatLng(this.userLat,this.userLng),
  //   icon: { 
  //     url :'assets/imgs/carMarker.png',
  //     size: new google.maps.Size(71, 71),
  //     scaledSize: new google.maps.Size(20, 25) 
  //   }

  //   // label:{
  //   //   text:this.drivername,
  //   //   color:"black",  
  //   // }
  // });
  
  // let marker2 = new google.maps.Marker({
  //   map: this.map,
  //   animation: google.maps.Animation.DROP,
  //   position: new google.maps.LatLng(this.position.lat,this.position.lng),
  //   icon: { 
  //     url :'assets/imgs/carmarker2.png',
  //     size: new google.maps.Size(71, 71),
  //     scaledSize: new google.maps.Size(20, 25) 
  //   }

  //   // label:{
  //   //   text:this.drivername,
  //   //   color:"black",  
  //   // }
  // });
  // this.allMarkers.push(marker)
  // this.allMarkers.push(marker2)
  
  // let marker2 = new google.maps.Marker({
  //   map: this.map,
  //   animation: google.maps.Animation.DROP,
  //   position: new google.maps.LatLng(31.034428482129403,31.39275934193165),
  //   icon: { 
  //     url : this.markimage,
  //     size: new google.maps.Size(71, 71),
  //     scaledSize: new google.maps.Size(20, 25) 
  //   },
  //   label:{
  //     text:"my loc",
  //     color:"black",      
  //   }
  // });

  // let directionsService = new google.maps.DirectionsService;
  // let directionsDisplay = new google.maps.DirectionsRenderer;

  this.directionsDisplay.setMap(this.map);
  // this.directionsDisplay.setOptions( { suppressMarkers: true } );
  
  // directionsDisplay.setPanel(this.directionsPanel.nativeElement);
 // 31.034428482129403,lng:31.39275934193165\
// console.log("lat:parseFloat(this.trapLat),lng:parseFloat(this.trapLng)",parseFloat(this.trapLat),parseFloat(this.trapLng))|

this.directionsService.route({
      origin:  {lat:this.position.lat,lng:this.position.lng},
      destination: {lat:parseFloat(this.userLat),lng:parseFloat(this.userLng)} ,
      travelMode: google.maps.TravelMode['DRIVING']
  }, (res, status) => {

      if(status == google.maps.DirectionsStatus.OK){
          this.directionsDisplay.setDirections(res);
      } else {
          console.warn(status);
      }

  });


}


addanotherRoute(){
  console.log("addanotherRoute")
  for(var j=0;j<this.allMarkers.length;j++)
  {
    this.allMarkers[j].setMap(null);
  }
  // let marker2 = new google.maps.Marker({
  //   map: this.map,
  //   animation: google.maps.Animation.DROP,
  //   position: new google.maps.LatLng(this.position.lat,this.position.lng),
  //   // icon: { 
  //   //   url : this.markimage,
  //   //   size: new google.maps.Size(71, 71),
  //   //   scaledSize: new google.maps.Size(20, 25) 
  //   // },
  //   // label:{
  //   //   text:this.drivername,
  //   //   color:"black",
  //   // }
  // });

  // this.allMarkers.push(marker2)

  // origin: {lat:31.0324953,lng:31.3964502},
  // destination: {lat:31.034428482129403,lng:31.39275934193165},

  // let marker3 = new google.maps.Marker({
  //   map: this.map,
  //   animation: google.maps.Animation.DROP,
  //   position: new google.maps.LatLng(this.driverposition.lat,this.driverposition.lng),
  //   // icon: { 
  //   //   url : this.markimage,
  //   //   size: new google.maps.Size(71, 71),
  //   //   scaledSize: new google.maps.Size(20, 25) 
  //   // },
  //   label:{
  //     text:this.drivername,
  //     color:"black",      
  //   }
  // });
  // this.allMarkers.push(marker3)

  // let directionsService = new google.maps.DirectionsService;
  // let directionsDisplay = new google.maps.DirectionsRenderer;
  
  // this.directionsDisplay.setMap(null);
  
  // let marker = new google.maps.Marker({
  //   map: this.map,
  //   animation: google.maps.Animation.DROP,
  //   position: new google.maps.LatLng(this.userLat,this.userLng),
  //   icon: { 
  //     url :'assets/imgs/carMarker.png',
  //     size: new google.maps.Size(71, 71),
  //     scaledSize: new google.maps.Size(20, 25) 
  //   }

  //   // label:{
  //   //   text:this.drivername,
  //   //   color:"black",  
  //   // }
  // });
  
  // let marker2 = new google.maps.Marker({
  //   map: this.map,
  //   animation: google.maps.Animation.DROP,
  //   position: new google.maps.LatLng(this.position.lat,this.position.lng),
  //   icon: { 
  //     url :'assets/imgs/carmarker2.png',
  //     size: new google.maps.Size(71, 71),
  //     scaledSize: new google.maps.Size(20, 25) 
  //   }

  //   // label:{
  //   //   text:this.drivername,
  //   //   color:"black",  
  //   // }
  // });

  // this.allMarkers.push(marker)
  // this.allMarkers.push(marker2)
  

  this.directionsDisplay.setMap(this.map);
  // this.directionsDisplay.setOptions({ suppressMarkers: true });

// directionsDisplay.setPanel(this.directionsPanel.nativeElement);
// 31.034428482129403,lng:31.39275934193165\
// console.log("lat:parseFloat(this.trapLat),lng:parseFloat(this.trapLng)",parseFloat(this.trapLat),parseFloat(this.trapLng))|
  this.directionsService.route({
      origin:  {lat:parseFloat(this.position.lat),lng:parseFloat(this.position.lng)},
      destination: {lat:parseFloat(this.userLat),lng:parseFloat(this.userLng)} ,
      travelMode: google.maps.TravelMode['DRIVING'],
      
  }, (res, status) => {

    console.log("route res 1: ",res,"res.request.destination.location.lat()",res.request.destination.location.lat(),"res.request.destination.location.lng()",res.request.destination.location.lng(),"res.request.origin.location.lat(): ",res.request.origin.location.lat(),"res.request.origin.location.lng()",res.request.origin.location.lng())
    console.log("route res 2: ",res,"this.position.lat :",this.position.lat,"this.position.lng: ",this.position.lng,"this.userLat: ",this.userLat,"this.userLng: ",this.userLng)
    console.log("route res 3: res.routes[0].legs[0].start_location.lat() ",res.routes[0].legs[0].start_location.lat())
    // end_location
      if(status == google.maps.DirectionsStatus.OK){
        this.directionsDisplay.setDirections(res);
      }else{
        console.warn(status);
      }

      // let marker2 = new google.maps.Marker({
      //   map: this.map,
      //   animation: google.maps.Animation.DROP,
      //   position: new google.maps.LatLng(res.routes[0].legs[0].start_location.lat(),res.routes[0].legs[0].start_location.lng()),
      //   icon: { 
      //     url :'assets/imgs/carmarker2.png',
      //     size: new google.maps.Size(85, 85),
      //     scaledSize: new google.maps.Size(40, 45) 
      //   }
    
      //   // label:{
      //   //   text:this.drivername,
      //   //   color:"black",  
      //   // }
      // });
      
      // this.allMarkers.push(marker2)
    

  });


  // let marker2 = new google.maps.Marker({
  //   map: this.map,
  //   animation: google.maps.Animation.DROP,
  //   position: new google.maps.LatLng(this.position.lat,this.position.lng),
  //   icon: { 
  //     url :'assets/imgs/carmarker2.png',
  //     size: new google.maps.Size(85, 85),
  //     scaledSize: new google.maps.Size(40, 45) 
  //   }

  //   // label:{
  //   //   text:this.drivername,
  //   //   color:"black",  
  //   // }
  // });
  
  // this.allMarkers.push(marker2)
  

}

myid
next(){
  this.storage.get('info').then(val=>{
    console.log("user ",val)
    this.myid = val.alldata.id
    this.helper.updatenext(this.myid)
  this.navCtrl.push('DriverMap2Page',{lat:this.latfromPusher,lng:this.lngfromPusher})
  });  
}


}
