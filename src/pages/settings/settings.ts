import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,App } from 'ionic-angular';

import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { Platform } from 'ionic-angular/platform/platform';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';
import { DriverHomePage } from '../driver-home/driver-home';
import { UserHomePage } from '../user-home/user-home';


@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  langDirection
  scaleClass="";
  language;
  Languagetra

  type
  constructor(public appCtrl: App,private storage: Storage, public platform:Platform,public helper:HelperProvider,public translate:TranslateService,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    console.log("langDirection from home :",this.langDirection)
      
    this.type = this.navParams.get("from")
    console.log("type from orders : ",this.type)

        if(this.langDirection == "rtl")
          this.scaleClass="scaleClassrtl";
        else
          this.scaleClass="scaleClassltr";

        if (this.helper.lang_direction == 'ltr') {        
          this.language = "العربية";  
          this.Languagetra = "Language"
        }else {    
          this.language = "English";
          this.Languagetra = "اللغة"
        }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SetingsPage');
  }
  dismiss(){
    this.navCtrl.pop();
  }
 
  changeLanguage() {
    
   
    if (this.helper.lang_direction == 'rtl') {

      this.storage.set('wasaly_language', 'en').then(resp=>{
        console.log("resp from wasaly_language en ,: ",resp)
        // window.location.reload();
        this.translate.setDefaultLang('en');
        this.translate.use('en');
        this.platform.setDir('ltr', true);
        this.helper.lang_direction = "ltr";
        this.langDirection = "ltr";
        this.translate.currentLang = 'en'
        if(this.type == "user")
        this.navCtrl.setRoot(UserHomePage)
          // this.navCtrl.setRoot(HomePage);
        else if(this.type == "driver")
          this.navCtrl.setRoot(DriverHomePage);
        // this.appCtrl.getRootNav().setRoot(HomePage);        
      });
      // this.translate.setDefaultLang('en');
      // this.translate.use('en');
      // this.platform.setDir('ltr', true);
      // this.helper.lang_direction = "ltr";
      // this.helper.storeLanguage('en')
      // this.langDirection = "ltr";
      // this.navCtrl.setRoot(HomePage);

    }
    else {
      // this.translate.use('ar');
      // this.helper.lang_direction = 'rtl';
      // this.translate.setDefaultLang('ar');
      // this.langDirection = "rtl";
      // this.platform.setDir('rtl',true);
      // this.helper.storeLanguage('ar')
      // this.navCtrl.setRoot(HomePage);
      this.storage.set('wasaly_language', 'ar').then(resp=>{
        console.log("resp from wasaly_language ar ,: ",resp)
        this.translate.use('ar');
        this.helper.lang_direction = 'rtl';
        this.translate.setDefaultLang('ar');
        this.langDirection = "rtl";
        this.platform.setDir('rtl',true);
        this.translate.currentLang = 'ar'
        //this.navCtrl.setRoot(HomePage);
        if(this.type == "user")
        this.navCtrl.setRoot(UserHomePage)
          // this.navCtrl.setRoot(HomePage);
        else if(this.type == "driver")
          this.navCtrl.setRoot(DriverHomePage); 
        // this.appCtrl.getRootNav().setRoot(HomePage);
      });

    }
  }
}
