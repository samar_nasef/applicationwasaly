import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HalfMapToMarkLocationPage } from './half-map-to-mark-location';

@NgModule({
  declarations: [
    HalfMapToMarkLocationPage,
  ],
  imports: [
    IonicPageModule.forChild(HalfMapToMarkLocationPage),
  ],
})
export class HalfMapToMarkLocationPageModule {}
