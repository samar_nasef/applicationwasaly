import { Component ,ViewChild ,ElementRef} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';

// import {
//   GoogleMaps,
//   GoogleMap,
//   GoogleMapsEvent,
//   GoogleMapOptions,
//   CameraPosition,
//   MarkerOptions,
//   Marker,
//   Environment,
//   LocationService,
//   MyLocation,
//   HtmlInfoWindow
// } from '@ionic-native/google-maps';

import { ServiceProvider } from '../../providers/service/service'

import { TranslateService } from '@ngx-translate/core';
declare var google;

@IonicPage()
@Component({
  selector: 'page-half-map-to-mark-location',
  templateUrl: 'half-map-to-mark-location.html',
})
export class HalfMapToMarkLocationPage {

  // map: GoogleMap;
  @ViewChild('map') mapElement:ElementRef;
  map: any;

  from

  marker

  buttonTxt

  choosenLat
  choosenLng

  specifyLoc

  allMarkers=[]

  scaleClass="";
  langDirection = "";


  constructor(public srv:ServiceProvider, public translate:TranslateService,public helper:HelperProvider, public navCtrl: NavController, public navParams: NavParams) {
   
    this.langDirection = this.helper.lang_direction;
    console.log("langDirection from home :",this.langDirection)
         
    
        if(this.langDirection == "rtl"){
          this.scaleClass="scaleClassrtl";
         
        }
        else{
          this.scaleClass="scaleClassltr";
          

        }

    this.from = this.navParams.get("from")
    console.log("this.from from half map  : ",this.from)

    this.choosenLat = this.navParams.get("lat")
    this.choosenLng = this.navParams.get("lng")
    
    console.log("choosen Loc : ",this.choosenLat, " , ",this.choosenLng)
    this.specifyLoc = this.translate.instant("specifyLoc")
    if(this.from == "from")
      this.buttonTxt = this.translate.instant("confirmLocation")
    else if(this.from == "to")
    this.buttonTxt = this.translate.instant("continue")

}

  ionViewDidLoad() {
    console.log('ionViewDidLoad HalfMapToMarkLocationPage');
    this.addMap()
  }
  addMap(){
  // Environment.setEnv({
  //         'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY',
  //         'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY'
  //       });
    
  //       let mapOptions: GoogleMapOptions = {
  //         camera: {
  //           //  target:AIR_PORTS
  //           target:
  //             {
  //              lat: this.choosenLat,
  //              lng: this.choosenLng
  //            }
  //            ,
  //            zoom: 18,
  //            tilt: 30
  //          },controls: {
  //           zoom: false,//true
  //           myLocationButton : true,
  //           myLocation:true
            
  //       }
  //       };
    
  //       if(this.map)
  //       this.map.remove()

  //       this.map = GoogleMaps.create('map_canvas', mapOptions);

  let latlng = new google.maps.LatLng(this.choosenLat, this.choosenLng);
  var mapOptions={
    center:latlng,
    zoom:15,
    // zoom:20,
    streetViewControl:false,
    fullscreenControl:false,
    mapTypeId:google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true,
    styles:this.helper.mapStyle
   

  };

  this.map=  new google.maps.Map(this.mapElement.nativeElement,mapOptions);

      

        this.addMapClick()
     
      
  }

  addMapClick(){

    
 

    this.map.addListener('click',(ev) => {
        console.log("lat clicked",ev.latLng.lat());
        console.log("lon clicked",ev.latLng.lng());
        var loc
        loc = {
          lat:ev.latLng.lat(),
          lng:ev.latLng.lng()
        }

        for(var j=0;j<this.allMarkers.length;j++)
    {
      this.allMarkers[j].setMap(null);
    }
         
        let marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: new google.maps.LatLng(loc.lat,loc.lng),
          // icon: { 
          //   url : this.markimage,
          //   size: new google.maps.Size(71, 71),
          //   scaledSize: new google.maps.Size(20, 25) 
          // },
          // label:{
          //   text:"From",
          //   color:"black",      
          // }
        });

        this.allMarkers.push(marker)
        if(this.from == "from")
        {
          console.log(" form marker ")
          this.helper.fromLocation = loc.lat+","+loc.lng;
          console.log("this.helper.fromLocation",this.helper.fromLocation)
        }
        else if(this.from == "to"){
          console.log(" to marker ")
          this.helper.toLocation = loc.lat+","+loc.lng;
          console.log("this.helper.toLocation",this.helper.toLocation)
        }

       
        
    });
//     this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe((location: any[]) => {
//       console.log("click location from map on :",location);
     
//      var loc
//      loc = {
//       lat: location[0].lat,
//       lng: location[0].lng
//   }; 

//   if(this.from == "from")
//   {
//     console.log(" form marker ")
//     this.helper.fromLocation = loc.lat+","+loc.lng;
//     console.log("this.helper.fromLocation",this.helper.fromLocation)
//   }
//   else if(this.from == "to"){
//     console.log(" to marker ")
//     this.helper.toLocation = loc.lat+","+loc.lng;
//     console.log("this.helper.toLocation",this.helper.toLocation)
//   }

//   if(this.marker)
// this.marker.remove()

//   this.marker = this.map.addMarkerSync({
//     // title: 'Ionic\n <br>hello',
//     // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
//     icon: 'orange',
//     animation: 'DROP',
//     position: {
//       lat:  location[0].lat,
//       lng:  location[0].lng
//     }
//   });


// });

  }

  nextBtnClicked(){
    if(this.from == "from"){
      this.navCtrl.pop()
    }else if(this.from == "to"){
      this.srv.getaddress(this.helper.toLocation.split(",")[0],this.helper.toLocation.split(",")[1]).subscribe(
        resp=>{
          console.log("resp from get address from userHome",resp , "address from google :", JSON.parse(JSON.stringify(resp)).results[0].formatted_address);
 
          // this.autocomplete.input =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;
 
          this.helper.toAddressDesc =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;
     
          this.navCtrl.push("CompleteOrderPage",{lat:this.helper.toLocation.split(",")[0],lng:this.helper.toLocation.split(",")[1],address:this.helper.toAddressDesc,userLat:this.helper.fromLocation.split(",")[0],userLng:this.helper.fromLocation.split(",")[1]})
        },err=>{
      console.log("err from get address1",err);
      // this.driverlocation = "";
      })

     
    }
  }


  dismiss(){
    this.navCtrl.pop()
  }

}
