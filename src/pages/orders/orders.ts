import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServiceProvider } from '../../providers/service/service';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})
export class OrdersPage {

  scaleClass="";
  langDirection = "";
  // statusColor="rgb(155, 181, 155)"
  
  

  // ordersArr=[{id:1,time:"5:55 am",date:"12-10-2018",status:"1",statusTxt:"Not Completed",x:"#9E9E9E",driverimg:"assets/imgs/default-avatar.png",ordername: "aaaaaaaaaa",oderdetails:"ddddddddddddddd",orderloc:"hhhhhhhhhhhhhhhhhhhh"},
  // {id:2,time:"5:55 am",date:"12-10-2018",status:"1",statusTxt:"Completed",x:"rgb(94, 130, 94)",driverimg:"assets/imgs/default-avatar.png",ordername: "aaaaaaaaaa",oderdetails:"ddddddddddddddd",orderloc:"hhhhhhhhhhhhhhhhhhhh"},
  // {id:3,time:"5:55 am",date:"12-10-2018",status:"1",statusTxt:"مكتملة",x:"rgb(94, 130, 94)",driverimg:"assets/imgs/default-avatar.png",ordername: "aaaaaaaaaa",oderdetails:"ddddddddddddddd",orderloc:"hhhhhhhhhhhhhhhhhhhh"},
  // {id:4,time:"5:55 am",date:"12-10-2018",status:"1",statusTxt:"غير مكتملة",x:"#9E9E9E",driverimg:"assets/imgs/default-avatar.png",ordername: "aaaaaaaaaa",oderdetails:"ddddddddddddddd",orderloc:"hhhhhhhhhhhhhhhhhhhh"}];

  ordersArr=[]
  daytra
  ordernametra
  orderdetailstra
  orderloctra

  type

  //order status 
  //0 create
  //1 driver accept
  //2 done
  //3 user cancel
  //4 driver cancel

  
  constructor(public translate: TranslateService,public datepipe: DatePipe,public storage: Storage,public srv:ServiceProvider, public helper:HelperProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
console.log("langDirection from home :",this.langDirection)
     this.type = this.navParams.get("from")
     console.log("type from orders : ",this.type)

    if(this.langDirection == "rtl")
    {
      this.scaleClass="scaleClassrtl";
      this.daytra = "يوم"
      this.ordernametra = "رقم الطلب"
      this.orderdetailstra = "تفاصيل الرحلة"
      this.orderloctra = "الموقع"
      
    }  
    else{
      this.scaleClass="scaleClassltr";
      this.daytra = "Day"
      this.ordernametra = "Order no."
      this.orderdetailstra = "Trip Details"
      this.orderloctra = "Location"
    }
      

  }
  myid
  noorders
  ionViewDidLoad() {
    console.log('ionViewDidLoad OrdersPage');
    this.storage.get('info').then(val=>{
      console.log("user ",val)
      this.myid = val.alldata.id
    })
    this.noorders = true
  }


ionViewDidEnter(){
  console.log("ionViewDidEnter OrdersPage")
    
if(this.type == "user"){
  console.log("get user orders")
  this.storage.get('info').then(val=>{
    console.log("user ",val)
    this.myid = val.alldata.id
  this.srv.getUserOrders(this.myid,resp=>{
    console.log("resp ffrom getUserOrders",resp)
    this.ordersArr= JSON.parse(resp).Orders
    if(this.ordersArr.length == 0){
this.noorders = false
    }else{
      this.noorders = true
    for(var j=0;j<this.ordersArr.length;j++){
      this.ordersArr[j].date = this.ordersArr[j].created_at.split(" ")[0]
      this.ordersArr[j].time= this.datepipe.transform(this.ordersArr[j].created_at, 'h:m a')
      //order status 
  //0 create
  //1 driver accept
  //2 done
  //3 user cancel
  //4 driver cancel
  if(this.ordersArr[j].status == "1"){
    this.ordersArr[j].x="#9E9E9E"
    this.ordersArr[j].statusTxt = this.translate.instant("orderaccept")
  }
  else if(this.ordersArr[j].status == "2"){
    this.ordersArr[j].x="rgb(94, 130, 94)"
    this.ordersArr[j].statusTxt = this.translate.instant("orderdone")
  }
  else if(this.ordersArr[j].status == "3"){
    this.ordersArr[j].x="#e01213"
    this.ordersArr[j].statusTxt = this.translate.instant("ordercancelled")
  }
  

    }
  }

  },err=>{
    console.log("err ffrom getUserOrders",err)
  })
})
}else if(this.type == "driver"){
  console.log("get driver orders")
  this.storage.get('info').then(val=>{
    console.log("user ",val)
    this.myid = val.alldata.id
  this.srv.getDriverOrders(this.myid,resp=>{
    console.log("resp ffrom getUserOrders",resp)

    this.ordersArr= JSON.parse(resp).Orders
    if(this.ordersArr.length == 0){
this.noorders = false
    }else{
      this.noorders = true
    for(var j=0;j<this.ordersArr.length;j++){
      this.ordersArr[j].date = this.ordersArr[j].created_at.split(" ")[0]
      this.ordersArr[j].time= this.datepipe.transform(this.ordersArr[j].created_at, 'h:m a')
      //order status 
  //0 create
  //1 driver accept
  //2 done
  //3 user cancel
  //4 driver cancel
  if(this.ordersArr[j].status == "1"){
    this.ordersArr[j].x="#9E9E9E"
    this.ordersArr[j].statusTxt = this.translate.instant("orderaccept")
  }
  else if(this.ordersArr[j].status == "2"){
    this.ordersArr[j].x="rgb(94, 130, 94)"
    this.ordersArr[j].statusTxt = this.translate.instant("orderdone")
  }
  else if(this.ordersArr[j].status == "3"){
    this.ordersArr[j].x="#e01213"
    this.ordersArr[j].statusTxt = this.translate.instant("ordercancelled")
  }
  

    }
  }
  
  },err=>{
    console.log("err ffrom getUserOrders",err)
  })
})
}

}

}
