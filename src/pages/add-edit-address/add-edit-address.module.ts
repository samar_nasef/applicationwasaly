import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddEditAddressPage } from './add-edit-address';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';


@NgModule({
  declarations: [
    AddEditAddressPage,
  ],
  imports: [
    IonicPageModule.forChild(AddEditAddressPage),
    TranslateModule.forChild()
  ],
})
export class AddEditAddressPageModule {}
