import { Component,NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment,
  LocationService,
  MyLocation,
  HtmlInfoWindow
} from '@ionic-native/google-maps';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { Storage } from '@ionic/storage';
declare var google;


@IonicPage()
@Component({
  selector: 'page-add-edit-address',
  templateUrl: 'add-edit-address.html',
})
export class AddEditAddressPage {

  scaleClass="";
  langDirection = "";
  
  GoogleAutocomplete
  //autocomplete
  autocompleteItems=[]
  geocoder
  desc
  position
 
  searchtxt=""
  
  map: GoogleMap;
  
  autocomplete = { input: '' };

  hidemap
   marker: Marker 
   placename

   from
   btnTxt
   addresstxt
   myid

  constructor(public storage:Storage,public srv:ServiceProvider, public translate: TranslateService,private zone:NgZone,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
   
  //   this.helper.geoLoc((data)=>{console.log("data from geo loc user home :",data)
  //   this.position = {lat:this.helper.lat,lng:this.helper.lng}
  //   this.initmap()
  // })

    if(this.langDirection == "rtl"){
      this.scaleClass="scaleClassrtl";
      // this.alertdir = "rtldir"
    }
    else{
      this.scaleClass="scaleClassltr";
      // this.alertdir = "ltrdir"
    }
    this.from = this.navParams.get("from")
    if(this.from == "add"){
  
      this.helper.geoLoc((data)=>{console.log("data from geo loc user home :",data)
    this.position = {lat:this.helper.lat,lng:this.helper.lng}
    this.initmap()
  })

      this.btnTxt = this.translate.instant("save")
      this.addresstxt = this.translate.instant("addAddress")
      this.hidemap = false
      // this.position = {lat:this.helper.lat,lng:data.this.helper.lng}  
      // public lat = 30.044281;
      // public lng = 31.340002;  

      // this.position = {lat:30.044281,lng:31.340002}
      // this.initmap()
      
    }
    else  if(this.from == "edit"){
      this.btnTxt = this.translate.instant("edit2")
      this.addresstxt = this.translate.instant("editAddress")
      console.log("this.navParams.get(data): ",this.navParams.get("data"))
      var data = this.navParams.get("data")
this.placename =data.name
this.position = {lat:data.lat,lng:data.lng}
console.log("edit placename: ",this.placename," , position : ",this.position)
this.hidemap = false
this.initmap()  
}
    
console.log("langDirection from complete order :",this.langDirection)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddEditAddressPage');
    // this.helper.geoLoc((data)=>{console.log("data from geo loc user home :",data)
    // // this.loadMap()
    // })
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.geocoder = new google.maps.Geocoder;
    this.autocomplete = { input: '' };
    this.autocompleteItems = [];
  }

  updateSearchResults(){
    console.log("updateSearchResults");
    
    if (this.autocomplete.input == '') {
      this.autocompleteItems = [];
      // this.driversArr = []
      return;
    }
    console.log("this.autocomplete.input: ",this.autocomplete.input)
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input },
    (predictions, status) => {
      this.autocompleteItems = [];
      this.zone.run(() => {
        predictions.forEach((prediction) => {
          this.autocompleteItems.push(prediction);
        });
      });
    });
  }
  selectSearchResult(item){
    console.log("selectSearchResult , ",item)
    // alert("select search res")
    this.hidemap = false
//    this.loadMap()
    this.desc = item.description
    this.autocompleteItems= []
    
    // 'placeId': item.place_id
    //'address':item.description
    this.geocoder.geocode({'placeId': item.place_id}, (results, status) => {
      console.log("geocoder.geocode results : ",results)
      console.log("geocoder.geocode status : ",status)
      console.log("results[0].geometry.location.lat : ",results[0].geometry.location.lat())
      console.log("results[0].geometry.location.lng : ",results[0].geometry.location.lng())
      if(status === 'OK' && results[0]){
        this.position = {
            lat: results[0].geometry.location.lat(),
            lng: results[0].geometry.location.lng()
        };
        console.log("item.position : ",this.position);
        
        
        
        Environment.setEnv({
          'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY',
          'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY'
        });
    
        let mapOptions: GoogleMapOptions = {
          camera: {
            //  target:AIR_PORTS
            target:
              {
               lat: this.position.lat,
               lng: this.position.lng
             }
             ,
             zoom: 18,
             tilt: 30
           },controls: {
            zoom: false,//true
            myLocationButton : true,
            myLocation:true
            
        }
        };
    
        if(this.map)
        this.map.remove()

        this.map = GoogleMaps.create('map_canvas', mapOptions);

        this.marker= this.map.addMarkerSync({
          // title: 'Ionic\n <br>hello',
          // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
          icon: 'orange',
          animation:'DROP',// 'BOUNCE',//'DROP',
          position: {
            lat:  this.position.lat,
            lng:  this.position.lng
          }
        });

        this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe((location: any[]) => {
          console.log("map location from map on :",location);
         // alert("map location : "+JSON.stringify(location));
         this.position = {
          lat: location[0].lat,
          lng: location[0].lng
      };
      this.marker.remove()

          this.marker = this.map.addMarkerSync({
            // title: 'Ionic\n <br>hello',
            // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
            icon: 'orange',
            animation: 'DROP',
            position: {
              lat:  location[0].lat,
              lng:  location[0].lng
            }
          });
        });

        // this.map.on(GoogleMapsEvent.MY_LOCATION_CLICK).subscribe((location: any[]) => {
        //   console.log("location from map on :",location);
        //   alert("location : "+JSON.stringify(location));
        // });

      }
    })
  }

  dismiss(){
    this.navCtrl.pop()
  }
  save(){

//     lat
// :
// 31.0470131
// lng
// :
// 31.37165749999997
   if(!this.placename){
    this.helper.presentToast(this.translate.instant("enterplacename"))
    return
   }else if(!this.position)
   {
     this.helper.presentToast(this.translate.instant("specifylocationonmap"))
    return
   } else{
    // this.desc
    // this.position
    // this.placename
   
     if(this.from == "add"){
      console.log("save place for add")
      this.storage.get('info').then(val=>{
            console.log("user ",val)
            this.myid = val.alldata.id
            let params={
              'name' : this.placename,
              'lat':this.position.lat,
              'lng':this.position.lng,
              'address':this.desc,
              'user_id':this.myid
            }
                this.srv.addAddress(params,resp=>{
                  console.log("resp from addAddress : ",resp)
                  this.helper.presentToast(this.translate.instant("createAdd"))
                  this.navCtrl.setRoot('AddressHistoryPage')
                },err=>{
                  console.log("err from addAddress : ",err)

                })

      
      });
  

     }else if(this.from == "edit"){
      console.log("save place for edit")
     }

   }
    
  }

  initmap(){

    Environment.setEnv({
      'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY',
      'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY'
    });

    let mapOptions: GoogleMapOptions = {
      camera: {
        //  target:AIR_PORTS
        target:
          {
           lat: this.position.lat,
           lng: this.position.lng
         }
         ,
         zoom: 18,
         tilt: 30
       },controls: {
        zoom: false,//true
        myLocationButton : true,
        myLocation:true
        
    }
    };

    if(this.map)
    this.map.remove()

    this.map = GoogleMaps.create('map_canvas', mapOptions);

    this.marker= this.map.addMarkerSync({
      // title: 'Ionic\n <br>hello',
      // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
      icon: 'orange',
      animation:'DROP',// 'BOUNCE',//'DROP',
      position: {
        lat:  this.position.lat,
        lng:  this.position.lng
      }
    });

    this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe((location: any[]) => {
      console.log("map location from map on :",location);
     // alert("map location : "+JSON.stringify(location));
     this.position = {
      lat: location[0].lat,
      lng: location[0].lng
  };
  this.marker.remove()

      this.marker = this.map.addMarkerSync({
        // title: 'Ionic\n <br>hello',
        // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
        icon: 'orange',
        animation: 'DROP',
        position: {
          lat:  location[0].lat,
          lng:  location[0].lng
        }
      });
    });
  }
}
