import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController} from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServiceProvider } from '../../providers/service/service';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';



@IonicPage()
@Component({
  selector: 'page-complete-order',
  templateUrl: 'complete-order.html',
})
export class CompleteOrderPage {
locationLat
locationLng
address

scaleClass="";
langDirection = "";

private registerForm : FormGroup;

tripname
cartype
orderdetails
price
hidepricerr

myid

submitAttempt = false;
errors = {
  tripnameErr:"",
  cartypeErr:"",
  orderdetailsErr:"",
  priceErr:""
};

placeholder = {cartype:"",orderdetails:"",price:"",passworrd:"",confirmpassword:"",tripname:"",email:""};
alertdir
userLat
userLng

  constructor(public alertController: AlertController,public storage:Storage,public translate: TranslateService,private formBuilder: FormBuilder,public srv:ServiceProvider,public helper:HelperProvider, public navCtrl: NavController, public navParams: NavParams) {
    
    this.locationLat = this.navParams.get("lat")
    this.locationLng = this.navParams.get("lng")
    this.address = this.navParams.get("address")

    this.userLat = this.navParams.get("userLat")
    this.userLng = this.navParams.get("userLng")
    console.log("all data from complete oreder : ",this.locationLat,",",this.locationLng,",",this.address,",",this.userLat,",",this.userLng)
    this.langDirection = this.helper.lang_direction;
    console.log("langDirection from complete order :",this.langDirection)
     
    if(this.langDirection == "rtl"){
      this.scaleClass="scaleClassrtl";
      this.alertdir = "rtldir"
    }
    else{
      this.scaleClass="scaleClassltr";
      this.alertdir = "ltrdir"
    }
      

      this.registerForm = this.formBuilder.group({
        orderdetails:['', Validators.required],
        cartype: ['',Validators.required],
        tripname :['',Validators.required],
        price:['',Validators.required]
             });
  this.placeholder.tripname = this.translate.instant("ordername")
  this.errors.tripnameErr = this.translate.instant("ordernameerr")

  this.errors.cartypeErr = this.translate.instant("carsizeerr2")

  this.placeholder.orderdetails = this.translate.instant("orderdetails2")
  this.errors.orderdetailsErr = this.translate.instant("orderDetailserr")

  this.hidepricerr = ""
  this.placeholder.price = this.translate.instant("price")
  this.errors.priceErr = this.translate.instant("priceerr")
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompleteOrderPage');
  }
  ordernow(){
    if(! this.registerForm.valid ){
      this.submitAttempt=true;
      // this.content.scrollToTop(3000);
  
      
      // if(this.registerForm.controls["password"].errors){
      //   if(this.registerForm.controls["password"].errors['required'])
      //     this.errors.passworrdErr = this.translate.instant("passworrdErr");
      //   else if (this.registerForm.controls["password"].errors['minlength'])
      //     this.errors.passworrdErr = this.translate.instant("passinvaild");
      //   else
      //     console.log("passErrors:",this.registerForm.controls["password"].errors);
      
      // }
  
      if(!this.registerForm.controls["price"].errors){
        if(!Number( this.price)){
          this.hidepricerr = this.translate.instant("invalidprice")
          return
        }else{
          this.hidepricerr = ""
        }
      }
     

    }else{
     
if(!Number( this.price)){
  this.hidepricerr = this.translate.instant("invalidprice")
  return
}else{
  this.hidepricerr = ""
}

       this.storage.get('info').then(val=>{
    console.log("user ",val)
    this.myid = val.alldata.id
  var data = {
  "user_id":this.myid,
  "description":this.orderdetails,
  "address":this.address,
  "lat":this.locationLat,
  "lng":this.locationLng,
  "userlat":this.userLat,
  "userlng":this.userLng,
  "type":this.cartype,
  "price":this.price,
}
// "userlat":this.helper.lat,
// "userlng":this.helper.lng
  console.log("order data : ",data)
  this.srv.createnewOrder(data,resp=>{
    console.log("resp from create new order : ",resp)
    if(JSON.parse(resp).code == 200){
      // this.helper.presentToast(ordersent)
      // this.presentThanks("",this.translate.instant("ordersent"))
      this.navCtrl.setRoot('WatingViewPage')
    }
     
    else 
      this.helper.presentToast(this.translate.instant("connectionErr"))
  },err=>{
    console.log("err from create new order : ",err)
  })
});

    }
  }
  presentThanks(txt,msg) {
    console.log("txt",txt)
        
        const alert = this.alertController.create({
          // title: txt,
          message:msg,
          cssClass: this.alertdir,
          buttons: [
          {
            text: this.translate.instant("agree"),
             role: 'cancel',
            // cssClass: this.alertdir,
            handler: () => {
              console.log('Confirm Cancel');
             
            this.navCtrl.pop()
            }
          }
        ]
        });
    
        alert.present();
      }

      dismiss(){
        this.navCtrl.pop()
      }
}
