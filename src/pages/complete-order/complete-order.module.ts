import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompleteOrderPage } from './complete-order';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CompleteOrderPage,
  ],
  imports: [
    IonicPageModule.forChild(CompleteOrderPage),
    TranslateModule.forChild()
    
  ],
})
export class CompleteOrderPageModule {}
