// import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Component , ViewChild , ElementRef} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DriverHomePage } from '../driver-home/driver-home';

import { HelperProvider } from '../../providers/helper/helper';

declare var google:any;


@IonicPage()
@Component({
  selector: 'page-driver-map2',
  templateUrl: 'driver-map2.html',
})
export class DriverMap2Page {

  @ViewChild('map') mapElement:ElementRef;
  map: any;

  // map: GoogleMap;
  scaleClass="";
  langDirection = "";
  position

  latfromPusher
  lngfromPusher

  directionsService
  directionsDisplay

  constructor(public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
    {
      this.scaleClass="scaleClassrtl";} 
      else{
        this.scaleClass="scaleClassltr";}
  
    
    this.latfromPusher = this.navParams.get("lat")
    this.lngfromPusher = this.navParams.get("lng")
     

    this.position = {lat :this.helper.lat,lng : this.helper.lng}
  }

  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad DriverMap2Page');
  // }
  timer2
  startTimerTogetmylocfordrivermap(){
   this.timer2 =   setInterval(()=>{
      this.helper.geoLoc((data)=>{
        console.log("location data :" ,data)
//         inspectorLat: 31.0657428
// inspectorLocAccuracy: 10.914999961853027
// inspectorLong: 31.6421449
var resp = JSON.parse(JSON.stringify(data))
this.position = {lat :this.helper.lat,lng : this.helper.lng}
this.addanotherRoute()
      })
    },2000)
    
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad DriverMapPage');
   this.directionsService = new google.maps.DirectionsService;
   this.directionsDisplay = new google.maps.DirectionsRenderer;
    this.initMap2()
this.startTimerTogetmylocfordrivermap()
  }
  dismiss(){
    console.log("dismiss")
    // this.navCtrl.pop();
    this.navCtrl.setRoot(DriverHomePage)
  }

  // initmap(){

  //   Environment.setEnv({
  //     'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY',
  //     'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY'
  //   });
  
  //   let mapOptions: GoogleMapOptions = {
  //     camera: {
  //       //  target:AIR_PORTS
  //       target:
  //         {
  //          lat: this.position.lat,
  //          lng: this.position.lng
  //        }
  //        ,
  //        zoom: 18,
  //        tilt: 30
  //      },controls: {
  //       zoom: false,//true
  //       myLocationButton : true,
  //       myLocation:true
        
  //   }
  //   };
  
  //   if(this.map)
  //   this.map.remove()
  
  //   this.map = GoogleMaps.create('map_canvas', mapOptions);
  
  
  // }
  
  allMarkers=[]

initMap2(){

  console.log("init map");

  let latlng = new google.maps.LatLng(this.position.lat, this.position.lng);
  var mapOptions={
    center:latlng,
    zoom:15,
    streetViewControl:false,
    fullscreenControl:false,
    mapTypeId:google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true,
    styles:this.helper.mapStyle
  };

  this.map=  new google.maps.Map(this.mapElement.nativeElement,mapOptions);

  var markers, i;
  
  for(var j=0;j<this.allMarkers.length;j++)
  {
    this.allMarkers[j].setMap(null);
  }


  let marker = new google.maps.Marker({
    map: this.map,
    animation: google.maps.Animation.DROP,
    position: new google.maps.LatLng(this.latfromPusher,this.lngfromPusher),
    

    // label:{
    //   text:this.drivername,
    //   color:"black",  
    // }
  });
  
  this.allMarkers.push(marker)
  
  // let marker2 = new google.maps.Marker({
  //   map: this.map,
  //   animation: google.maps.Animation.DROP,
  //   position: new google.maps.LatLng(31.034428482129403,31.39275934193165),
  //   icon: { 
  //     url : this.markimage,
  //     size: new google.maps.Size(71, 71),
  //     scaledSize: new google.maps.Size(20, 25) 
  //   },
  //   label:{
  //     text:"my loc",
  //     color:"black",      
  //   }
  // });

  // let directionsService = new google.maps.DirectionsService;
  // let directionsDisplay = new google.maps.DirectionsRenderer;

  this.directionsDisplay.setMap(this.map);

  // directionsDisplay.setPanel(this.directionsPanel.nativeElement);
 // 31.034428482129403,lng:31.39275934193165\
// console.log("lat:parseFloat(this.trapLat),lng:parseFloat(this.trapLng)",parseFloat(this.trapLat),parseFloat(this.trapLng))|

this.directionsService.route({
      origin:  {lat:this.position.lat,lng:this.position.lng},
      destination: {lat:parseFloat(this.latfromPusher),lng:parseFloat(this.lngfromPusher)} ,
      travelMode: google.maps.TravelMode['DRIVING']
  }, (res, status) => {

      if(status == google.maps.DirectionsStatus.OK){
          this.directionsDisplay.setDirections(res);
      } else {
          console.warn(status);
      }

  });

}


addanotherRoute(){
  console.log("addanotherRoute")
  for(var j=0;j<this.allMarkers.length;j++)
  {
    this.allMarkers[j].setMap(null);
  }
  // let marker2 = new google.maps.Marker({
  //   map: this.map,
  //   animation: google.maps.Animation.DROP,
  //   position: new google.maps.LatLng(this.position.lat,this.position.lng),
  //   // icon: { 
  //   //   url : this.markimage,
  //   //   size: new google.maps.Size(71, 71),
  //   //   scaledSize: new google.maps.Size(20, 25) 
  //   // },
  //   // label:{
  //   //   text:this.drivername,
  //   //   color:"black",
  //   // }
  // });

  // this.allMarkers.push(marker2)

  // origin: {lat:31.0324953,lng:31.3964502},
  // destination: {lat:31.034428482129403,lng:31.39275934193165},

  // let marker3 = new google.maps.Marker({
  //   map: this.map,
  //   animation: google.maps.Animation.DROP,
  //   position: new google.maps.LatLng(this.driverposition.lat,this.driverposition.lng),
  //   // icon: { 
  //   //   url : this.markimage,
  //   //   size: new google.maps.Size(71, 71),
  //   //   scaledSize: new google.maps.Size(20, 25) 
  //   // },
  //   label:{
  //     text:this.drivername,
  //     color:"black",      
  //   }
  // });
  // this.allMarkers.push(marker3)

  // let directionsService = new google.maps.DirectionsService;
  // let directionsDisplay = new google.maps.DirectionsRenderer;
  
  // this.directionsDisplay.setMap(null);
  this.directionsDisplay.setMap(this.map);

// directionsDisplay.setPanel(this.directionsPanel.nativeElement);
// 31.034428482129403,lng:31.39275934193165\
// console.log("lat:parseFloat(this.trapLat),lng:parseFloat(this.trapLng)",parseFloat(this.trapLat),parseFloat(this.trapLng))|
  this.directionsService.route({
      origin:  {lat:parseFloat(this.position.lat),lng:parseFloat(this.position.lng)},
      destination: {lat:parseFloat(this.latfromPusher),lng:parseFloat(this.lngfromPusher)} ,
      travelMode: google.maps.TravelMode['DRIVING']
  }, (res, status) => {

      if(status == google.maps.DirectionsStatus.OK){
        this.directionsDisplay.setDirections(res);
      }else{
        console.warn(status);
      }

  });
}

}
