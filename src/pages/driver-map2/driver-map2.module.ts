import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverMap2Page } from './driver-map2';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    DriverMap2Page,
  ],
  imports: [
    IonicPageModule.forChild(DriverMap2Page),
    TranslateModule.forChild()
  ],
})
export class DriverMap2PageModule {}
