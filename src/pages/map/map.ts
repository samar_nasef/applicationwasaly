import { Component,ViewChild ,ElementRef} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServiceProvider } from '../../providers/service/service';
import { TranslateService } from '@ngx-translate/core';

// import {
//   GoogleMaps,
//   GoogleMap,
//   GoogleMapsEvent,
//   GoogleMapOptions,
//   CameraPosition,
//   MarkerOptions,
//   Marker,
//   Environment,
//   LocationService,
//   MyLocation,
//   HtmlInfoWindow
// } from '@ionic-native/google-maps';

import { UserHomePage } from '../user-home/user-home';
declare var google:any;

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  @ViewChild('map') mapElement:ElementRef;
  map: any;

  // map: GoogleMap;
  
  origin
desc
  orderId
  scaleClass="";
  langDirection = "";
  position
  driverId 

  drivername
  driverlocation
  driverImg 

  driverObject
  marker
  driverdistance
  
  driverposition

  directionsService
  directionsDisplay

  constructor( public translate:TranslateService,public srv:ServiceProvider, public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
console.log("langDirection from map :",this.langDirection)
// this.helper.geoLoc((data)=>{console.log("data from geo loc user home :",data)})
// this.position = {lat :this.helper.lat,lng : this.helper.lng}

this.directionsService = new google.maps.DirectionsService;
this.directionsDisplay = new google.maps.DirectionsRenderer;

this.driverId =  this.navParams.get("driver_id")
this.orderId = this.navParams.get("order_id")
console.log("orderid from map : ",this.orderId)
this.helper.geoLoc((data)=>{
  console.log("data from geo loc user home :",data)
  this.position = {lat :this.helper.lat,lng : this.helper.lng}
  this.initMap2()
})

// this.initmap()
// this.helper.onnextChanged(this.driverId,resp=>{
//   console.log("on next change su : ",resp)
//   if(resp == 0)
//   {
//     this.helper.getUserLoc(this.driverId,userresp=>{
//       console.log("user resp : ",userresp)
//       this.
//       this.addanotherRoute()

//     })
    

//   }else if(resp == 1){
//     this.addanotherRoute()
//   }

// })

this.helper.onlocationChanged(this.driverId,location=>{
  console.log("location from map view : ",location)
  this.driverposition = {lat :location.split(",")[0],lng : location.split(",")[1]}
  this.srv.getaddress(this.driverposition.lat,this.driverposition.lng).subscribe(
    resp=>{
      console.log("resp from get address1",resp);
       this.driverlocation =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;
  },err=>{
  console.log("err from get address1",err);
  // this.driverlocation = "";
  })
  this.srv.getDurationAndDistance(this.helper.lat,this.helper.lng,this.driverposition.lat,this.driverposition.lng).subscribe(
  resp=>{
    console.log("resp from getDurationAndDistance from init map with doc loc-> doctor map: ", resp);
    var respObj = JSON.parse(JSON.stringify(resp));
    
   this.driverdistance =  respObj.routes[0].legs[0].distance.text    

    console.log("duration",respObj.routes[0].legs[0].duration.text);
    var dur = respObj.routes[0].legs[0].duration.text;
    var durVal = respObj.routes[0].legs[0].duration.value;
    console.log("dur val after set",durVal);
    console.log("routes resp : ",respObj.routes[0].legs[0]);
  });
  // this.addNewMarker()
  
  // this.addanotherRoute()

  this.helper.onnextChanged(this.driverId,resp=>{
      console.log("on next change su : ",resp)
      if(resp == 0)
      {
        console.log("next  0")
      
      
  this.origin = this.driverposition

  this.helper.getUserLoc(this.driverId,userLoc=>{
    console.log("get user loc : ",userLoc)
    this.desc= {lat :userLoc.split(",")[0],lng : userLoc.split(",")[1]}
    this.addanotherRoute()
  })
  

      }else if(resp == 1){

        console.log("next  1")

this.origin = this.driverposition
this.helper.getThingLoc(this.driverId,userLoc=>{
  console.log("get thing loc : ",userLoc)
  this.desc= {lat :userLoc.split(",")[0],lng : userLoc.split(",")[1]}
  this.addanotherRoute()
})

        // this.addanotherRoute()
  
      }

  });

//   if(!this.marker){
//   this.initmap()
//   console.log("if !marker init map")
// }else{
//   this.addNewMarker()
//   console.log("else !marker add marker")
// }
  
})


this.srv.getDriverDetails(this.driverId,resp=>{
  console.log("resp from getDriverDetails : ",resp)
  var respdata = JSON.parse(resp)
  // if(respdata.code == 200){
    console.log("status 200")
    this.driverObject = respdata.userdata
this.drivername = respdata.userdata.name
console.log("driver name ",this.drivername, "obj :",this.driverObject)
//change driver location , distance according to fire base 
this.driverlocation = respdata.userdata.address
this.driverImg = respdata.userdata.img

this.position = {lat :respdata.userdata.lat,lng : respdata.userdata.lng}


 

  // }  
},err=>{
console.log("err from getDriverDetails : ",err)
}
)
    if(this.langDirection == "rtl")
    {
      this.scaleClass="scaleClassrtl";} else{
        this.scaleClass="scaleClassltr";}
  
      }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
    this.position = {lat :this.helper.lat,lng : this.helper.lng}
    this.initMap2()    
  }
  ionViewDidEnter(){
    console.log("ionViewDidEnter")
    // this.loadMap();
  }

//   loadMap() {

//     var HND_AIR_PORT = {lat: 35.548852, lng: 139.784086};
// var SFO_AIR_PORT = {lat: 37.615223, lng: -122.389979};
// var HNL_AIR_PORT = {lat: 21.324513, lng: -157.925074};
// var AIR_PORTS = [
//   HND_AIR_PORT,
//   HNL_AIR_PORT,
//   SFO_AIR_PORT
// ];

//     // This code is necessary for browser
//     Environment.setEnv({
//       'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY',
//       'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY'
//     });

//     let mapOptions: GoogleMapOptions = {
//       camera: {
//          target:AIR_PORTS
//         //   {
//         //    lat: 43.0741904,
//         //    lng: -89.3809802
//         //  }
//          ,
//          zoom: 18,
//          tilt: 30
//        },controls: {
//         zoom: false,//true
//         myLocationButton : true,
//         myLocation:true
        
//     }
//     };

//     this.map = GoogleMaps.create('map_canvas', mapOptions);

//     // let marker: Marker = this.map.addMarkerSync({
//     //   // title: 'Ionic\n <br>hello',
//     //   // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
//     //   icon: 'orange',
//     //   animation: 'DROP',
//     //   position: {
//     //     lat: 43.0741904,
//     //     lng: -89.3809802
//     //   }
//     // });
  
  
//     let htmlInfoWindow = new HtmlInfoWindow();
//     htmlInfoWindow.setContent('hello <br> hi<br> <button ion-button style="background-color:red;color:white" (click)="himarker(5)">Demo</button>' 
//     );
    
// //     var html = "<img src='./House-icon.png' width='64' height='64' >" +
// //     "<br>" +
// //     "This is an example";
// // htmlInfoWindow.setContent(html);
//     // {
//     //   width: "280px",
//     //   height: "330px"
//     // }

//     // marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
//     //   alert('clicked');
//     //   htmlInfoWindow.open(marker);
//     // });

//     this.map.addPolyline({
//       'points': AIR_PORTS,
//       'color' : '#AA00FF',
//       'width': 10,
//       'geodesic': true
//     });

    
//     this.map.on(GoogleMapsEvent.MY_LOCATION_CLICK).subscribe((location: any[]) => {
//       console.log("location from map on :",location);
//     //   let marker: Marker = this.map.addMarkerSync({
//     //     'title': ["Current your location:\n",
//     //         "latitude:" + location.latLng.lat.toFixed(3),
//     //         "longitude:" + location.latLng.lng.toFixed(3),
//     //         "speed:" + location.speed,
//     //         "time:" + location.time,
//     //         "bearing:" + location.bearing].join("\n"),
//     //     'position': location.latLng,
//     //     'animation': plugin.google.maps.Animation.DROP,
//     //     'disableAutoPan': true
//     //   });
//     //   marker.showInfoWindow();
//     // });

//     })
//   }


// mylocation(){
//   LocationService.getMyLocation().then((myLocation: MyLocation) => {
//   console.log("myloc from mylocation:  ",myLocation);
    
//   // let options: GoogleMapOptions = {
//   //   camera: {
//   //     target:   {
//   //          lat: myLocation.latLng.lat,
//   //          lng:myLocation.latLng.lng
//   //        } 
//   //   }
//   // };

//   // this.map = GoogleMaps.create('map_canvas', options);
//   // let marker: Marker = this.map.addMarkerSync({
//   //   title: 'my loc '+myLocation.latLng,
//   //   icon: 'red',
//   //   animation: 'DROP',
//   //   position: {
//   //     lat: myLocation.latLng.lat,
//   //     lng: myLocation.latLng.lng
//   //   }
//   // });

//   });  
// }

himarker(val){
  alert("hi marker "+ val)
}
dismiss(){
  console.log("dismiss")
  // this.navCtrl.pop();
  this.navCtrl.setRoot(UserHomePage)
}
// addNewMarker(){
//   if(this.marker)
//   this.marker.remove()

//           this.marker = this.map.addMarkerSync({
//             // title: 'Ionic\n <br>hello',
//             // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
//             icon: 'orange',
//             animation: 'DROP',
//             position: {
//               lat:  this.position.lat,
//               lng:  this.position.lng
//             }
//           });
// }

// initmap(){

//   Environment.setEnv({
//     'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY',
//     'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCOlXpEUuoXStcUGDGXfFx_axfNfi4CkvY'
//   });

//   let mapOptions: GoogleMapOptions = {
//     camera: {
//       //  target:AIR_PORTS
//       target:
//         {
//          lat: this.position.lat,
//          lng: this.position.lng
//        }
//        ,
//        zoom: 18,
//        tilt: 30
//      },controls: {
//       zoom: false,//true
//       myLocationButton : true,
//       myLocation:true
      
//   }
//   };

//   if(this.map)
//   this.map.remove()

//   this.map = GoogleMaps.create('map_canvas', mapOptions);



//           // this.marker = this.map.addMarkerSync({
//           //   // title: 'Ionic\n <br>hello',
//           //   // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
//           //   icon: 'orange',
//           //   animation: 'DROP',
//           //   position: {
//           //     lat:  this.position.lat,
//           //     lng:  this.position.lng
//           //   }
//           // });

//   // this.marker= this.map.addMarkerSync({
//   //   // title: 'Ionic\n <br>hello',
//   //   // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
//   //   icon: 'orange',
//   //   animation:'DROP',// 'BOUNCE',//'DROP',
//   //   position: {
//   //     lat:  this.position.lat,
//   //     lng:  this.position.lng
//   //   }
//   // });

// //   this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe((location: any[]) => {
// //     console.log("map location from map on :",location);
// //    // alert("map location : "+JSON.stringify(location));
// //    this.position = {
// //     lat: location[0].lat,
// //     lng: location[0].lng
// // };
// // this.marker.remove()

// //     this.marker = this.map.addMarkerSync({
// //       // title: 'Ionic\n <br>hello',
// //       // snippet:'snippet , <button ion-button (click)="onButtonClick($event)">Demo</button>',
// //       icon: 'orange',
// //       animation: 'DROP',
// //       position: {
// //         lat:  location[0].lat,
// //         lng:  location[0].lng
// //       }
// //     });
// //   });
// }

gotodeatils(){
  // var item = {id:1,name:"name",carno:"12356",carType:"aaaa",img:"assets/imgs/default-avatar.png"}
  console.log("this.driverObject : ",this.driverObject)
  this.navCtrl.push("DetailsPage",{item:this.driverObject})
}
FinishOrder(){
  console.log("FinishOrder")
  this.srv.endOrder(this.orderId,resp=>{
    console.log("resp from endOrder : ",resp)
    var respData = JSON.parse(resp)
    console.log("code from resp end order: ",respData.code)
    // {"0":"Status","1":"Successful Reject","code":200}
    if(respData.code == 200){
      console.log("200 status")
      this.helper.presentToast(this.translate.instant("endOrder"))
      this.navCtrl.push("RatePage",{driverId:this.driverId})
    }else{
      this.helper.presentToast(this.translate.instant("connectionErr"))
    }
    
  },err=>{
    console.log("err from endOrder : ",err)
  })
  
}

allMarkers=[]

initMap2(){

  console.log("init map");

  let latlng = new google.maps.LatLng(this.position.lat, this.position.lng);
  var mapOptions={
    center:latlng,
    zoom:15,
    streetViewControl:false,
    fullscreenControl:false,
    mapTypeId:google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true,
    styles:this.helper.mapStyle
    
  };

  this.map=  new google.maps.Map(this.mapElement.nativeElement,mapOptions);

  var markers, i;
  
  for(var j=0;j<this.allMarkers.length;j++)
  {
    this.allMarkers[j].setMap(null);
  }

  // origin: {lat:31.0324953,lng:31.3964502},
  // destination: {lat:31.034428482129403,lng:31.39275934193165},

//   let marker = new google.maps.Marker({
//     map: this.map,
//     animation: google.maps.Animation.DROP,
//     position: new google.maps.LatLng(this.driverposition.lat,this.driverposition.lng),
    
//     //  icon: { 
//    //   url : this.markimage,
//   //    size: new google.maps.Size(71, 71),
//  //     scaledSize: new google.maps.Size(20, 25) 
// //      },

//     label:{
//       text:this.drivername,
//       color:"black",  
//     }
//   });
  
//   this.allMarkers.push(marker)
  
  // let marker2 = new google.maps.Marker({
  //   map: this.map,
  //   animation: google.maps.Animation.DROP,
  //   position: new google.maps.LatLng(31.034428482129403,31.39275934193165),
  //   icon: { 
  //     url : this.markimage,
  //     size: new google.maps.Size(71, 71),
  //     scaledSize: new google.maps.Size(20, 25) 
  //   },
  //   label:{
  //     text:"my loc",
  //     color:"black",      
  //   }
  // });

  // let directionsService = new google.maps.DirectionsService;
  // let directionsDisplay = new google.maps.DirectionsRenderer;

  this.directionsDisplay.setMap(this.map);

  // directionsDisplay.setPanel(this.directionsPanel.nativeElement);
 // 31.034428482129403,lng:31.39275934193165\
// console.log("lat:parseFloat(this.trapLat),lng:parseFloat(this.trapLng)",parseFloat(this.trapLat),parseFloat(this.trapLng))|

this.directionsService.route({
      origin:  {lat:this.position.lat,lng:this.position.lng},
      destination: {lat:parseFloat(this.driverposition.lat),lng:parseFloat(this.driverposition.lng)} ,
      travelMode: google.maps.TravelMode['DRIVING']
  }, (res, status) => {

      if(status == google.maps.DirectionsStatus.OK){
        this.directionsDisplay.setDirections(res);
      } else {
          console.warn(status);
      }

  });

}


addanotherRoute(){
  console.log("addanotherRoute")
  for(var j=0;j<this.allMarkers.length;j++)
  {
    this.allMarkers[j].setMap(null);
  }
  
  // let marker2 = new google.maps.Marker({
  //   map: this.map,
  //   animation: google.maps.Animation.DROP,
  //   position: new google.maps.LatLng(this.position.lat,this.position.lng),
  //   // icon: { 
  //   //   url : this.markimage,
  //   //   size: new google.maps.Size(71, 71),
  //   //   scaledSize: new google.maps.Size(20, 25) 
  //   // },
  //   // label:{
  //   //   text:this.drivername,
  //   //   color:"black",
  //   // }
  // });

  // this.allMarkers.push(marker2)

  // origin: {lat:31.0324953,lng:31.3964502},
  // destination: {lat:31.034428482129403,lng:31.39275934193165},

  // let marker3 = new google.maps.Marker({
  //   map: this.map,
  //   animation: google.maps.Animation.DROP,
  //   position: new google.maps.LatLng(this.driverposition.lat,this.driverposition.lng),
  //   // icon: { 
  //   //   url : this.markimage,
  //   //   size: new google.maps.Size(71, 71),
  //   //   scaledSize: new google.maps.Size(20, 25) 
  //   // },
  //   label:{
  //     text:this.drivername,
  //     color:"black",      
  //   }
  // });
  // this.allMarkers.push(marker3)

  // let directionsService = new google.maps.DirectionsService;
  // let directionsDisplay = new google.maps.DirectionsRenderer;
  this.directionsDisplay.setMap(null);
  this.directionsDisplay.setMap(this.map);
  // driverposition
// directionsDisplay.setPanel(this.directionsPanel.nativeElement);
// 31.034428482129403,lng:31.39275934193165\
// console.log("lat:parseFloat(this.trapLat),lng:parseFloat(this.trapLng)",parseFloat(this.trapLat),parseFloat(this.trapLng))|
this.directionsService.route({
      origin:  {lat:parseFloat(this.origin.lat),lng:parseFloat(this.origin.lng)},
      destination: {lat:parseFloat(this.desc.lat),lng:parseFloat(this.desc.lng)} ,
      travelMode: google.maps.TravelMode['DRIVING']
  }, (res, status) => {

      if(status == google.maps.DirectionsStatus.OK){
        this.directionsDisplay.setDirections(res);
      }else{
        console.warn(status);
      }

  });
}

}